#!/usr/bin/python
#----------------------------------------------------------------------
#----------------------------------------------------------------------

import sys
import codecs



# use binary mode to avoid adding \r
if sys.platform == "win32":
	import os, msvcrt
	msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)

if len(sys.argv) < 2:
	print 'usage:', sys.argv[0], 'parsed_file'
	sys.exit()


# represent a single token in a sentence.
class Token:
	def __init__(self, id, word, lemma, cpos, pos, mfeats):
		self.id = int(id)
		self.word = word
		self.lemma = lemma
		self.cpos = cpos
		self.pos = pos
		self.mfeats = mfeats

	def __init__(self, items):
		self.id = int(items[0])
		self.word = items[1]
		self.lemma = items[2]
		self.cpos = items[3]
		self.pos = items[4]
		self.mfeats = items[5]

	def __repr__(self):
		return '\t'.join([str(self.id), self.word, self.lemma, self.cpos, self.pos, self.mfeats])

def Stampa(tokens):
	for tok in tokens:
		print '\t'.join([str(tok.id), tok.word.encode("utf-8"), tok.lemma.encode("utf-8"), tok.cpos.encode("utf-8"), tok.pos.encode("utf-8"), tok.mfeats.encode("utf-8")])
	print

def TrasformaEStampa(tok, MA, clit):
	if clit:
		clitS=clit.split("-\t")
		print '\t'.join([clitS[0]+tok.word.encode("utf-8"), clitS[1]])
		return MA, ""
	MFtrans=""
	if tok.mfeats=="_":
		pos=tok.pos
	elif (tok.pos[0] in ["S","A", "E", "R",  "T", "P", "D", "N"]):
		if not(((tok.mfeats).find("per="))==-1):
			MFtrans+=tok.mfeats[(tok.mfeats).find("per=")+4]
		if not(((tok.mfeats).find("gen="))==-1):
			MFtrans+=tok.mfeats[(tok.mfeats).find("gen=")+4]
		if not(((tok.mfeats).find("num="))==-1):
			MFtrans+=tok.mfeats[(tok.mfeats).find("num=")+4]
		pos=tok.pos+MFtrans
	elif (tok.pos[0] =="V"): #V, VA, VM
		if not(((tok.mfeats).find("gen="))==-1):
			MFtrans=tok.mfeats[(tok.mfeats).find("mod=")+4]
			MFtrans+="s" #MFtrans+=tok.mfeats[(tok.mfeats).find("ten=")]  #omesso in conll format..
			MFtrans+=tok.mfeats[(tok.mfeats).find("gen=")+4]
			MFtrans+=tok.mfeats[(tok.mfeats).find("num=")+4]
		elif tok.mfeats[(tok.mfeats).find("mod=")+4] in ["d", "m"]:
			if not(((tok.mfeats).find("mod="))==-1):
				MFtrans=tok.mfeats[(tok.mfeats).find("mod=")+4]
			#if not(((tok.mfeats).find("ten="))==-1):   #viene inseriro automaticamente nella creazione del conll... (es: arrabbierei)
				#MFtrans+=tok.mfeats[(tok.mfeats).find("ten=")+4]
			if not(((tok.mfeats).find("per="))==-1):
				MFtrans+=tok.mfeats[(tok.mfeats).find("per=")+4]
			if not(((tok.mfeats).find("num="))==-1):
				MFtrans+=tok.mfeats[(tok.mfeats).find("num=")+4]
		elif tok.mfeats[(tok.mfeats).find("mod=")+4]=="p":
			MFtrans=tok.mfeats[(tok.mfeats).find("mod=")+4]
			MFtrans+="p" #MFtrans+=tok.mfeats[(tok.mfeats).find("ten=")]  #omesso in conll format.. (es: cinguettante)
			MFtrans+=tok.mfeats[(tok.mfeats).find("num=")+4]
		else:
			if not(((tok.mfeats).find("mod="))==-1):
				MFtrans=tok.mfeats[(tok.mfeats).find("mod=")+4]
			if not(((tok.mfeats).find("ten="))==-1):
				MFtrans+=tok.mfeats[(tok.mfeats).find("ten=")+4]
			if not(((tok.mfeats).find("per="))==-1):
				MFtrans+=tok.mfeats[(tok.mfeats).find("per=")+4]
			if not(((tok.mfeats).find("num="))==-1):
				MFtrans+=tok.mfeats[(tok.mfeats).find("num=")+4]
		pos=tok.pos+MFtrans
	else:
		MA[tok.pos]=1
#		print >> sys.stderr, tok
		print '\t'.join([tok.word.encode("utf-8"), tok.pos.encode("utf-8"), tok.mfeats.encode("utf-8"), "!!!"])
		#return
		return MA
	if len(tok.word)>1 and tok.word[-1]=="-":
		pos+="c"
		return MA, '\t'.join([tok.word.encode("utf-8"), pos.encode("utf-8")])
	print '\t'.join([tok.word.encode("utf-8"), pos.encode("utf-8")])
	return MA, ""


def analisi(fileP):
	tokens = []
	clit=""
	MA={}
	sysFileP = codecs.open(fileP, 'r', 'utf-8')
	while True:
		lineP = sysFileP.readline()
		#print lineP.encode("utf-8")
		if lineP == '':
			break
		if lineP == '\n':
			#print tokens
			trovataPunteggiatura=0.0
			for tok in tokens:
				#print "XXX",'\t'.join([str(tok.id), tok.word.encode("utf-8"), tok.lemma.encode("utf-8"), tok.cpos.encode("utf-8"), tok.pos.encode("utf-8"), tok.mfeats.encode("utf-8")])
				#print clit
				MA, clit=TrasformaEStampa(tok, MA, clit)
			print
			tokens=[]
			continue
		tokens.append(Token(lineP.strip().split('\t')))

	if MA:
		print "POS escluse dalla trasformazione:"
		for pos in MA:
			print pos

analisi(sys.argv[1])
