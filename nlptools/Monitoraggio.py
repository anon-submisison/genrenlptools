#!/usr/bin/python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
#----------------------------------------------------------------------

# Count Statistics

import sys
import re
import math
import unicodedata
import os.path
import getopt
import codecs
from Conf import Conf
from Token import Token
from Sentence import Sentence
from Utils import *
from pkg_resources import resource_filename

class Monitoraggio():
	
	def __init__(self,  encode='utf-8'):
		self.DizionarioDeMauro = {}
		self.encode = encode
		self.confronto = False
		#parole analizzate per il calcolo del  Type(forme)/token e Type(lemmi)/token
		self.rangeType = 50.0
		self.rangeType2 = 100.0
		self.azzeraVariabili()
			
	def setLessico(self,lessico):
		self.lessico = lessico

	def setConfronto(self, confronto):
		self.confronto = confronto
		self.divisioneDoc = True
		if confronto:
			self.facile = resource_filename("t2k.catenapy", "Lessici/it/Readability/2Parole_doc.confronto")
			self.difficile = resource_filename("t2k.catenapy", "Lessici/it/Readability/Repubblica_doc.confronto")
 
	def setDivisioneDoc(self, divisioneDoc):
		self.divisioneDoc=divisioneDoc

	def CaricaDizionarioFondamentale(self):
		try:
			lessicoFile = codecs.open(self.lessico, encoding=self.encode, mode='r')
		except IOError:
			print >> sys.stderr,'Il file', self.lessico, 'non  esiste!'
			exit(-1)
		for l in lessicoFile:
			t = l.strip().split('\t')
			self.DizionarioDeMauro[t[0]]=(t[1].split())[0]

	def caricaStatistiche(self, fileModello):
		MAP={}
		try:
			File = codecs.open(fileModello, encoding=self.encode, mode='r')
		except IOError:
			print >> sys.stderr,'Il file', fileModello, 'non  esiste!'
			exit(-1)
		for l in File:
			t = l.strip().split('\t')
			MAP[t[0]]=t[1]
		return MAP



	def DividiTestoInDocumenti(self, inputFileName, destDir=None):
		if not(self.divisioneDoc):
			(self.fileInput, self.fileOutput, self.outputFileName) = openIOFiles(inputFileName, destDir, ".monit_global")
		else:
			(self.fileInput, self.fileOutput, self.outputFileName) = openIOFiles(inputFileName, destDir, ".monit")
		TESTO=[]
		Frase=[]
		DOC=[]
		documento="1"
		while True:
			line = self.fileInput.readline()
			if line == '':
				if DOC:
					TESTO.append((documento, DOC))
				break
			if ('</doc>' in line):
				Frase = []
				continue
			if '1\t<doc' in line or '<doc' in line:
				if self.divisioneDoc:  #QUI3010
					if " id=" in line:
						#m = re.search('(?<=id=\").*?(?=\")',line)
						m = re.search(' id\s*=\"(.*?)\"',line)
					elif " index=" in line or " index =" in line:
						#m = re.search('(?<=index =\").*?(?=\")',line)
						m = re.search(' index\s*=\"(.*?)\"',line)
					if DOC:
						TESTO.append((documento, DOC))
					idDoc = m.group(1)
					documento= m.group(1)
					DOC=[]
				Frase = []
				continue
			if line == '\n':
				if len(Frase)>0:
					DOC.append(Frase)
				Frase = []
				continue
			Frase.append(line)
		return TESTO


	def azzeraVariabili(self):
		self.RadiciVerbaliConSogg=0.0
		self.ParolePiene=0.0
		self.TOT_Token=0.0
		self.TotaleConjDisj=0.0
		self.TOT_ROOT=0.0
		self.TotaleValidiPerLunghezzaLink=0.0
		self.Lemmi={}
		self.Forme={}
		self.DirezioneDipendenze={"subord_pre":0.0, "subord_post":0.0}
		self.DirezioneDipendenzeSO={"subj_pre":0.0, "subj_post":0.0, "obj_pre": 0.0, "obj_post":0.0}
		self.LemmiTT={}
		self.FormeTT={}
		self.LemmiTT2={}
		self.FormeTT2={}
		self.TrovateInDizionario=0.0
		self.lunghezzaLink=0.0
		self.lunghezzaLinkMax=0.0
		self.altezzaMax=0.0
		self.sentCount=0.0
		self.DEP={}
		self.LINK={}
		self.V_ARG={}
		self.ARG={}
		self.ARG_Special={}
		self.MOD_Special={}
		self.COMP_Special={}
		self.POS={}
		self.CPOS={}
		self.CATENE_PREP={}
		self.VERBI_TEN={}
		self.VERBI_MODO={}
		self.VERBI_NUM_PERSON={}
		self.LINK_ENTRANTI={}
		self.TOT_Catene=0.0
		self.TOT_Arg=0.0
		self.TOT_Comp=0.0
		self.RadiciVerbali=0.0
		self.TOT_Mod=0.0
		self.ContaLinkEntranti=0.0
		self.lunghezzaCatena=0.0
		self.verbCount=0.0
		self.tokenCount=0.0
		self.verbTOT=0.0
		self.verbiTempo=0.0
		self.verbiNumPer=0.0
		self.TOT_Subordinate=0.0
		self.altezzaSubordinantiTOT=0.0
		self.ALTEZZASub={}
		self.LunghezzaParole=0.0
		#Legenda:(dal piu semplice al piu complesso)
		#1)FO: Fondamentale;
		#2)AU: Alto Uso
		#3)AD: Alta Disponibilta
		self.DizFondamentale ={"AD":0.0, "AU":0.0, "FO": 0.0}
		self.DizionarioDeMauroType={}
		self.DizFondamentaleType ={"AD":0.0, "AU":0.0, "FO": 0.0}

	def visita(self, tok, tokens):
		if not(tok.children):
			altezza=0
		else:
			altezzaMax=0
			for tokC in tok.children:
				tmp=self.visita(tokC, tokens)
				if tmp>altezzaMax:
					altezzaMax=tmp
				altezza=altezzaMax+1
		return altezza

	def visitaSubordinate(self, tok, tokens, altezzaSUB, albero):
		#global TOT_Subordinate, self.altezzaSubordinantiTOT, self.ALTEZZASub
		trovato=False
		for figlio in tok.children:
			if (figlio.cpos=='V' and (figlio.dep=='mod' or figlio.dep=='arg')):
				trovato=True
				break
			if ((figlio.pos=='CS' or figlio.pos=='B' or figlio.cpos=='E') and (figlio.dep=='arg' or figlio.dep=='mod' or figlio.dep=='comp')):
				trovato=True
				break

		if not(trovato):
			if altezzaSUB:
				self.TOT_Subordinate+=1.0
				if altezzaSUB in self.ALTEZZASub:
					self.ALTEZZASub[altezzaSUB]+=1.0
				else:
					self.ALTEZZASub[altezzaSUB]=1.0
				self.altezzaSubordinantiTOT+=altezzaSUB
		else:
			beccato=False
			for figlio in tok.children:
				if (figlio.cpos=='V' and (figlio.dep=='mod' or figlio.dep=='arg')):
					albero+=" "+figlio.word
					altezzaSUB+=1
					altezzaSUB=self.visitaSubordinate(figlio, tokens, altezzaSUB, albero)
					beccato=True

				if ((figlio.pos=='CS' or figlio.pos=='B' or figlio.cpos=='E') and (figlio.dep=='arg' or figlio.dep=='mod' or figlio.dep=='comp')):
					for figlio2 in figlio.children:
						if figlio2.dep=='sub' or (figlio2.dep=='prep' and figlio2.cpos=='V'):

							altezzaSUB+=1
							albero+=" "+figlio2.word
							altezzaSUB=self.visitaSubordinate(figlio2, tokens, altezzaSUB, albero)
							beccato=True

			if not(beccato) and altezzaSUB:
				self.TOT_Subordinate+=1.0
				if altezzaSUB in self.ALTEZZASub:
					self.ALTEZZASub[altezzaSUB]+=1.0
				else:
					self.ALTEZZASub[altezzaSUB]=1.0
				self.altezzaSubordinantiTOT+=altezzaSUB

		altezzaSUB=altezzaSUB-1
		return altezzaSUB

	def visitaCatenePrep(self, tok, tokens, altezzaSUBP, TOT_Catene_Parziali, Visitato, ALTEZZASubP):
		#global lunghezzaCatena
		Visitato.append(tok.id)
		Catena=False
		fid=0
		for figlio in tok.children:
			if figlio.dep=="prep":
				altezzaSUBP+=1
				fid =  figlio.id
		if fid!=0:
			for figlio in tokens[fid-1].children:
				if (figlio.dep in ('comp','comp_temp','comp_loc')):
					altezzaSUBP, TOT_Catene_Parziali, ALTEZZASubP, Visitato = self.visitaCatenePrep(figlio, tokens, altezzaSUBP, TOT_Catene_Parziali, Visitato, ALTEZZASubP)
					Catena=True

		if not(Catena):
			if altezzaSUBP:
		            TOT_Catene_Parziali+=1.0
			    if altezzaSUBP not in ALTEZZASubP:
				    ALTEZZASubP[altezzaSUBP]=0.0
			    ALTEZZASubP[altezzaSUBP]+=1.0
			    self.lunghezzaCatena+=altezzaSUBP

		altezzaSUBP=altezzaSUBP-1
		return altezzaSUBP, TOT_Catene_Parziali, ALTEZZASubP, Visitato


	def CalcolaAltezzaAlbero(self, tokens):
		altezza=0.0
		altezzaMax=0.0
		altezzaTOT=0.0
		nROOT=0.0
		for tok in tokens:
			if (tok.dep=='ROOT') and (tok.head==0):
				nROOT+=1.0
				altezza= self.visita(tok, tokens)
				altezzaTOT+=altezza
		if nROOT>0:
			return (altezzaTOT/nROOT)
		else:
			return altezzaTOT

	def CalcolaCatenaSubordinate(self, tokens):
		#global TOT_Subordinate, altezzaSubordinantiTOT, ALTEZZASub
		altezzaSUB=0
		albero=""
		for tok in tokens:
			if (tok.dep=='ROOT') and (tok.head==0) and tok.cpos=='V':
				aa=self.visitaSubordinate(tok, tokens, altezzaSUB, albero)


	def CalcolaCatenePrep(self, tokens):
		#global TOT_Catene, lunghezzaCatena, CATENE_PREP
		Visitato=[]

		for i in range(len(tokens)):
			if tokens[i].dep in ('comp','comp_temp','comp_loc') and not(tokens[i].id in Visitato):
				#QUI!
				#print "INIZIO:", tokens[i].word
				TOT_Catene_Parziali=0
				LunghezzaCatenaAttuale=0
				altezzaSUB, TOT_Catene_Parziali, self.CATENE_PREP, Visitato=self.visitaCatenePrep(tokens[i], tokens, 0, 0, Visitato, self.CATENE_PREP)
				self.TOT_Catene+=TOT_Catene_Parziali
			
	def CalcolaArchiEntranti(self, tokens):
		#global ContaLinkEntranti, LINK_ENTRANTI
		NumeroLinkEntrantiAttuale=0.0
		for i in range(len(tokens)):
			if tokens[i].pos=='V':
				NumeroLinkEntrantiAttuale=0.0
				for tok in tokens[i].children:
					if tok.dep not in ('con','conj','dis','disj', 'sub','punc', 'prep', 'mod'):
						self.ContaLinkEntranti+=1
						NumeroLinkEntrantiAttuale+=1
				if NumeroLinkEntrantiAttuale in self.LINK_ENTRANTI:
					self.LINK_ENTRANTI[NumeroLinkEntrantiAttuale]+=1
				else:
					#if NumeroLinkEntrantiAttuale == 67: #err
						#print tokens
					self.LINK_ENTRANTI[NumeroLinkEntrantiAttuale]=1


	def FeatLessicali(self, tokens, DizionarioDeMauro):
		#lobal Forme, LunghezzaParole, Lemmi, DizFondamentale, TrovateInDizionario, TOT_Token, DizFondamentaleType, DizionarioDeMauroType, FormeTT, LemmiTT, FormeTT2, LemmiTT2
		for tok in tokens:
			if not(tok.cpos=='F'):
				self.TOT_Token+=1.0
				self.LunghezzaParole+=float(len(tok.word))
			#Rapporto Types/Token: (sul Lemma e sulla Forma)
			if tok.word in self.Forme:
				self.Forme[tok.word]+=1.0
			else:
				self.Forme[tok.word]=1.0
			if tok.lemma in self.Lemmi:
				self.Lemmi[tok.lemma]+=1.0
			else:
				self.Lemmi[tok.lemma]=1.0
			if self.TOT_Token< self.rangeType:
				if tok.word in self.FormeTT:
					self.FormeTT[tok.word]+=1.0
				else:
					self.FormeTT[tok.word]=1.0
				if tok.lemma in self.LemmiTT:
					self.LemmiTT[tok.lemma]+=1.0
				else:
					self.LemmiTT[tok.lemma]=1.0
			if self.TOT_Token<self.rangeType2:
				if tok.word in self.FormeTT2:
					self.FormeTT2[tok.word]+=1.0
				else:
					self.FormeTT2[tok.word]=1.0
				if tok.lemma in self.LemmiTT2:
					self.LemmiTT2[tok.lemma]+=1.0
				else:
					self.LemmiTT2[tok.lemma]=1.0
			if (tok.lemma in self.DizionarioDeMauro) or ((tok.lemma).lower() in self.DizionarioDeMauro):
				self.TrovateInDizionario+=1.0
				if (not(tok.lemma in self.DizionarioDeMauroType) and not((tok.lemma).lower() in self.DizionarioDeMauroType)):
					self.DizionarioDeMauroType[tok.lemma]=1.0
					if tok.lemma in self.DizionarioDeMauro:
						self.DizFondamentaleType[self.DizionarioDeMauro[tok.lemma]]+=1.0
					elif((tok.lemma).lower() in self.DizionarioDeMauro):
						self.DizFondamentaleType[self.DizionarioDeMauro[(tok.lemma).lower()]]+=1.0
				if tok.lemma in self.DizionarioDeMauro:
					self.DizFondamentale[self.DizionarioDeMauro[tok.lemma]]+=1.0
				elif((tok.lemma).lower() in self.DizionarioDeMauro):
					self.DizFondamentale[self.DizionarioDeMauro[(tok.lemma).lower()]]+=1.0


	def FeatPOS(self, tokens):
		#global self.POS, self.CPOS, self.VERBI_TEN, self.VERBI_MODO, self.VERBI_NUM_PERSON, self.verbCount, self.ParolePiene, self.verbTOT, self.verbiTempo, self.verbiNumPer
		VerbiID={}
		for tok in tokens:
			if (tok.cpos in ['S', 'A', 'B'] or (tok.cpos=='V' and not(tok.pos=='VA'))):
				#print 'piene:', tok.pos
				self.ParolePiene+=1.0		
			if tok.pos in self.POS:
				self.POS[tok.pos]+=1
			else:
				self.POS[tok.pos]=1.0
			if tok.cpos in self.CPOS:
				self.CPOS[tok.cpos]+=1
			else:
				self.CPOS[tok.cpos]=1.0
			if tok.pos == 'V':
				self.verbCount += 1

			#Verbi: Morph:num=s|per=3|mod=i|ten=p
			#V	num=s|mod=p|gen=f
			if tok.pos == 'V':
				#print 'tok:',tok #err
				Bool_Verbo=False
				minV=tok.id
				for figlio in tokens[tok.id-1].children:
					if(figlio.pos=='VA'):# or figlio.pos=='VM'):
						#Bool_Verbo=True
						if minV>figlio.id:
							minV=figlio.id
					if figlio.pos=='VM':
						for figlio1 in figlio.children:
							if figlio1.cpos=='V':
								if minV>figlio1.id:
									minV=figlio1.id
				if not(minV in VerbiID):
					#print 'ecco:', tokens[minV-1] #err
					self.verbTOT += 1
					VerbiID[minV]=1.0
					tokVerboPrincipale=tokens[minV-1]
					posizione=(tokVerboPrincipale.mfeats).find("ten=")
					#if not(Bool_Verbo):
					if not(posizione==-1):
						self.verbiTempo+=1.0
						posMfeat=tokVerboPrincipale.cpos+'+'+(tokVerboPrincipale.mfeats)[posizione+4]
						#if posMfeat=='V+s' or posMfeat=='V+f':
							#print 'ecco:', tokens[minV-1] #err
						if posMfeat in self.VERBI_TEN:
							self.VERBI_TEN[posMfeat]+=1
						else:
							self.VERBI_TEN[posMfeat]=1
					#else: qui avremmo Participi e verbi all'infinito

					posizione=(tokens[minV-1].mfeats).find("mod=")
					if not(posizione==-1):
						posMfeat=tokVerboPrincipale.pos+'+'+(tokens[minV-1].mfeats)[posizione+4]
						if posMfeat in self.VERBI_MODO:
							self.VERBI_MODO[posMfeat]+=1
						else:
							self.VERBI_MODO[posMfeat]=1

					posizione1=(tokens[minV-1].mfeats).find("num=")
					posizione2=(tokens[minV-1].mfeats).find("per=")
					if not(posizione1==-1 or posizione2==-1):
						self.verbiNumPer+=1.0
						posMfeat=tokVerboPrincipale.pos+'+'+(tokens[minV-1].mfeats)[posizione1+4]+'+'+(tokens[minV-1].mfeats)[posizione2+4]
						if posMfeat in self.VERBI_NUM_PERSON:
							self.VERBI_NUM_PERSON[posMfeat]+=1
						else:
							self.VERBI_NUM_PERSON[posMfeat]=1

#QUI OGGGI:
	def FeatSintattiche(self, tokens):
		#global self.DEP, self.TOT_Arg, self.ARG, self.ARG_Special, self.TOT_Mod, self.MOD_Special, self.TOT_Comp, self.COMP_Special, self.altezzaMax, self.lunghezzaLink, self.lunghezzaLinkMax, self.TOT_ROOT, self.RadiciVerbali, self.TotaleValidiPerLunghezzaLink, self.TotaleConjDisj, self.DirezioneDipendenze, self.DirezioneDipendenzeSO, self.RadiciVerbaliConSogg, self.TOT_Subordinate, self.altezzaSubordinantiTOT, self.ALTEZZASub
		self.lunghezzaLinkmax=0.0
		lunghezzalink=0.0
		self.CalcolaCatenePrep(tokens)
		self.CalcolaCatenaSubordinate(tokens) #QUII
		self.altezzaMax+=self.CalcolaAltezzaAlbero(tokens)
		self.CalcolaArchiEntranti(tokens)
		#print tokens #errr
		for tok in tokens:
			if tok.head!=0 and tok.cpos[0]!='F':
				self.TotaleValidiPerLunghezzaLink+=1
				lunghezzalink=math.fabs(tok.id-tok.head)
				self.lunghezzaLink+=lunghezzalink
				if self.lunghezzaLinkmax<lunghezzalink:
					self.lunghezzaLinkmax=lunghezzalink

			if tok.dep=='ROOT':
				self.TOT_ROOT+=1
				if tok.cpos=='V' :
					self.RadiciVerbali+=1.0
					for figlio in tok.children:
						if figlio.dep=='subj' or figlio.dep=='subj_pass':
							self.RadiciVerbaliConSogg+=1.0
							break

			# direzione del link Soggetto/Oggetto rispetto al verbo:
			if tok.dep in ['subj', 'subj_pass']:
				if tok.head > tok.id:
					dipDir='subj_pre'
				else:
					dipDir='subj_post'
				if dipDir in self.DirezioneDipendenzeSO:
					self.DirezioneDipendenzeSO[dipDir]+=1.0
				else:
					self.DirezioneDipendenzeSO[dipDir]=1.0
			if tok.dep == 'obj':
				if tok.head > tok.id:
					dipDir='obj_pre'
				else:
					dipDir='obj_post'
				if dipDir in self.DirezioneDipendenzeSO:
					self.DirezioneDipendenzeSO[dipDir]+=1.0
				else:
					self.DirezioneDipendenzeSO[dipDir]=1.0
			#######
			if tok.dep in self.DEP:
				self.DEP[tok.dep]+=1
			else:
				self.DEP[tok.dep]=1.0
			#if (tok.dep in ('conj','disj','sub')): # Per ora le "sub" non mi servono
			if (tok.dep in ('conj','disj')):
				#link=tok.dep+'-->'+tokens[tok.head-1].pos
				link='conj/disj-->'+tokens[tok.head-1].cpos
				self.TotaleConjDisj+=1.0
				if link in self.LINK:
					self.LINK[link]+=1
				else:
					self.LINK[link]=1
			if (tok.dep == 'arg'):
				for figlio in tok.children:
					link=figlio.pos+'-->'+tok.pos+'-->'+tokens[tok.head-1].pos
					if link in self.ARG:
						self.ARG[link]+=1
					else:
						self.ARG[link]=1
				if (((tok.pos=='CS') or (tok.cpos=='E')) and (tokens[tok.head-1].pos=='V')):
					for figlio in tok.children:
						if (figlio.pos=='V' and (figlio.dep=='prep' or figlio.dep=='sub')):
							if tok.pos=='EA':
								#FraseSub=figlio.word+'-->'+tok.word+'-->'+tokens[tok.head-1].word  #errrrr
								link=figlio.pos+'-->'+'E'+'-->'+tokens[tok.head-1].pos
							else:
								#FraseSub=figlio.word+'-->'+tok.word+'-->'+tokens[tok.head-1].word  #errrrr
								link=figlio.pos+'-->'+tok.pos+'-->'+tokens[tok.head-1].pos
							##Calcolo se la subordinata e' a destra o a sinistra rispetto al verbo principale:
							if tok.head > tok.id:
								dipDir='subord_pre'
							else:
								dipDir='subord_post'
							if dipDir in self.DirezioneDipendenze:
								self.DirezioneDipendenze[dipDir]+=1.0
							else:
								self.DirezioneDipendenze[dipDir]=1.0
							######
							#print "self.ARG:", FraseSub #errrrr
							if link in self.ARG_Special:
								self.ARG_Special[link]+=1.0
							else:
								self.ARG_Special[link]=1.0
				if (tok.pos=='V' and tokens[tok.head-1].pos=='V'):
					#FraseSub=tok.word+'-->'+tokens[tok.head-1].word  #errrrr
					link=tok.pos+'-->'+tokens[tok.head-1].pos
					##Calcolo se la subordinata e' a destra o a sinistra rispetto al verbo principale:
					if tok.head > tok.id:
						dipDir='subord_pre'
					else:
						dipDir='subord_post'
					if dipDir in self.DirezioneDipendenze:
						self.DirezioneDipendenze[dipDir]+=1.0
					else:
						self.DirezioneDipendenze[dipDir]=1.0
					######
					#print "self.ARG:", FraseSub #errrrr
					if link in self.ARG_Special:
						self.ARG_Special[link]+=1.0
					else:
						self.ARG_Special[link]=1.0					
			if (tok.dep == 'mod'):
				self.TOT_Mod+=1
				if (((tok.pos=='CS') or (tok.cpos=='E') or (tok.cpos=='B')) and (tokens[tok.head-1].pos=='V')):
					for figlio in tok.children:
						if (figlio.pos=='V' and (figlio.dep=='prep' or figlio.dep=='sub')):
							#FraseSub=figlio.word+'-->'+tok.word+'-->'+tokens[tok.head-1].word  #errrrr
							link=figlio.pos+'-->'+tok.pos+'-->'+tokens[tok.head-1].pos
							##Calcolo se la subordinata e' a destra o a sinistra rispetto al verbo principale:
							if tok.head > tok.id:
								dipDir='subord_pre'
							else:
								dipDir='subord_post'
							if dipDir in self.DirezioneDipendenze:
								self.DirezioneDipendenze[dipDir]+=1.0
							else:
								self.DirezioneDipendenze[dipDir]=1.0
							######
							#print "MOD:",  FraseSub #errrrr
							if link in self.MOD_Special:
								self.MOD_Special[link]+=1.0
							else:
								self.MOD_Special[link]=1.0
			if (tok.dep == 'comp'):
				self.TOT_Comp+=1
				if ((tok.cpos=='E') and (tokens[tok.head-1].pos=='V')):
					for figlio in tok.children:
						if (figlio.pos=='V' and figlio.dep=='prep'):
							if tok.pos=='EA':
								#FraseSub=figlio.word+'-->'+tok.word+'-->'+tokens[tok.head-1].word  #errrrr
								link=figlio.pos+'-->'+'E'+'-->'+tokens[tok.head-1].pos
							else:
								link=figlio.pos+'-->'+tok.pos+'-->'+tokens[tok.head-1].pos
								#FraseSub=figlio.word+'-->'+tok.word+'-->'+tokens[tok.head-1].word  #errrrr
								#link=figlio.pos+'-->'+tok.pos+'-->'+tokens[tok.head-1].pos
							##Calcolo se la subordinata e' a destra o a sinistra rispetto al verbo principale:
							if tok.head > tok.id:
								dipDir='subord_pre'
							else:
								dipDir='subord_post'
							if dipDir in self.DirezioneDipendenze:
								self.DirezioneDipendenze[dipDir]+=1.0
							else:
								self.DirezioneDipendenze[dipDir]=1.0
							######
							#print "COMP:", FraseSub  #errrrr
							if link in self.COMP_Special:
								self.COMP_Special[link]+=1.0
							else:
								self.COMP_Special[link]=1.0
		self.lunghezzaLinkMax+=self.lunghezzaLinkmax


	def stampaStatisticheNoConfronto(self, documento):
		print >> self.fileOutput,  '\t'.join(["##########", "##########", "##########", "##########", "##########"])
		print >> self.fileOutput,  '\t'.join(["Statistiche Documento:", str(documento)])
		print >> self.fileOutput 
		print >> self.fileOutput,  '<b>Profilo di base'
		print >> self.fileOutput
		print >> self.fileOutput,  '\t'.join(['Numero totale periodi:', str(self.sentCount)])
		print >> self.fileOutput,  '\t'.join(['Numero totale parole (token):', str(self.tokenCount)])
		print >> self.fileOutput,  '\t'.join(['Lunghezza media dei periodi (in token):', str(self.tokenCount/self.sentCount)])
		print >> self.fileOutput,  '\t'.join(['Lunghezza media delle parole (in caratteri):',str(self.LunghezzaParole/self.TOT_Token)])
		print >> self.fileOutput
		print >> self.fileOutput
		print >> self.fileOutput,  '<b>Profilo lessicale'
		print >> self.fileOutput
		print >> self.fileOutput, '<i>Composizione del vocabolario'
		print >> self.fileOutput,  '\t'.join(['<t>Percentuale di lemmi appartenente al Vocabolario di Base (VdB):', str((len(self.DizionarioDeMauroType)*1.0/len(self.Lemmi)*1.0)*100)])
		tag="DizL  "
		classe="FO"
		if not(self.DizionarioDeMauroType):
			denominatore=1.0
		else:
			denominatore=len(self.DizionarioDeMauroType)
		print >> self.fileOutput, "<t>Ripartizione dei lemmi riconducibili al VdB rispetto ai repertori d'uso:"
		chiave="<t><t>Fondamentale:"
		print >> self.fileOutput,  '\t'.join([chiave, str((self.DizFondamentaleType[classe]/denominatore)*100*1.0)])
		classe="AU"
		chiave="<t><t>Alto uso:"
		print >> self.fileOutput,  '\t'.join([chiave, str((self.DizFondamentaleType[classe]/denominatore)*100*1.0)])
		classe="AD"
		chiave="<t><t>Alta disponibilità:"
		print >> self.fileOutput,  '\t'.join([chiave.decode('utf-8'), str((self.DizFondamentaleType[classe]/denominatore)*100*1.0)])
		print >> self.fileOutput
		if self.tokenCount>100:
			print >> self.fileOutput,  '\t'.join(["<i>Rapporto tipo/unità (calcolato rispetto alle prime 100 parole del testo):".decode('utf-8'), str(len(self.LemmiTT2)/self.rangeType2)])
		print >> self.fileOutput
		print >> self.fileOutput,  '\t'.join(['<i>Densità Lessicale:'.decode('utf-8'), str(self.ParolePiene/self.TOT_Token)])
		print >> self.fileOutput
		print >> self.fileOutput
		print >> self.fileOutput,  '<b>Profilo Sintattico'
		print >> self.fileOutput
		print >> self.fileOutput, '<i>"Misura" delle categorie morfo-sintattiche (%)'
		if "S" in self.POS:
			print >> self.fileOutput,  '\t'.join(["<t>Sostantivi:", str((self.POS["S"]/self.tokenCount)*100*1.0)])
		else:
			print >> self.fileOutput,  '\t'.join(["<t>Sostantivi:", str((0.0)*100*1.0)])
		if "SP" in self.POS:
			print >> self.fileOutput,  '\t'.join(["<t>Nomi Propri:", str((self.POS["SP"]/self.tokenCount)*100*1.0)])
		else:
			print >> self.fileOutput,  '\t'.join(["<t>Nomi Propri:", str((0.0)*100*1.0)])
		if "A" in self.CPOS:
			print >> self.fileOutput,  '\t'.join(["<t>Aggettivi:", str((self.CPOS["A"]/self.tokenCount)*100*1.0)])
		else:
			print >> self.fileOutput,  '\t'.join(["<t>Aggettivi:", str((0.0)*100*1.0)])
## 		if "V" in self.POS:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Verbi Principali:", self.corpusFacile["POS V"], str((self.POS["V"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS V"]])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Verbi Principali:", self.corpusFacile["POS V"], str((0.0)*100*1.0), self.corpusDifficile["POS V"]])
## 		if "VA" in self.POS:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Verbi Ausiliari:", self.corpusFacile["POS VA"], str((self.POS["VA"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS VA"]])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Verbi Ausiliari:", self.corpusFacile["POS VA"], str((0.0)*100*1.0), self.corpusDifficile["POS VA"]])
## 		if "VM" in self.POS:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Verbi Modali:", self.corpusFacile["POS VM"], str((self.POS["VM"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS VM"]])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Verbi Modali:", self.corpusFacile["POS VM"], str((0.0)*100*1.0), self.corpusDifficile["POS VM"]])
		if "V" in self.CPOS:
 			print >> self.fileOutput,  '\t'.join(["<t>Verbi:", str((self.CPOS["V"]/self.tokenCount)*100*1.0)])
		else:
 			print >> self.fileOutput,  '\t'.join(["<t>Verbi:", str((0.0)*100*1.0)])

		#Sistemare Coordinanti e Subordinanti in Difficile e Facile
		if "C" in self.CPOS:
 			print >> self.fileOutput,  '\t'.join(["<t>Congiunzioni:", str((self.CPOS["C"]/self.tokenCount)*100*1.0)])
			TotaleDivisione=0.0
			if "CC" in self.POS:
				TotaleDivisione+=self.POS["CC"]
			if "CS" in self.POS:
				TotaleDivisione+=self.POS["CS"]
			if "CC" in self.POS:
				print >> self.fileOutput,  '\t'.join(["<t><t>Coordinanti:", str((self.POS["CC"]/(TotaleDivisione)*1.0)*100*1.0)])
			else:
				print >> self.fileOutput,  '\t'.join(["<t><t>Coordinanti:", str((0.0)*100*1.0)])
			if "CS" in self.POS:
				print >> self.fileOutput,  '\t'.join(["<t><t>Subordinanti:", str((self.POS["CS"]/(TotaleDivisione)*1.0)*100*1.0)])
			else:
				print >> self.fileOutput,  '\t'.join(["<t><t>Subordinanti:", str((0.0)*100*1.0)])
 		else:
 			print >> self.fileOutput,  '\t'.join(["<t>Congiunzioni:", str((0.0)*100*1.0)])
			print >> self.fileOutput,  '\t'.join(["<t><t>Coordinanti:", str((0.0)*100*1.0)])
			print >> self.fileOutput,  '\t'.join(["<t><t>Subordinanti:", str((0.0)*100*1.0)])
		
## 		if "CS" in self.POS:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Congiunzioni Subordinanti:", self.corpusFacile["POS CS"], str((self.POS["CS"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS CS"]])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Congiunzioni Subordinanti:", self.corpusFacile["POS CS"], str((0.0)*100*1.0), self.corpusDifficile["POS CS"]])	
			
## 		if "CC" in self.POS:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Congiunzioni Coordinanti:", self.corpusFacile["POS CC"], str((self.POS["CC"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS CC"]])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Congiunzioni Coordinanti:", self.corpusFacile["POS CC"], str((0.0)*100*1.0), self.corpusDifficile["POS CC"]])	
## 		if "CS" in self.POS:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Congiunzioni Subordinanti:", self.corpusFacile["POS CS"], str((self.POS["CS"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS CS"]])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Congiunzioni Subordinanti:", self.corpusFacile["POS CS"], str((0.0)*100*1.0), self.corpusDifficile["POS CS"]])
		
## 		tag="Verbi "
## 		print >> self.fileOutput,  'Verbi+Modo:'
## 		for posMfeat in self.VERBI_MODO:
## 			print >> self.fileOutput,  '\t'.join([tag+posMfeat, str((self.VERBI_MODO[posMfeat]/self.verbTOT)*100*1.0)])
## 		print >> self.fileOutput,  'Verbi+Tempo:'
## 		for posMfeat in self.VERBI_TEN:
## 			print >> self.fileOutput,  '\t'.join([tag+posMfeat, str((self.VERBI_TEN[posMfeat]/self.verbiTempo)*100*1.0)])
## 		print >> self.fileOutput,  'Verbi+Numero+Persona:'
## 		for posMfeat in self.VERBI_NUM_PERSON:
## 			print >> self.fileOutput,  '\t'.join([tag+posMfeat, str((self.VERBI_NUM_PERSON[posMfeat]/self.verbiNumPer)*100*1.0)])

		###Calcolo il numero di Frasi Subordinate:
		FrasiSubordinate=0.0
		SubordinateInfinitive=0.0
		SubordinateCompletive=0.0
		#print >> self.fileOutput,  "******************"
		for arg in self.ARG_Special:
			FrasiSubordinate+=self.ARG_Special[arg]
			if arg in ["V-->V", "V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.ARG_Special[arg]
			elif arg=="V-->CS-->V":
				SubordinateCompletive+=self.ARG_Special[arg]
		for mod in self.MOD_Special:
			FrasiSubordinate+=self.MOD_Special[mod]
			if mod in ["V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.MOD_Special[mod]
			elif mod in ["V-->CS-->V", "V-->B-->V"]:
				SubordinateCompletive+=self.MOD_Special[mod]
		for comp in self.COMP_Special:
			if comp in ["V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.COMP_Special[comp]
			FrasiSubordinate+=self.COMP_Special[comp]
		### FINE Calcolo il numero di Frasi Subordinate ------------

		print >> self.fileOutput
		#print >> self.fileOutput,  '\t'.join(['Totale Teste Verbali:', str(self.verbCount)])
		print >> self.fileOutput, "<i>Struttura sintattica a dipendenze"
		#print >> self.fileOutput
		print >> self.fileOutput, "<t>Articolazione interna del periodo:"
		print >> self.fileOutput,  '\t'.join(['<t><t>Numero medio di proposizioni per periodo:', str(self.verbCount/self.sentCount)])
		print >> self.fileOutput, "<t><t>Proposizioni principali vs subordinate (%)"
		if (FrasiSubordinate+self.RadiciVerbali):
			print >> self.fileOutput,  '\t'.join(['<t><t><t>Principali:', str((self.RadiciVerbali/(FrasiSubordinate+self.RadiciVerbali))*100*1.0)])
			print >> self.fileOutput,  '\t'.join(['<t><t><t>Subordinate:', str((FrasiSubordinate/(FrasiSubordinate+self.RadiciVerbali))*100*1.0)])
		else:
			print >> self.fileOutput,  '\t'.join(['<t><t><t>Principali:',  str(0.0) ])
			print >> self.fileOutput,  '\t'.join(['<t><t><t>Subordinate:', str(0.0)])
		#print >> self.fileOutput,  '\t'.join(['Totale Radici Verbali:', str(self.RadiciVerbali)])
		#if (self.TOT_ROOT):
			#print >> self.fileOutput,  '\t'.join(['Sintassi - Percentuale di Radici Verbali:', str(self.RadiciVerbali/self.TOT_ROOT)])
		#else:
			#print >> self.fileOutput,  '\t'.join(['Sintassi - Percentuale di Radici Verbali:', str(self.RadiciVerbali/1.0)])
## 		if self.RadiciVerbali:
## 			print >> self.fileOutput,  '\t'.join(['Sintassi - Percentuale di Radici Verbali con Soggetto esplicito:', str(self.RadiciVerbaliConSogg/self.RadiciVerbali*100*1.0)])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(['Sintassi - Percentuale di Radici Verbali con Soggetto esplicito:', str(0.0)])
		print >> self.fileOutput, "<t>Articolazione interna della proposizione:"
		if self.verbCount:
			print >> self.fileOutput,  '\t'.join(['<t><t>Numero medio di parole per proposizione:', str((self.tokenCount/self.verbCount)*1.0)])
		else:
			print >> self.fileOutput,  '\t'.join(['<t><t>Numero medio di parole per proposizione:', str(0.0)])
		#print >> self.fileOutput,  '\t'.join(['Totale Archi Entranti in Teste Verbali:', str(self.ContaLinkEntranti)])
		if self.verbCount:
			print >> self.fileOutput,  '\t'.join(['<t><t>Numero medio di dipendenti per testa verbale:', str(self.ContaLinkEntranti/self.verbCount)])
		else:
			print >> self.fileOutput,  '\t'.join(['<t><t>Numero medio di dipendenti per testa verbale:', str(0.0)])
		#print >> self.fileOutput,  '\t'.join(['Totale catene preposizionali:', str(self.TOT_Catene)])
		print >> self.fileOutput, '<t>"Misura" della profondità dell\'albero sintattico:'.decode('utf-8')
		print >> self.fileOutput,  '\t'.join(['<t><t>Media delle altezze massime:', str(self.altezzaMax/self.sentCount)])
		
		if self.TOT_Catene==0.0:
			self.TOT_Catene=1.0
		print >> self.fileOutput,  '\t'.join(['<t><t>Profondità media di strutture nominali complesse:'.decode('utf-8'), str(self.lunghezzaCatena/self.TOT_Catene)])
		if self.TOT_Subordinate==0.0:
			self.TOT_Subordinate=1.0
		print >> self.fileOutput,  '\t'.join(['<t><t>Profondità media di "catene" di subordinazione:'.decode('utf-8'), str(self.altezzaSubordinantiTOT/self.TOT_Subordinate)])
		print >> self.fileOutput, '<t>"Misura" della lunghezza delle relazioni di dipendenza (calcolata come distanza in parole tra testa e dipendente):'
		
		if self.TotaleValidiPerLunghezzaLink:
			print >> self.fileOutput,  '\t'.join(['<t><t>Lunghezza media:', str(self.lunghezzaLink/self.TotaleValidiPerLunghezzaLink)])
		else:
			print >> self.fileOutput,  '\t'.join(['<t><t>Lunghezza media:', str(0.0)])
			
		print >> self.fileOutput,  '\t'.join(['<t><t>Media delle lunghezze massime:', str(self.lunghezzaLinkMax/(self.sentCount))])
	
		#QUIIIITOT_Subordinate, altezzaSUB, self.ALTEZZASub
		#print >> self.fileOutput,  '\t'.join(['Totale Strutture Subordinate:', str(self.TOT_Subordinate)])
		
		#print >> self.fileOutput, 
## 		print >> self.fileOutput,  'Tipo di DIPENDENZA:'
## 		for dep in self.DEP:
## 			tag="DIP "
## 			print >> self.fileOutput,  '\t'.join([tag+dep,  str((self.DEP[dep]/self.tokenCount)*100*1.0), str(self.DEP[dep])])
## 		print >> self.fileOutput, 
## 		print >> self.fileOutput, 
## 		print >> self.fileOutput,  'Archi entranti in Teste Verbali:'
## 		for linkE in LINK_ENTRANTI:
## 			tag="ARCH "
## 			if self.verbCount:
## 				print >> self.fileOutput,  '\t'.join([tag+str(linkE), str((LINK_ENTRANTI[linkE]/self.verbCount)*100*1.0), str(LINK_ENTRANTI[linkE])])
## 			else:
## 				print >> self.fileOutput,  '\t'.join([tag+str(linkE), str(0.0), str(LINK_ENTRANTI[linkE])])

## 		print >> self.fileOutput, 
## 		print >> self.fileOutput,  'Catene PREP:'
## 		for catena in CATENE_PREP:
## 			tag="PREP "
## 			print >> self.fileOutput,  '\t'.join([tag+str(catena), str((CATENE_PREP[catena]/self.TOT_Catene)*100*1.0), str(CATENE_PREP[catena])])

## 		#QUII
## 		print >> self.fileOutput, 
## 		print >> self.fileOutput,  'Catene SUBORD:'
## 		for subordinate in self.ALTEZZASub:
## 			tag="SUB "
## 			print >> self.fileOutput,  '\t'.join([tag+str(subordinate), str((self.ALTEZZASub[subordinate]/self.TOT_Subordinate)*100*1.0), str(self.ALTEZZASub[subordinate])])

		#print >> self.fileOutput, 
		#print >> self.fileOutput,  'Divisioni Frasi:'
		#print >> self.fileOutput 
## 		if FrasiSubordinate==0.0:
## 			FrasiSubordinate=1.0
## #
# 		print >> self.fileOutput,  '\t'.join(["Sintassi - subordinate percentuale di Completive", str((SubordinateCompletive/FrasiSubordinate)*100*1.0)])
## 		print >> self.fileOutput,  '\t'.join(["Sintassi - subordinate percentuale di Infinitive", str((SubordinateInfinitive/FrasiSubordinate)*100*1.0)])

		#print >> self.fileOutput 
## 		if not ('subord_pre' in self.DirezioneDipendenze):
## 			self.DirezioneDipendenze['subord_pre']=0.0
## 		if not ('subord_post' in self.DirezioneDipendenze):
## 			self.DirezioneDipendenze['subord_post']=0.0


## 		#print >> self.fileOutput,  'Distribuzione posizionale delle frasi Subordinate rispetto alle Principali:'
## 		if not(self.DirezioneDipendenze['subord_pre']+self.DirezioneDipendenze['subord_post']==0):
## 			print >> self.fileOutput,  '\t'.join(['Subordinate che precedono la frase principale:', self.corpusFacile['Subordinate che precedono la frase principale'], str((self.DirezioneDipendenze['subord_pre']/(self.DirezioneDipendenze['subord_pre']+self.DirezioneDipendenze['subord_post']))*100*1.0), self.corpusDifficile['Subordinate che precedono la frase principale']])
## 			print >> self.fileOutput,  '\t'.join(['Subordinate che seguono la frase principale:', self.corpusFacile['Subordinate che seguono la frase principale'], str((self.DirezioneDipendenze['subord_post']/(self.DirezioneDipendenze['subord_pre']+self.DirezioneDipendenze['subord_post']))*100*1.0), self.corpusDifficile['Subordinate che seguono la frase principale']])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(['Subordinate che precedono la frase principale:', self.corpusFacile['Subordinate che precedono la frase principale'], str(0), self.corpusDifficile['Subordinate che precedono la frase principale']])
## 			print >> self.fileOutput,  '\t'.join(['Subordinate che seguono la frase principale:', self.corpusFacile['Subordinate che seguono la frase principale'], str(0), self.corpusDifficile['Subordinate che seguono la frase principale']])
		print >> self.fileOutput
		print >> self.fileOutput

	#NUOVO HTML:
	def stampaStatisticheNoConfrontoHTMLandrea(self, documento, indice):
		if not(self.divisioneDoc):
			indice="Globale"
			documento="Globale"
		docTemplate = """
		<div class="accordion-group">
		<div class="accordion-heading">
		<a class="accordion-toggle" data-toggle="collapse"
		data-parent="#accordionmain"  href="#collapsedoc%(indice)s">
		Statistiche Documento: %(documento)s
		</a>
		</div>
		<div id="collapsedoc%(indice)s" class="accordion-body collapse">
		<div class="accordion-inner">
		<div class="accordion" id="accordion_detail_doc%(indice)s">
		<div class="accordion-group">
		<div class="accordion-heading">
		<a class="accordion-toggle" data-toggle="collapse"
		href="#collapse_dati_generali_doc%(indice)s">
		Profilo di base
		</a>
		</div>
		<div id="collapse_dati_generali_doc%(indice)s"
		class="accordion-body collapse">
		<div class="accordion-inner">
		<table class="table table-striped">
		<tr>
		<td>
		Numero totale periodi
		</td>
		<td>
		%(sentCount)s
		</td>
		</tr>
		<tr>
		<td>
		Numero totale parole (token):
		</td>
		<td>
		%(tokenCount)s
		</td>
		</tr>
		<tr>
		<td>
		Lunghezza media dei periodi (in token):
		</td>
		<td>
		%(tokenCount/sentCount).3f
		</td>
		</tr>
		<tr>
		<td>
		Lunghezza media delle parole (in caratteri)
		</td>
		<td>
		%(LunghezzaParole/TOT_Token).3f
		</td>
		</tr>
		</table>
		</div>
		</div>
		</div>
		<div class="accordion-group">
		<div class="accordion-heading">
		<a class="accordion-toggle" data-toggle="collapse"
		href="#collapse_profilo_lessicale_doc%(indice)s">
		Profilo lessicale
		</a>
		</div>
		<div id="collapse_profilo_lessicale_doc%(indice)s"
		class="accordion-body collapse in">
		<div class="accordion-inner">
		<table class="table table-striped">
		<tr>
		<td>
		Numero totale di lemmi diversi (compresa la punteggiatura)
		</td>
		<td>
		%(sentLemmi)s
		</td>
		</tr>
		<tr>
		<tr>
		<td colspan="2" style="font-style:italic;">
		Composizione del vocabolario
		</td>
		</tr>
		<tr>
		<td style="padding-left:1cm;">
		Percentuale di lemmi appartenente al Vocabolario di Base (VdB)
		</td>
		<td>
		%(len(DizionarioDeMauroType)/Lemmi).3f%%
		</td>
		</tr>
		<tr>
		<td  colspan="2" style="padding-left:1cm;font-style:italic;">
		Ripartizione dei lemmi riconducibili al VdB rispetto ai repertori d'uso:
		</td>
		</tr>
		<tr>
		<td style="padding-left:2cm;">
		Fondamentale
		</td>
		<td>
		%(fondamentale).3f%%
		</td>
		</tr>
		<tr>
		<td  style="padding-left:2cm;">
		Alto uso
		</td>
		<td>
		%(AltoUso).3f%%
		</td>
		</tr>
		<tr>
		<td  style="padding-left:2cm;">
		Alta disponibilità
		</td>
		<td>
		%(AltaDisponibilit).3f%%
		</td>
		</tr>
		<tr>
		<td>
		Densità Lessicale
		</td>
		<td>
		%(DensitLessicale).3f
		</td>
		</tr>
		<tr>
		<td>
		Rapporto tipo/unità (calcolato rispetto alle prime %(paroleTTR)s parole del testo):
		</td>
		<td>
		%(RapportoTipo/unit).3f
		</td>
		</tr>
		<tr>
		<td>
		Rapporto tipo/unità (calcolato rispetto a tutto il documento):
		</td>
		<td>
		%(RapportoTipo/unit_doc).3f
		</td>
		</tr>
		</table>
		</div>
		</div>
		</div>
		<div class="accordion-group">
		<div class="accordion-heading">
		<a class="accordion-toggle" data-toggle="collapse" href="#collapse_profilo_sintattico_doc%(indice)s">
		Profilo sintattico
		</a>
		</div>
		<div id="collapse_profilo_sintattico_doc%(indice)s" class="accordion-body collapse in">
		<div class="accordion-inner">
		<table class="table table-striped">
		<tr>
		<td colspan="2" style="font-style:italic;">
		"Misura" delle categorie morfo-sintattiche (%%)
		</td>
		</tr>
		<tr>
		<td style="padding-left:1cm;">
		Sostantivi:
		</td>
		<td>
		%(sostantivi).3f%%
		</td>
		</tr>
		<tr>
		<td style="padding-left:1cm;">
		Nomi Propri:
		</td>
		<td>
		%(NomiPropri).3f%%
		</td>
		</tr>
		<tr>
		<td style="padding-left:1cm;">
		Aggettivi:
		</td>
		<td>
		%(aggettivi).3f%%
		</td>
		</tr>
		<tr>
		<td style="padding-left:1cm;">
		Verbi:
		</td>
		<td>
		%(verbi).3f%%
		</td>
		</tr>
		<tr>
		<td style="padding-left:1cm;">
		Congiunzioni:
		</td>
		<td>
		%(Congiunzioni).3f%%
		</td>
		</tr>
		<tr>
		<td style="padding-left:2cm;">
		Coordinanti:
		</td>
		<td>
		%(Coordinanti).3f%%
		</td>
		</tr>
		<tr>
		<td style="padding-left:2cm;">
		Subordinanti:
		</td>
		<td>
		%(Subordinanti).3f%%
		</td>
		</tr>
		<tr>
		<td colspan="2" style="font-style:italic;">
		Struttura sintattica a dipendenze
		</td>
		</tr>
		<tr>
		<td colspan="2" style="padding-left:1cm;font-style:italic;">
		Articolazione interna del periodo:
		</td>
		</tr>
		<tr>
		<td style="padding-left:2cm;">
		Numero medio di proposizioni per periodo:
		</td>
		<td>
		%(proposizioniXperiodo).3f
		</td>
		</tr>
		<tr>
		<td colspan="2" style="padding-left:2cm;font-style:italic;">
		Proposizioni principali vs subordinate (%%)
		</td>
		</tr>
		<tr>
		<td style="padding-left:3cm;">
		Principali:
		</td>
		<td>
		%(Principali).3f%%
		</td>
		</tr>
		<tr>
		<td style="padding-left:3cm;">
		Subordinate:
		</td>
		<td>
		%(Subordinate).3f%%
		</td>
		</tr>
		<tr>
		<td colspan="2" style="padding-left:1cm;font-style:italic;">
		Articolazione interna della proposizione:
		</td>
		</tr>
		<tr>
		<td style="padding-left:2cm;">
		Numero medio di parole per proposizione:
		</td>
		<td>
		%(ParoleXPrep).3f
		</td>
		</tr>
		<tr>
		<td style="padding-left:2cm;">
		Numero medio di dipendenti per testa verbale:
		</td>
		<td>
		%(MLinkEntranti).3f
		</td>
		</tr>
		<tr>
		<td colspan="2" style="padding-left:1cm;font-style:italic;">
		"Misura" della profondità dell\'albero sintattico:
		</td>
		</tr>
		<tr>
		<td style="padding-left:2cm;">
		Media delle altezze massime:
		</td>
		<td>
		%(MaltezzaMax).3f
		</td>
		</tr>
		<tr>
		<td style="padding-left:2cm;">
		Profondità media di strutture nominali complesse:
		</td>
		<td>
		%(McatenePrep).3f
		</td>
		</tr>
		<tr>
		<td style="padding-left:2cm;">
		Profondità media di "catene" di subordinazione:
		</td>
		<td>
		%(McateneSubord).3f
		</td>
		</tr>
		<tr>
		<td colspan="2" style="padding-left:1cm;font-style:italic;">
		"Misura" della lunghezza delle relazioni di dipendenza (calcolata come distanza in parole tra testa e dipendente):
		</td>
		</tr>
		<tr>
		<td style="padding-left:2cm;">
		Lunghezza media:
		</td>
		<td>
		%(MediaLink).3f
		</td>
		</tr>
		<tr>
		<td style="padding-left:2cm;">
		Media delle lunghezze massime:
		</td>
		<td>
		%(MediaLinkMax).3f
		</td>
		</tr>
		
		</table>
		</div>
		</div>
		</div>

      </div>
    </div>
  </div>
</div>

		
		""".decode("utf-8")
		
		if not(self.DizionarioDeMauroType):
			denominatore=1.0
		else:
			denominatore=len(self.DizionarioDeMauroType)

		if self.tokenCount>self.rangeType2:
			paroleTTR=self.rangeType2
		else:
			paroleTTR=self.tokenCount
			
		if "S" in self.POS:
		        sostantivi=(self.POS["S"]/self.tokenCount)*100*1.0
		else:
			sostantivi=(0.0)*100*1.0
		if "SP" in self.POS:
	                NomiPropri=(self.POS["SP"]/self.tokenCount)*100*1.0
		else:
			NomiPropri=(0.0)*100*1.0

		if "A" in self.CPOS:
                        aggettivi=(self.CPOS["A"]/self.tokenCount)*100*1.0
		else:
			aggettivi=(0.0)*100*1.0
		if "V" in self.CPOS:
	                verbi=(self.CPOS["V"]/self.tokenCount)*100*1.0
		else:
 			verbi=(0.0)*100*1.0
		
		Congiunzioni=(0.0)*100*1.0
		Coordinanti=(0.0)*100*1.0
		Subordinanti=(0.0)*100*1.0
		if "C" in self.CPOS:
		        Congiunzioni=(self.CPOS["C"]/self.tokenCount)*100*1.0
			TotaleDivisione=0.0
			if "CC" in self.POS:
				TotaleDivisione+=self.POS["CC"]
			if "CS" in self.POS:
				TotaleDivisione+=self.POS["CS"]
			if "CC" in self.POS:
				Coordinanti=(self.POS["CC"]/(TotaleDivisione)*1.0)*100*1.0
			if "CS" in self.POS:
				Subordinanti=(self.POS["CS"]/(TotaleDivisione)*1.0)*100*1.0
		
		###Calcolo il numero di Frasi Subordinate:
		FrasiSubordinate=0.0
		SubordinateInfinitive=0.0
		SubordinateCompletive=0.0
		for arg in self.ARG_Special:
			FrasiSubordinate+=self.ARG_Special[arg]
			if arg in ["V-->V", "V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.ARG_Special[arg]
			elif arg=="V-->CS-->V":
				SubordinateCompletive+=self.ARG_Special[arg]
		for mod in self.MOD_Special:
			FrasiSubordinate+=self.MOD_Special[mod]
			if mod in ["V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.MOD_Special[mod]
			elif mod in ["V-->CS-->V", "V-->B-->V"]:
				SubordinateCompletive+=self.MOD_Special[mod]
		for comp in self.COMP_Special:
			if comp in ["V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.COMP_Special[comp]
			FrasiSubordinate+=self.COMP_Special[comp]
		### FINE Calcolo il numero di Frasi Subordinate ------------

		Principali=0.0
		Subordinate=0.0
		if (FrasiSubordinate+self.RadiciVerbali):
		        Principali=(self.RadiciVerbali/(FrasiSubordinate+self.RadiciVerbali))*100*1.0
			Subordinate=(FrasiSubordinate/(FrasiSubordinate+self.RadiciVerbali))*100*1.0 

		ParoleXPrep=0.0
		MLinkEntranti=0.0
		if self.verbCount:
	                ParoleXPrep=(self.tokenCount/self.verbCount)*1.0
			MLinkEntranti=self.ContaLinkEntranti/self.verbCount
		
		if self.TOT_Catene==0.0:
			self.TOT_Catene=1.0
		if self.TOT_Subordinate==0.0:
			self.TOT_Subordinate=1.0
		
		if self.TOT_Token==0.0:
			self.TOT_Token=1.0

		MediaLink=0.0
		MediaLinkMax=0.0
		if self.TotaleValidiPerLunghezzaLink:
			MediaLink=self.lunghezzaLink/self.TotaleValidiPerLunghezzaLink
		MediaLinkMax=self.lunghezzaLinkMax/(self.sentCount)
		
		

		#err 2306:
		#print 
		#print "self.sentCount", self.sentCount
		#print "denominatore", denominatore
		#print "paroleTTR", paroleTTR
		#print "self.tokenCount", self.tokenCount
		#print "TOT_Token", self.TOT_Token
		#print "TOT_Catene", self.TOT_Catene
		#print "self.TOT_Subordinate", self.TOT_Subordinate
		#################
		
		Risultati={"documento":documento, "indice": indice, "sentCount": self.sentCount, "sentLemmi": len(self.Lemmi), "tokenCount": self.tokenCount, "tokenCount/sentCount": self.tokenCount/self.sentCount, "LunghezzaParole/TOT_Token": self.LunghezzaParole/self.TOT_Token, "len(DizionarioDeMauroType)/Lemmi": (len(self.DizionarioDeMauroType)*1.0/len(self.Lemmi)*1.0)*100, "fondamentale": (self.DizFondamentaleType["FO"]/denominatore)*100*1.0, "AltaDisponibilit": (self.DizFondamentaleType["AD"]/denominatore)*100*1.0, "AltoUso": (self.DizFondamentaleType["AU"]/denominatore)*100*1.0, "paroleTTR":paroleTTR, "RapportoTipo/unit": len(self.LemmiTT2)*1.0/paroleTTR*1.0, "RapportoTipo/unit_doc": len(self.Lemmi)*1.0/self.tokenCount*1.0, "DensitLessicale": self.ParolePiene/self.TOT_Token, "sostantivi":sostantivi, "NomiPropri": NomiPropri, "aggettivi": aggettivi, "verbi":verbi, "Congiunzioni":Congiunzioni, "Coordinanti":Coordinanti, "Subordinanti":Subordinanti, "proposizioniXperiodo": self.verbCount/self.sentCount, "Principali": Principali, "Subordinate": Subordinate, "MLinkEntranti": MLinkEntranti, "MaltezzaMax":self.altezzaMax/self.sentCount, "McatenePrep": self.lunghezzaCatena/self.TOT_Catene, "McateneSubord": self.altezzaSubordinantiTOT/self.TOT_Subordinate, "MediaLink": MediaLink, "MediaLinkMax":MediaLinkMax, "ParoleXPrep": ParoleXPrep}

		print >> self.fileOutput, docTemplate % Risultati
		

	#NUOVO ritorna La struttura Dati:
	def calcolaStatistiche(self, documento, indice):
		if not(self.divisioneDoc):
			indice="Globale"
			documento="Globale"
				
		if not(self.DizionarioDeMauroType):
			denominatore=1.0
		else:
			denominatore=len(self.DizionarioDeMauroType)

		if self.tokenCount>self.rangeType2:
			paroleTTR=self.rangeType2
		else:
			paroleTTR=self.tokenCount
			
		if "S" in self.POS:
		        sostantivi=(self.POS["S"]/self.tokenCount)*100*1.0
		else:
			sostantivi=(0.0)*100*1.0
		if "SP" in self.POS:
	                NomiPropri=(self.POS["SP"]/self.tokenCount)*100*1.0
		else:
			NomiPropri=(0.0)*100*1.0

		if "A" in self.CPOS:
                        aggettivi=(self.CPOS["A"]/self.tokenCount)*100*1.0
		else:
			aggettivi=(0.0)*100*1.0
		if "V" in self.CPOS:
	                verbi=(self.CPOS["V"]/self.tokenCount)*100*1.0
		else:
 			verbi=(0.0)*100*1.0
		
		Congiunzioni=(0.0)*100*1.0
		Coordinanti=(0.0)*100*1.0
		Subordinanti=(0.0)*100*1.0
		if "C" in self.CPOS:
		        Congiunzioni=(self.CPOS["C"]/self.tokenCount)*100*1.0
			TotaleDivisione=0.0
			if "CC" in self.POS:
				TotaleDivisione+=self.POS["CC"]
			if "CS" in self.POS:
				TotaleDivisione+=self.POS["CS"]
			if "CC" in self.POS:
				Coordinanti=(self.POS["CC"]/(TotaleDivisione)*1.0)*100*1.0
			if "CS" in self.POS:
				Subordinanti=(self.POS["CS"]/(TotaleDivisione)*1.0)*100*1.0
		
		###Calcolo il numero di Frasi Subordinate:
		FrasiSubordinate=0.0
		SubordinateInfinitive=0.0
		SubordinateCompletive=0.0
		for arg in self.ARG_Special:
			FrasiSubordinate+=self.ARG_Special[arg]
			if arg in ["V-->V", "V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.ARG_Special[arg]
			elif arg=="V-->CS-->V":
				SubordinateCompletive+=self.ARG_Special[arg]
		for mod in self.MOD_Special:
			FrasiSubordinate+=self.MOD_Special[mod]
			if mod in ["V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.MOD_Special[mod]
			elif mod in ["V-->CS-->V", "V-->B-->V"]:
				SubordinateCompletive+=self.MOD_Special[mod]
		for comp in self.COMP_Special:
			if comp in ["V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.COMP_Special[comp]
			FrasiSubordinate+=self.COMP_Special[comp]
		### FINE Calcolo il numero di Frasi Subordinate ------------

		Principali=0.0
		Subordinate=0.0
		if (FrasiSubordinate+self.RadiciVerbali):
		        Principali=(self.RadiciVerbali/(FrasiSubordinate+self.RadiciVerbali))*100*1.0
			Subordinate=(FrasiSubordinate/(FrasiSubordinate+self.RadiciVerbali))*100*1.0 

		ParoleXPrep=0.0
		MLinkEntranti=0.0
		if self.verbCount:
	                ParoleXPrep=(self.tokenCount/self.verbCount)*1.0
			MLinkEntranti=self.ContaLinkEntranti/self.verbCount
		
		if self.TOT_Catene==0.0:
			self.TOT_Catene=1.0
		if self.TOT_Subordinate==0.0:
			self.TOT_Subordinate=1.0

		MediaLink=0.0
		MediaLinkMax=0.0
		if self.TotaleValidiPerLunghezzaLink:
			MediaLink=self.lunghezzaLink/self.TotaleValidiPerLunghezzaLink
		MediaLinkMax=self.lunghezzaLinkMax/(self.sentCount)
		
		Risultati={"sentCount": self.sentCount, "sentLemmi": len(self.Lemmi), "tokenCount": self.tokenCount, "percTokenSentence": self.tokenCount/self.sentCount, "percCaratteriParole": self.LunghezzaParole/self.TOT_Token, "percLemmiDizionarioDeMauro": (len(self.DizionarioDeMauroType)*1.0/len(self.Lemmi)*1.0)*100, "fondamentale": (self.DizFondamentaleType["FO"]/denominatore)*100*1.0, "AltaDisponibilit": (self.DizFondamentaleType["AD"]/denominatore)*100*1.0, "AltoUso": (self.DizFondamentaleType["AU"]/denominatore)*100*1.0, "TTR": len(self.LemmiTT2)*1.0/paroleTTR*1.0, "TTR_tot": len(self.Lemmi)*1.0/self.tokenCount*1.0, "DensitLessicale": self.ParolePiene/self.TOT_Token, "sostantivi":sostantivi, "NomiPropri": NomiPropri, "aggettivi": aggettivi, "verbi":verbi, "Congiunzioni":Congiunzioni, "Coordinanti":Coordinanti, "Subordinanti":Subordinanti, "proposizioniXperiodo": self.verbCount/self.sentCount, "Principali": Principali, "Subordinate": Subordinate, "MLinkEntranti": MLinkEntranti, "MaltezzaMax":self.altezzaMax/self.sentCount, "McatenePrep": self.lunghezzaCatena/self.TOT_Catene, "McateneSubord": self.altezzaSubordinantiTOT/self.TOT_Subordinate, "MediaLink": MediaLink, "MediaLinkMax":MediaLinkMax, "ParoleXPrep": ParoleXPrep}
		
		return Risultati

	###############################FINE NUOVO


	def stampaStatisticheConfronto(self, documento):

		print >> self.fileOutput,  '\t'.join(["##########", "##########", "##########", "##########", "##########"])
		print >> self.fileOutput,  '\t'.join(["Statistiche Documento:", str(documento)])
		print >> self.fileOutput 
		print >> self.fileOutput,  '<b>Dati generali'
		print >> self.fileOutput
		#print >> self.fileOutput,  '\t'.join(['Numero totale periodi:', self.corpusFacile['Totale Frasi:'], str(self.sentCount), self.corpusDifficile['Totale Frasi:'] ])
		#print >> self.fileOutput,  '\t'.join(['Numero totale parole (token):', self.corpusFacile['Totale Token:'], str(self.tokenCount), self.corpusDifficile['Totale Token:']])
		print >> self.fileOutput,  '\t'.join(['Numero totale periodi:', str(self.sentCount)])
		print >> self.fileOutput,  '\t'.join(['Numero totale parole (token):', str(self.tokenCount)])
		print >> self.fileOutput,  '\t'.join(['Lunghezza media dei periodi (in token):', self.corpusFacile['Numero di Token per Frase:'], str(self.tokenCount/self.sentCount), self.corpusDifficile['Numero di Token per Frase:']])
		print >> self.fileOutput,  '\t'.join(['Lunghezza media delle parole (in caratteri):', self.corpusFacile['Numero di Caratteri per Token (esclusa la punteggiatura):'], str(self.LunghezzaParole/self.TOT_Token), self.corpusDifficile['Numero di Caratteri per Token (esclusa la punteggiatura):']])
		print >> self.fileOutput
		print >> self.fileOutput
		print >> self.fileOutput,  '<b>Profilo lessicale'
		print >> self.fileOutput
		print >> self.fileOutput, '<i>Composizione del vocabolario'
		#print >> self.fileOutput
## 		print >> self.fileOutput,  '\t'.join(['Dizionario - percentuale delle parole ritrovate:', str((TrovateInDizionario/self.TOT_Token)*100*1.0)])
## 		for classe in DizFondamentale:
## 			tag="DizF  "
## 			if TrovateInDizionario:
## 				print >> self.fileOutput,  '\t'.join([tag+classe, str((DizFondamentale[classe]/TrovateInDizionario)*100*1.0)])
## 			else:
## 				print >> self.fileOutput,  '\t'.join([tag+classe, str((DizFondamentale[classe]/1.0)*100*1.0)])	

		
		print >> self.fileOutput,  '\t'.join(['<t>Percentuale di lemmi appartenente al Vocabolario di Base (VdB):', self.corpusFacile['Percentuale di lemmi trovati nel dizionario di base:'], str((len(self.DizionarioDeMauroType)*1.0/len(self.Lemmi)*1.0)*100), self.corpusDifficile['Percentuale di lemmi trovati nel dizionario di base:']])
		tag="DizL  "
		classe="FO"
		if not(self.DizionarioDeMauroType):
			denominatore=1.0
		else:
			denominatore=len(self.DizionarioDeMauroType)
		print >> self.fileOutput, "<t>Ripartizione dei lemmi riconducibili al VdB rispetto ai repertori d'uso:"
		chiave="<t><t>Fondamentale:"
		print >> self.fileOutput,  '\t'.join([chiave, self.corpusFacile[tag+classe+'t'], str((self.DizFondamentaleType[classe]/denominatore)*100*1.0), self.corpusDifficile[tag+classe+'t']])
		classe="AU"
		chiave="<t><t>Alto uso:"
		print >> self.fileOutput,  '\t'.join([chiave, self.corpusFacile[tag+classe+'t'], str((self.DizFondamentaleType[classe]/denominatore)*100*1.0), self.corpusDifficile[tag+classe+'t']])
		classe="AD"
		chiave="<t><t>Alta disponibilità:"
		print >> self.fileOutput,  '\t'.join([chiave.decode('utf-8'), self.corpusFacile[tag+classe+'t'], str((self.DizFondamentaleType[classe]/denominatore)*100*1.0), self.corpusDifficile[tag+classe+'t']])
		print >> self.fileOutput
		#print >> self.fileOutput,  '\t'.join(['Range '+str(self.rangeType)+' Type(forme)/token:', self.corpusFacile['Range '+str(self.rangeType)+' Type(forme)/token:'], str(len(self.FormeTT)/self.rangeType), self.corpusDifficile['Range '+str(self.rangeType)+' Type(forme)/token:']])
		#print >> self.fileOutput,  '\t'.join(['Range '+str(self.rangeType)+' Type(lemmi)/token:', self.corpusFacile['Range '+str(self.rangeType)+' Type(lemmi)/token:'], str(len(self.LemmiTT)/self.rangeType), self.corpusDifficile['Range '+str(self.rangeType)+' Type(lemmi)/token:']])
		#print >> self.fileOutput,  '\t'.join(['Range '+str(self.rangeType2)+' Type(forme)/token:', self.corpusFacile['Range '+str(self.rangeType2)+' Type(forme)/token:'], str(len(self.FormeTT2)/self.rangeType2), self.corpusDifficile['Range '+str(self.rangeType2)+' Type(forme)/token:']])
		#print >> self.fileOutput,  '\t'.join(['Range '+str(self.rangeType2)+' Type(lemmi)/token:', self.corpusFacile['Range '+str(self.rangeType2)+' Type(lemmi)/token:'], str(len(self.LemmiTT2)/self.rangeType2), self.corpusDifficile['Range '+str(self.rangeType2)+' Type(lemmi)/token:']])
		if self.tokenCount>100:
			print >> self.fileOutput,  '\t'.join(["<i>Rapporto tipo/unità (calcolato rispetto alle prime 100 parole del testo):".decode('utf-8'), self.corpusFacile['Range '+str(self.rangeType2)+' Type(lemmi)/token:'], str(len(self.LemmiTT2)/self.rangeType2), self.corpusDifficile['Range '+str(self.rangeType2)+' Type(lemmi)/token:']])
		print >> self.fileOutput
		print >> self.fileOutput,  '\t'.join(['<i>Densità Lessicale:'.decode('utf-8'), self.corpusFacile['Densita Lessicale:'], str(self.ParolePiene/self.TOT_Token), self.corpusDifficile['Densita Lessicale:']])
		print >> self.fileOutput
		print >> self.fileOutput
		print >> self.fileOutput,  '<b>Profilo Sintattico'
		print >> self.fileOutput
		print >> self.fileOutput, '<i>"Misura" delle categorie morfo-sintattiche (%)'
		if "S" in self.POS:
			print >> self.fileOutput,  '\t'.join(["<t>Sostantivi:", self.corpusFacile["POS S"], str((self.POS["S"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS S"]])
		else:
			print >> self.fileOutput,  '\t'.join(["<t>Sostantivi:", self.corpusFacile["POS S"], str((0.0)*100*1.0), self.corpusDifficile["POS S"]])
		if "SP" in self.POS:
			print >> self.fileOutput,  '\t'.join(["<t>Nomi Propri:", self.corpusFacile["POS SP"], str((self.POS["SP"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS SP"]])
		else:
			print >> self.fileOutput,  '\t'.join(["<t>Nomi Propri:", self.corpusFacile["POS SP"], str((0.0)*100*1.0), self.corpusDifficile["POS SP"]])
		if "A" in self.CPOS:
			print >> self.fileOutput,  '\t'.join(["<t>Aggettivi:", self.corpusFacile["CPOS A"], str((self.CPOS["A"]/self.tokenCount)*100*1.0), self.corpusDifficile["CPOS A"]])
		else:
			print >> self.fileOutput,  '\t'.join(["<t>Aggettivi:", self.corpusFacile["CPOS A"], str((0.0)*100*1.0), self.corpusDifficile["CPOS A"]])
## 		if "V" in self.POS:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Verbi Principali:", self.corpusFacile["POS V"], str((self.POS["V"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS V"]])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Verbi Principali:", self.corpusFacile["POS V"], str((0.0)*100*1.0), self.corpusDifficile["POS V"]])
## 		if "VA" in self.POS:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Verbi Ausiliari:", self.corpusFacile["POS VA"], str((self.POS["VA"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS VA"]])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Verbi Ausiliari:", self.corpusFacile["POS VA"], str((0.0)*100*1.0), self.corpusDifficile["POS VA"]])
## 		if "VM" in self.POS:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Verbi Modali:", self.corpusFacile["POS VM"], str((self.POS["VM"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS VM"]])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Verbi Modali:", self.corpusFacile["POS VM"], str((0.0)*100*1.0), self.corpusDifficile["POS VM"]])
		if "V" in self.CPOS:
 			print >> self.fileOutput,  '\t'.join(["<t>Verbi:", self.corpusFacile["CPOS V"], str((self.CPOS["V"]/self.tokenCount)*100*1.0), self.corpusDifficile["CPOS V"]])
		else:
 			print >> self.fileOutput,  '\t'.join(["<t>Verbi:", self.corpusFacile["CPOS V"], str((0.0)*100*1.0), self.corpusDifficile["CPOS V"]])

		#Sistemare Coordinanti e Subordinanti in Difficile e Facile
		if "C" in self.CPOS:
 			print >> self.fileOutput,  '\t'.join(["<t>Congiunzioni:", self.corpusFacile["CPOS C"], str((self.CPOS["C"]/self.tokenCount)*100*1.0), self.corpusDifficile["CPOS C"]])
			TotaleDivisione=0.0
			if "CC" in self.POS:
				TotaleDivisione+=self.POS["CC"]
			if "CS" in self.POS:
				TotaleDivisione+=self.POS["CS"]
			if "CC" in self.POS:
				print >> self.fileOutput,  '\t'.join(["<t><t>Coordinanti:", self.corpusFacile["Rel_Coordinanti"], str((self.POS["CC"]/(TotaleDivisione)*1.0)*100*1.0), self.corpusDifficile["Rel_Coordinanti"]])
			else:
				print >> self.fileOutput,  '\t'.join(["<t><t>Coordinanti:", self.corpusFacile["Rel_Coordinanti"], str((0.0)*100*1.0), self.corpusDifficile["Rel_Coordinanti"]])
			if "CS" in self.POS:
				print >> self.fileOutput,  '\t'.join(["<t><t>Subordinanti:", self.corpusFacile["Rel_Subordinanti"], str((self.POS["CS"]/(TotaleDivisione)*1.0)*100*1.0), self.corpusDifficile["Rel_Subordinanti"]])
			else:
				print >> self.fileOutput,  '\t'.join(["<t><t>Subordinanti:", self.corpusFacile["Rel_Subordinanti"], str((0.0)*100*1.0), self.corpusDifficile["Rel_Subordinanti"]])
 		else:
 			print >> self.fileOutput,  '\t'.join(["<t>Congiunzioni:", self.corpusFacile["CPOS C"], str((0.0)*100*1.0), self.corpusDifficile["CPOS C"]])
			print >> self.fileOutput,  '\t'.join(["<t><t>Coordinanti:", self.corpusFacile["POS CC"], str((0.0)*100*1.0), self.corpusDifficile["POS CC"]])
			print >> self.fileOutput,  '\t'.join(["<t><t>Subordinanti:", self.corpusFacile["POS CS"], str((0.0)*100*1.0), self.corpusDifficile["POS CS"]])
		
## 		if "CS" in self.POS:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Congiunzioni Subordinanti:", self.corpusFacile["POS CS"], str((self.POS["CS"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS CS"]])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Congiunzioni Subordinanti:", self.corpusFacile["POS CS"], str((0.0)*100*1.0), self.corpusDifficile["POS CS"]])	
			
## 		if "CC" in self.POS:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Congiunzioni Coordinanti:", self.corpusFacile["POS CC"], str((self.POS["CC"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS CC"]])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Congiunzioni Coordinanti:", self.corpusFacile["POS CC"], str((0.0)*100*1.0), self.corpusDifficile["POS CC"]])	
## 		if "CS" in self.POS:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Congiunzioni Subordinanti:", self.corpusFacile["POS CS"], str((self.POS["CS"]/self.tokenCount)*100*1.0), self.corpusDifficile["POS CS"]])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(["Percentuale di Congiunzioni Subordinanti:", self.corpusFacile["POS CS"], str((0.0)*100*1.0), self.corpusDifficile["POS CS"]])
		
## 		tag="Verbi "
## 		print >> self.fileOutput,  'Verbi+Modo:'
## 		for posMfeat in self.VERBI_MODO:
## 			print >> self.fileOutput,  '\t'.join([tag+posMfeat, str((self.VERBI_MODO[posMfeat]/self.verbTOT)*100*1.0)])
## 		print >> self.fileOutput,  'Verbi+Tempo:'
## 		for posMfeat in self.VERBI_TEN:
## 			print >> self.fileOutput,  '\t'.join([tag+posMfeat, str((self.VERBI_TEN[posMfeat]/self.verbiTempo)*100*1.0)])
## 		print >> self.fileOutput,  'Verbi+Numero+Persona:'
## 		for posMfeat in self.VERBI_NUM_PERSON:
## 			print >> self.fileOutput,  '\t'.join([tag+posMfeat, str((self.VERBI_NUM_PERSON[posMfeat]/self.verbiNumPer)*100*1.0)])

		###Calcolo il numero di Frasi Subordinate:
		FrasiSubordinate=0.0
		SubordinateInfinitive=0.0
		SubordinateCompletive=0.0
		#print >> self.fileOutput,  "******************"
		for arg in self.ARG_Special:
			FrasiSubordinate+=self.ARG_Special[arg]
			if arg in ["V-->V", "V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.ARG_Special[arg]
			elif arg=="V-->CS-->V":
				SubordinateCompletive+=self.ARG_Special[arg]
		for mod in self.MOD_Special:
			FrasiSubordinate+=self.MOD_Special[mod]
			if mod in ["V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.MOD_Special[mod]
			elif mod in ["V-->CS-->V", "V-->B-->V"]:
				SubordinateCompletive+=self.MOD_Special[mod]
		for comp in self.COMP_Special:
			if comp in ["V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.COMP_Special[comp]
			FrasiSubordinate+=self.COMP_Special[comp]
		### FINE Calcolo il numero di Frasi Subordinate ------------

		print >> self.fileOutput
		#print >> self.fileOutput,  '\t'.join(['Totale Teste Verbali:', str(self.verbCount)])
		print >> self.fileOutput, "<i>Struttura sintattica a dipendenze"
		#print >> self.fileOutput
		print >> self.fileOutput, "<t>Articolazione interna del periodo:"
		print >> self.fileOutput,  '\t'.join(['<t><t>Numero medio di proposizioni per periodo:', self.corpusFacile['Media di Teste Verbali per frase:'], str(self.verbCount/self.sentCount), self.corpusDifficile['Media di Teste Verbali per frase:']])
		print >> self.fileOutput, "<t><t>Proposizioni principali vs subordinate (%)"
		if (FrasiSubordinate+self.RadiciVerbali):
			print >> self.fileOutput,  '\t'.join(['<t><t><t>Principali:', self.corpusFacile['Percentuale frasi Principali'],  str((self.RadiciVerbali/(FrasiSubordinate+self.RadiciVerbali))*100*1.0) , self.corpusDifficile['Percentuale frasi Principali']])
			print >> self.fileOutput,  '\t'.join(['<t><t><t>Subordinate:', self.corpusFacile['Percentuale frasi Subordinate'], str((FrasiSubordinate/(FrasiSubordinate+self.RadiciVerbali))*100*1.0)  , self.corpusDifficile['Percentuale frasi Subordinate']])
		else:
			print >> self.fileOutput,  '\t'.join(['<t><t><t>Principali:', self.corpusFacile['Percentuale frasi Principali'],  str(0.0) , self.corpusDifficile['Percentuale frasi Principali']])
			print >> self.fileOutput,  '\t'.join(['<t><t><t>Subordinate:', self.corpusFacile['Percentuale frasi Subordinate'], str(0.0)  , self.corpusDifficile['Percentuale frasi Subordinate']])
		#print >> self.fileOutput,  '\t'.join(['Totale Radici Verbali:', str(self.RadiciVerbali)])
		#if (self.TOT_ROOT):
			#print >> self.fileOutput,  '\t'.join(['Sintassi - Percentuale di Radici Verbali:', str(self.RadiciVerbali/self.TOT_ROOT)])
		#else:
			#print >> self.fileOutput,  '\t'.join(['Sintassi - Percentuale di Radici Verbali:', str(self.RadiciVerbali/1.0)])
## 		if self.RadiciVerbali:
## 			print >> self.fileOutput,  '\t'.join(['Sintassi - Percentuale di Radici Verbali con Soggetto esplicito:', str(self.RadiciVerbaliConSogg/self.RadiciVerbali*100*1.0)])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(['Sintassi - Percentuale di Radici Verbali con Soggetto esplicito:', str(0.0)])
		print >> self.fileOutput, "<t>Articolazione interna della proposizione:"
		if self.verbCount:
			print >> self.fileOutput,  '\t'.join(['<t><t>Numero medio di parole per proposizione:', self.corpusFacile['Numero di token per Clausola:'], str((self.tokenCount/self.verbCount)*1.0), self.corpusDifficile['Numero di token per Clausola:']])
		else:
			print >> self.fileOutput,  '\t'.join(['<t><t>Numero medio di parole per proposizione:', self.corpusFacile['Numero di token per Clausola:'], str(0.0), self.corpusDifficile['Numero di token per Clausola:']])
		#print >> self.fileOutput,  '\t'.join(['Totale Archi Entranti in Teste Verbali:', str(self.ContaLinkEntranti)])
		if self.verbCount:
			print >> self.fileOutput,  '\t'.join(['<t><t>Numero medio di dipendenti per testa verbale:', self.corpusFacile['Media di Archi Entranti in Teste Verbali:'], str(self.ContaLinkEntranti/self.verbCount), self.corpusDifficile['Media di Archi Entranti in Teste Verbali:']])
		else:
			print >> self.fileOutput,  '\t'.join(['<t><t>Numero medio di dipendenti per testa verbale:', self.corpusFacile['Media di Archi Entranti in Teste Verbali:'], str(0.0), self.corpusDifficile['Media di Archi Entranti in Teste Verbali:']])
		#print >> self.fileOutput,  '\t'.join(['Totale catene preposizionali:', str(self.TOT_Catene)])
		print >> self.fileOutput, '<t>"Misura" della profondità dell\'albero sintattico:'.decode('utf-8')
		print >> self.fileOutput,  '\t'.join(['<t><t>Media delle altezze massime:', self.corpusFacile['Media delle Altezze Massime degli alberi sintattici:'], str(self.altezzaMax/self.sentCount), self.corpusDifficile['Media delle Altezze Massime degli alberi sintattici:']])
		
		if self.TOT_Catene==0.0:
			self.TOT_Catene=1.0
		print >> self.fileOutput,  '\t'.join(['<t><t>Profondità media di strutture nominali complesse:'.decode('utf-8'), self.corpusFacile['Lunghezza Media delle Catene preposizionali:'], str(self.lunghezzaCatena/self.TOT_Catene), self.corpusDifficile['Lunghezza Media delle Catene preposizionali:']])
		if self.TOT_Subordinate==0.0:
			self.TOT_Subordinate=1.0
		print >> self.fileOutput,  '\t'.join(['<t><t>Profondità media di "catene" di subordinazione:'.decode('utf-8'), self.corpusFacile['Lunghezza Media delle Catene Subordinanti:'], str(self.altezzaSubordinantiTOT/self.TOT_Subordinate), self.corpusDifficile['Lunghezza Media delle Catene Subordinanti:']])
		print >> self.fileOutput, '<t>"Misura" della lunghezza delle relazioni di dipendenza (calcolata come distanza in parole tra testa e dipendente):'
		
		if self.TotaleValidiPerLunghezzaLink:
			print >> self.fileOutput,  '\t'.join(['<t><t>Lunghezza media:', self.corpusFacile['Media della lunghezza delle relazioni sintattiche (esclusa la punteggiatura):'], str(self.lunghezzaLink/self.TotaleValidiPerLunghezzaLink), self.corpusDifficile['Media della lunghezza delle relazioni sintattiche (esclusa la punteggiatura):']])
		else:
			print >> self.fileOutput,  '\t'.join(['<t><t>Lunghezza media:', self.corpusFacile['Media della lunghezza delle relazioni sintattiche (esclusa la punteggiatura):'], str(0.0), self.corpusDifficile['Media della lunghezza delle relazioni sintattiche (esclusa la punteggiatura):']])
			
		print >> self.fileOutput,  '\t'.join(['<t><t>Media delle lunghezze massime:', self.corpusFacile['Media della lunghezza delle relazioni sintattiche massime (esclusa la punteggiatura):'], str(self.lunghezzaLinkMax/(self.sentCount)), self.corpusDifficile['Media della lunghezza delle relazioni sintattiche massime (esclusa la punteggiatura):']])
	
		#QUIIIITOT_Subordinate, altezzaSUB, self.ALTEZZASub
		#print >> self.fileOutput,  '\t'.join(['Totale Strutture Subordinate:', str(self.TOT_Subordinate)])
		
		#print >> self.fileOutput, 
## 		print >> self.fileOutput,  'Tipo di DIPENDENZA:'
## 		for dep in self.DEP:
## 			tag="DIP "
## 			print >> self.fileOutput,  '\t'.join([tag+dep,  str((self.DEP[dep]/self.tokenCount)*100*1.0), str(self.DEP[dep])])
## 		print >> self.fileOutput, 
## 		print >> self.fileOutput, 
## 		print >> self.fileOutput,  'Archi entranti in Teste Verbali:'
## 		for linkE in LINK_ENTRANTI:
## 			tag="ARCH "
## 			if self.verbCount:
## 				print >> self.fileOutput,  '\t'.join([tag+str(linkE), str((LINK_ENTRANTI[linkE]/self.verbCount)*100*1.0), str(LINK_ENTRANTI[linkE])])
## 			else:
## 				print >> self.fileOutput,  '\t'.join([tag+str(linkE), str(0.0), str(LINK_ENTRANTI[linkE])])

## 		print >> self.fileOutput, 
## 		print >> self.fileOutput,  'Catene PREP:'
## 		for catena in CATENE_PREP:
## 			tag="PREP "
## 			print >> self.fileOutput,  '\t'.join([tag+str(catena), str((CATENE_PREP[catena]/self.TOT_Catene)*100*1.0), str(CATENE_PREP[catena])])

## 		#QUII
## 		print >> self.fileOutput, 
## 		print >> self.fileOutput,  'Catene SUBORD:'
## 		for subordinate in self.ALTEZZASub:
## 			tag="SUB "
## 			print >> self.fileOutput,  '\t'.join([tag+str(subordinate), str((self.ALTEZZASub[subordinate]/self.TOT_Subordinate)*100*1.0), str(self.ALTEZZASub[subordinate])])

		#print >> self.fileOutput, 
		#print >> self.fileOutput,  'Divisioni Frasi:'
		#print >> self.fileOutput 
## 		if FrasiSubordinate==0.0:
## 			FrasiSubordinate=1.0
## #
# 		print >> self.fileOutput,  '\t'.join(["Sintassi - subordinate percentuale di Completive", str((SubordinateCompletive/FrasiSubordinate)*100*1.0)])
## 		print >> self.fileOutput,  '\t'.join(["Sintassi - subordinate percentuale di Infinitive", str((SubordinateInfinitive/FrasiSubordinate)*100*1.0)])

		#print >> self.fileOutput 
## 		if not ('subord_pre' in self.DirezioneDipendenze):
## 			self.DirezioneDipendenze['subord_pre']=0.0
## 		if not ('subord_post' in self.DirezioneDipendenze):
## 			self.DirezioneDipendenze['subord_post']=0.0


## 		#print >> self.fileOutput,  'Distribuzione posizionale delle frasi Subordinate rispetto alle Principali:'
## 		if not(self.DirezioneDipendenze['subord_pre']+self.DirezioneDipendenze['subord_post']==0):
## 			print >> self.fileOutput,  '\t'.join(['Subordinate che precedono la frase principale:', self.corpusFacile['Subordinate che precedono la frase principale'], str((self.DirezioneDipendenze['subord_pre']/(self.DirezioneDipendenze['subord_pre']+self.DirezioneDipendenze['subord_post']))*100*1.0), self.corpusDifficile['Subordinate che precedono la frase principale']])
## 			print >> self.fileOutput,  '\t'.join(['Subordinate che seguono la frase principale:', self.corpusFacile['Subordinate che seguono la frase principale'], str((self.DirezioneDipendenze['subord_post']/(self.DirezioneDipendenze['subord_pre']+self.DirezioneDipendenze['subord_post']))*100*1.0), self.corpusDifficile['Subordinate che seguono la frase principale']])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(['Subordinate che precedono la frase principale:', self.corpusFacile['Subordinate che precedono la frase principale'], str(0), self.corpusDifficile['Subordinate che precedono la frase principale']])
## 			print >> self.fileOutput,  '\t'.join(['Subordinate che seguono la frase principale:', self.corpusFacile['Subordinate che seguono la frase principale'], str(0), self.corpusDifficile['Subordinate che seguono la frase principale']])
		print >> self.fileOutput
		print >> self.fileOutput


	def stampaStatistiche(self, documento):
		#print >> self.fileOutput
		print >> self.fileOutput,  '\t'.join(["##########", "##########", "##########", "##########", "##########"])
		print >> self.fileOutput,  '\t'.join(["Statistiche Documento:", str(documento)])
		print >> self.fileOutput 
		print >> self.fileOutput,  '**************** Valori statistici vari: ****************'
		print >> self.fileOutput
		print >> self.fileOutput,  '\t'.join(['Totale Frasi:', str(self.sentCount)])
		print >> self.fileOutput,  '\t'.join(['Totale Token:', str(self.tokenCount)])
		print >> self.fileOutput,  '\t'.join(['Numero di Token per Frase:', str(self.tokenCount/self.sentCount)])
		print >> self.fileOutput,  '\t'.join(['Numero di Caratteri per Token (esclusa la punteggiatura):', str(self.LunghezzaParole/self.TOT_Token)])
		print >> self.fileOutput
		print >> self.fileOutput
		print >> self.fileOutput,  '**************** Caratteristiche Lessicali: ****************'
		print >> self.fileOutput
## 		print >> self.fileOutput,  '\t'.join(['Dizionario - percentuale delle parole ritrovate:', str((TrovateInDizionario/self.TOT_Token)*100*1.0)])
## 		for classe in DizFondamentale:
## 			tag="DizF  "
## 			if TrovateInDizionario:
## 				print >> self.fileOutput,  '\t'.join([tag+classe, str((DizFondamentale[classe]/TrovateInDizionario)*100*1.0)])
## 			else:
## 				print >> self.fileOutput,  '\t'.join([tag+classe, str((DizFondamentale[classe]/1.0)*100*1.0)])	

		
		print >> self.fileOutput,  '\t'.join(['Percentuale di lemmi trovati nel dizionario di base:', str((len(self.DizionarioDeMauroType)*1.0/len(self.Lemmi)*1.0)*100)])
		tag="DizL  "
		classe="FO"
		if not(self.DizionarioDeMauroType):
			denominatore=1.0
		else:
			denominatore=len(self.DizionarioDeMauroType)
		chiave="Vocabolario Fondamentale:"
		print >> self.fileOutput,  '\t'.join([chiave, str((self.DizFondamentaleType[classe]/denominatore)*100*1.0)])
		classe="AU"
		chiave="Vocabolario di alto uso:"
		print >> self.fileOutput,  '\t'.join([chiave, str((self.DizFondamentaleType[classe]/denominatore)*100*1.0)])
		classe="AU"
		chiave="Vocabolario di alta disponibilita':"
		print >> self.fileOutput,  '\t'.join([chiave, str((self.DizFondamentaleType[classe]/denominatore)*100*1.0)])
		print >> self.fileOutput
		print >> self.fileOutput,  '\t'.join(['Range '+str(self.rangeType)+' Type(forme)/token:', str(len(self.FormeTT)/self.rangeType)])
		print >> self.fileOutput,  '\t'.join(['Range '+str(self.rangeType)+' Type(lemmi)/token:', str(len(self.LemmiTT)/self.rangeType)])
		print >> self.fileOutput,  '\t'.join(['Range '+str(self.rangeType2)+' Type(forme)/token:', str(len(self.FormeTT2)/self.rangeType2)])
		print >> self.fileOutput,  '\t'.join(['Range '+str(self.rangeType2)+' Type(lemmi)/token:', str(len(self.LemmiTT2)/self.rangeType2)])
		print >> self.fileOutput
		print >> self.fileOutput,  '\t'.join(['Densita Lessicale:', str(self.ParolePiene/self.TOT_Token)])
		print >> self.fileOutput
		print >> self.fileOutput
		print >> self.fileOutput,  '**************** Caratteristiche Morfo-Sintattiche e Sintattiche: ****************'
		print >> self.fileOutput
		#print >> self.fileOutput,  'CPOS:'
		if "S" in self.POS:
			print >> self.fileOutput,  '\t'.join(["Percentuale di Sostantivi", str((self.POS["S"]/self.tokenCount)*100*1.0)])
		if "SP" in self.POS:
			print >> self.fileOutput,  '\t'.join(["Percentuale di Nomi Propri", str((self.POS["SP"]/self.tokenCount)*100*1.0)])
		if "A" in self.CPOS:
			print >> self.fileOutput,  '\t'.join(["Percentuale di Aggettivi", str((self.CPOS["A"]/self.tokenCount)*100*1.0)])
		if "V" in self.CPOS:
			print >> self.fileOutput,  '\t'.join(["Percentuale di Verbi", str((self.CPOS["V"]/self.tokenCount)*100*1.0)])
		if "C" in self.CPOS:
			print >> self.fileOutput,  '\t'.join(["Percentuale di Congiunzioni", str((self.CPOS["C"]/self.tokenCount)*100*1.0)])
		
## 		tag="Verbi "
## 		print >> self.fileOutput,  'Verbi+Modo:'
## 		for posMfeat in self.VERBI_MODO:
## 			print >> self.fileOutput,  '\t'.join([tag+posMfeat, str((self.VERBI_MODO[posMfeat]/self.verbTOT)*100*1.0)])
## 		print >> self.fileOutput,  'Verbi+Tempo:'
## 		for posMfeat in self.VERBI_TEN:
## 			print >> self.fileOutput,  '\t'.join([tag+posMfeat, str((self.VERBI_TEN[posMfeat]/self.verbiTempo)*100*1.0)])
## 		print >> self.fileOutput,  'Verbi+Numero+Persona:'
## 		for posMfeat in self.VERBI_NUM_PERSON:
## 			print >> self.fileOutput,  '\t'.join([tag+posMfeat, str((self.VERBI_NUM_PERSON[posMfeat]/self.verbiNumPer)*100*1.0)])
		print >> self.fileOutput
		#print >> self.fileOutput,  '\t'.join(['Totale Teste Verbali:', str(self.verbCount)])
		print >> self.fileOutput,  '\t'.join(['Media di Teste Verbali per frase:', str(self.verbCount/self.sentCount)])
		#print >> self.fileOutput,  '\t'.join(['Totale Radici Verbali:', str(self.RadiciVerbali)])
		#if (self.TOT_ROOT):
			#print >> self.fileOutput,  '\t'.join(['Sintassi - Percentuale di Radici Verbali:', str(self.RadiciVerbali/self.TOT_ROOT)])
		#else:
			#print >> self.fileOutput,  '\t'.join(['Sintassi - Percentuale di Radici Verbali:', str(self.RadiciVerbali/1.0)])
## 		if self.RadiciVerbali:
## 			print >> self.fileOutput,  '\t'.join(['Sintassi - Percentuale di Radici Verbali con Soggetto esplicito:', str(self.RadiciVerbaliConSogg/self.RadiciVerbali*100*1.0)])
## 		else:
## 			print >> self.fileOutput,  '\t'.join(['Sintassi - Percentuale di Radici Verbali con Soggetto esplicito:', str(0.0)])
		if self.verbCount:
			print >> self.fileOutput,  '\t'.join(['Numero di token per Clausola:', str((self.tokenCount/self.verbCount)*1.0)])
		else:
			print >> self.fileOutput,  '\t'.join(['Numero di token per Clausola:', str(0.0)])
		#print >> self.fileOutput,  '\t'.join(['Totale Archi Entranti in Teste Verbali:', str(self.ContaLinkEntranti)])
		if self.verbCount:
			print >> self.fileOutput,  '\t'.join(['Media di Archi Entranti in Teste Verbali:', str(self.ContaLinkEntranti/self.verbCount)])
		else:
			print >> self.fileOutput,  '\t'.join(['Media di Archi Entranti in Teste Verbali:', str(0.0)])
		#print >> self.fileOutput,  '\t'.join(['Totale catene preposizionali:', str(self.TOT_Catene)])
		if self.TOT_Catene==0.0:
			self.TOT_Catene=1.0
		print >> self.fileOutput,  '\t'.join(['Lunghezza Media delle Catene preposizionali:', str(self.lunghezzaCatena/self.TOT_Catene)])
		if self.TotaleValidiPerLunghezzaLink:
			print >> self.fileOutput,  '\t'.join(['Media della lunghezza delle relazioni sintattiche (esclusa la punteggiatura):', str(self.lunghezzaLink/self.TotaleValidiPerLunghezzaLink)])
		else:
			print >> self.fileOutput,  '\t'.join(['Media della lunghezza delle relazioni sintattiche (esclusa la punteggiatura):', str(0.0)])
		print >> self.fileOutput,  '\t'.join(['Media della lunghezza delle relazioni sintattiche massime (esclusa la punteggiatura):', str(self.lunghezzaLinkMax/(self.sentCount))])
		print >> self.fileOutput,  '\t'.join(['Media delle Altezze Massime degli alberi:', str(self.altezzaMax/self.sentCount)])
		#QUIIIITOT_Subordinate, altezzaSUB, self.ALTEZZASub
		#print >> self.fileOutput,  '\t'.join(['Totale Strutture Subordinate:', str(self.TOT_Subordinate)])
		if self.TOT_Subordinate==0.0:
			self.TOT_Subordinate=1.0
		print >> self.fileOutput,  '\t'.join(['Lunghezza Media delle Catene Subordinanti:', str(self.altezzaSubordinantiTOT/self.TOT_Subordinate)])
		#print >> self.fileOutput, 
## 		print >> self.fileOutput,  'Tipo di DIPENDENZA:'
## 		for dep in self.DEP:
## 			tag="DIP "
## 			print >> self.fileOutput,  '\t'.join([tag+dep,  str((self.DEP[dep]/self.tokenCount)*100*1.0), str(self.DEP[dep])])
## 		print >> self.fileOutput, 
## 		print >> self.fileOutput, 
## 		print >> self.fileOutput,  'Archi entranti in Teste Verbali:'
## 		for linkE in LINK_ENTRANTI:
## 			tag="ARCH "
## 			if self.verbCount:
## 				print >> self.fileOutput,  '\t'.join([tag+str(linkE), str((LINK_ENTRANTI[linkE]/self.verbCount)*100*1.0), str(LINK_ENTRANTI[linkE])])
## 			else:
## 				print >> self.fileOutput,  '\t'.join([tag+str(linkE), str(0.0), str(LINK_ENTRANTI[linkE])])

## 		print >> self.fileOutput, 
## 		print >> self.fileOutput,  'Catene PREP:'
## 		for catena in CATENE_PREP:
## 			tag="PREP "
## 			print >> self.fileOutput,  '\t'.join([tag+str(catena), str((CATENE_PREP[catena]/self.TOT_Catene)*100*1.0), str(CATENE_PREP[catena])])

## 		#QUII
## 		print >> self.fileOutput, 
## 		print >> self.fileOutput,  'Catene SUBORD:'
## 		for subordinate in self.ALTEZZASub:
## 			tag="SUB "
## 			print >> self.fileOutput,  '\t'.join([tag+str(subordinate), str((self.ALTEZZASub[subordinate]/self.TOT_Subordinate)*100*1.0), str(self.ALTEZZASub[subordinate])])
		###Calcolo il numero di Frasi Subordinate:
		FrasiSubordinate=0.0
		SubordinateInfinitive=0.0
		SubordinateCompletive=0.0
		#print >> self.fileOutput,  "******************"
		for arg in self.ARG_Special:
			FrasiSubordinate+=self.ARG_Special[arg]
			if arg in ["V-->V", "V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.ARG_Special[arg]
			elif arg=="V-->CS-->V":
				SubordinateCompletive+=self.ARG_Special[arg]
		for mod in self.MOD_Special:
			FrasiSubordinate+=self.MOD_Special[mod]
			if mod in ["V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.MOD_Special[mod]
			elif mod in ["V-->CS-->V", "V-->B-->V"]:
				SubordinateCompletive+=self.MOD_Special[mod]
		for comp in self.COMP_Special:
			if comp in ["V-->E-->V", "V-->EA-->V"]:
				SubordinateInfinitive+=self.COMP_Special[comp]
			FrasiSubordinate+=self.COMP_Special[comp]

		#print >> self.fileOutput, 
		#print >> self.fileOutput,  'Divisioni Frasi:'
		if (FrasiSubordinate+self.RadiciVerbali):
			print >> self.fileOutput,  '\t'.join(['Percentuale frasi Principali',  str((self.RadiciVerbali/(FrasiSubordinate+self.RadiciVerbali))*100*1.0)]) # , str(self.RadiciVerbali)])
			print >> self.fileOutput,  '\t'.join(['Percentuale frasi Subordinate', str((FrasiSubordinate/(FrasiSubordinate+self.RadiciVerbali))*100*1.0)]) #  , str(FrasiSubordinate)])
		else:
			print >> self.fileOutput,  '\t'.join(['Percentuale frasi Principali',  str(0.0)]) #, str(self.RadiciVerbali)])
			print >> self.fileOutput,  '\t'.join(['Percentuale frasi Subordinate', str(0.0)])  #, str(FrasiSubordinate)])
		#print >> self.fileOutput
		if FrasiSubordinate==0.0:
			FrasiSubordinate=1.0
## 		print >> self.fileOutput,  '\t'.join(["Sintassi - subordinate percentuale di Completive", str((SubordinateCompletive/FrasiSubordinate)*100*1.0)])
## 		print >> self.fileOutput,  '\t'.join(["Sintassi - subordinate percentuale di Infinitive", str((SubordinateInfinitive/FrasiSubordinate)*100*1.0)])

		#print >> self.fileOutput 
		if not ('subord_pre' in self.DirezioneDipendenze):
			self.DirezioneDipendenze['subord_pre']=0.0
		if not ('subord_post' in self.DirezioneDipendenze):
			self.DirezioneDipendenze['subord_post']=0.0


		#print >> self.fileOutput,  'Distribuzione posizionale delle frasi Subordinate rispetto alle Principali:'
		if not(self.DirezioneDipendenze['subord_pre']+self.DirezioneDipendenze['subord_post']==0):
			print >> self.fileOutput,  '\t'.join(['Subordinate che precedono la frase principale', str((self.DirezioneDipendenze['subord_pre']/(self.DirezioneDipendenze['subord_pre']+self.DirezioneDipendenze['subord_post']))*100*1.0)])
			print >> self.fileOutput,  '\t'.join(['Subordinate che seguono la frase principale', str((self.DirezioneDipendenze['subord_post']/(self.DirezioneDipendenze['subord_pre']+self.DirezioneDipendenze['subord_post']))*100*1.0)])
		else:
			print >> self.fileOutput,  '\t'.join(['Subordinate che precedono la frase principale', str(0)])
			print >> self.fileOutput,  '\t'.join(['Subordinate che seguono la frase principale', str(0)])
		print >> self.fileOutput
		print >> self.fileOutput



        def analyze(self, Frasi):
		#global self.tokenCount, self.sentCount
		tokens = []		
		for i in range(len(Frasi)):
			for j in range(len(Frasi[i])):
				tok=Token(((Frasi[i])[j].strip()).split('\t'))
				tokens.append(tok)
			self.tokenCount +=len(tokens)
			#err 06022014
			#for tok in tokens:
				#print tok.word
			Sentence(tokens)
			self.FeatLessicali(tokens, self.DizionarioDeMauro)
			self.FeatPOS(tokens)
			self.FeatSintattiche(tokens)
			tokens = []
		self.sentCount=len(Frasi)

	def Parsing(self, inputFileName, destDir = None):
		indice=0
		self.inputFileName = inputFileName
		numDocumenti=1.0
		TESTO=self.DividiTestoInDocumenti(inputFileName, destDir)
		if not(self.confronto):
			print >> self.fileOutput,  "<div class=\"accordion\" id=\"accordionmain\">"
		for NomeDocumento in TESTO:
			indice+=1
			#self.analyze(TESTO[documento])
			self.analyze(NomeDocumento[1])
			if self.confronto:
				self.stampaStatisticheConfronto(NomeDocumento[0])
			else:
				self.stampaStatisticheNoConfrontoHTMLandrea(NomeDocumento[0], indice)
			self.azzeraVariabili()
		if not(self.confronto):
			print >> self.fileOutput,  "</div>"
		#self.outputFileName = renameTmpFileName(self.outputFileName)
		
	def getAnalisi(self, inputFileName, dest_dir=None):
		indice=0
		self.inputFileName = inputFileName
		numDocumenti=1.0
		TESTO=self.DividiTestoInDocumenti(inputFileName, dest_dir)
		for NomeDocumento in TESTO:
			indice+=1
			self.analyze(NomeDocumento[1])
			Risultato=self.calcolaStatistiche(NomeDocumento[0], indice)
			self.azzeraVariabili()
		return Risultato

	def CaricaMonitoraggio(self):
		self.DizionarioDeMauro={}
		self.CaricaDizionarioFondamentale()
		if self.confronto:
			self.corpusFacile=self.caricaStatistiche(self.facile)
			self.corpusDifficile=self.caricaStatistiche(self.difficile)
			
	def Monitoraggio(self, inputFileName, destDir=None):
		self.Parsing(inputFileName, destDir)

def usage():
	print """Monitoraggio.py:
	Input file must be in CoNLL format.

	Usage:
	  Monitoraggio.py [options] [file]
	
	Options:
	-c                        set confronto
	-D                        considera il corpus senza la divisione in documenti
	-i    nomefile            nome del file da monitorare documento per documento
  	"""

def main():
	try:
		long_opts = ['os', 'help', 'usage']
		opts, args = getopt.gnu_getopt(sys.argv[1:], 'h c i: D',long_opts)
	except getopt.GetoptError:
		usage()
		sys.exit()
	#Valori di default:
	confronto=False
	divisioneDoc=True
	scriptpath = os.path.abspath(os.path.dirname(sys.argv[0]))
	fileLessico=scriptpath+"/"+'Lessici/it/Readability/DizionarioFondamentale'
	inputFileName=""
	for opt, arg in opts:
	    if opt in ('-h', '--help'):
		usage()
		sys.exit()
	    elif opt == '--usage':
		usage()
		sys.exit()   
	    elif opt == '-c':
		confronto=True
	    elif opt == '-D':
		divisioneDoc=False
	    elif opt == '-i':
		inputFileName=arg
	    else:
		    assert False, "unhandled option"
	p = Monitoraggio()
	#p.setEncode(encode)
	p.setLessico(fileLessico)
	p.setConfronto(confronto)
	p.setDivisioneDoc(divisioneDoc)
	p.CaricaMonitoraggio()
	
	p.Monitoraggio(inputFileName)
	#print p.getAnalisi(inputFileName)

if __name__ == "__main__":
    main()
