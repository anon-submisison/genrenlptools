from collections import defaultdict
import logging
import math
import operator

def CalcolaLL(n1p, np1, n11, npp):
  #print "n1p, np1, n11, npp", n1p, np1, n11, npp
  #print  "npp", npp #err_2609
  #print  "n1p", n1p #err_2609
  #print  "np1", np1 #err_2609
  #print  "n11", n11 #err_2609
  LL=0.0
  if n1p<n11:
      n1p=n11
  if np1<n11:
      np1=n11
  n12=n1p-n11
  n21=np1-n11
  np2=npp-np1
  n2p=npp-n1p
  #print "np2", np2 #err_2609
  #print "n12", n12 #err_2609
  n22=np2-n12
  m11=(n1p*np1)*1.0/npp*1.0
  m12=(n1p*np2)*1.0/npp*1.0
  m21=(n2p*np1)*1.0/npp*1.0
  m22=(n2p*np2)*1.0/npp*1.0
  a=0.0
  b=0.0
  c=0.0
  d=0.0
  if (n11!=0 and m11!=0):
      a=math.log(n11/m11)
  if (n12!=0 and m12!=0):
      b=math.log(n12/m12)
  if (n21!=0 and m21!=0):
      #print
      #print "n21", n21
      #print "m21", m21
      #print "n21/m21", n21/m21
      c=math.log(n21/m21)
  if (n22!=0 and m22!=0):
      #print "n22", n22  #err_2609
      #print "m22", m22  #err_2609
      d=math.log(n22/m22)
  if n11 == 0:
    LL = 0
  else:
    LL=(2 * ((n11*a)+(n12*b)+(n21*c)+(n22*d)))*1.0
  return LL

def init_dict(label_set):
  start_dict = {}
  for x in label_set:
    start_dict[x] = 0
  start_dict['total'] = 0
  return start_dict

def infogain_priv(labels, samples, ll=True):
  label_set = set(labels)
  dict_ctr = defaultdict(lambda: init_dict(label_set))
  label_feat_frequencies = defaultdict(lambda: defaultdict(int))
  labels_frequencies = defaultdict(int)
  feature_frequencies = defaultdict(int)
  # Samples must be distinct
  samples = [[x for x in set(sample)] for sample in samples]

  features_set = set()
  for label, sample in zip(labels, samples):
    labels_frequencies[label] += 1
    labels_frequencies['total'] += 1
    for feat in sample:
      features_set.add(feat)
      dict_ctr[feat][label] += 1
      dict_ctr[feat]['total'] += 1
      feature_frequencies[feat] += 1
      feature_frequencies['total'] += 1
      label_feat_frequencies[label][feat] += 1
      label_feat_frequencies[label]['total'] += 1
      #label_features_totalfrequencies[label]['total'] += 1
  infogain_map_dict = defaultdict(dict)
  for label in label_set:
    for feature in features_set:

      # Frequenza assoluta della feature selezionata
      num_features_selected_feat = float(feature_frequencies[feature])  # noqa

      # Numero assoluto di features
      num_features_total = float(feature_frequencies['total'])  # noqa

      # Numero di feature totale presente nella label corrente
      num_features_in_label = float(label_feat_frequencies[label]['total'])  # noqa

      # Numero di feature della feature selezionata nella label corrente
      num_features_selected_feat_in_label = float(label_feat_frequencies[label][feature])  # noqa

      # Numero totale di documenti
      num_labels = float(labels_frequencies['total'])

      # Numero totale di documenti per la classe corrente
      num_selected_label = float(labels_frequencies[label])

      if ll:
        final_value = CalcolaLL(num_features_selected_feat,
                                num_selected_label,
                                num_features_selected_feat_in_label,
                                num_features_total)
        infogain_map_dict[label][feature] = final_value
      else:
        p_t_c = num_features_selected_feat_in_label/num_selected_label  # noqa OK

        p_tn_c = 1 - p_t_c

        p_t = num_features_selected_feat/num_labels  # noqa OK
        p_c = num_selected_label/num_labels  # noqa OK

        p_t_cn = (num_features_selected_feat - num_features_selected_feat_in_label) / (num_labels - num_selected_label)  # noqa
        p_cn = (num_labels - num_selected_label) / num_labels  # noqa

        p_tn = (num_labels - num_features_selected_feat)/num_labels  # noqa OK
        p_tn_cn = 1 - p_t_cn  #(num_features_in_label - num_features_selected_feat_in_label) / (num_features_total - num_features_in_label)
        final_value = 0
        if p_t_c == 0 or p_t_cn == 0 or p_tn_c == 0 or p_tn_cn == 0 or\
              (p_t * p_c) == 0 or (p_t * p_cn) == 0 or \
              (p_tn * p_c) == 0 or (p_tn * p_cn) == 0:
          final_value = 0
        else:
          # S1 (Positivo)
          s_1 = p_t_c * math.log(p_t_c / (p_t * p_c))
          # S2
          s_2 = p_t_cn * math.log(p_t_cn / (p_t * p_cn))
          #  s_2 = 0
          # S3
          s_3 = p_tn_c * math.log(p_tn_c / (p_tn * p_c))
          # s_3 = 0
          # S4 (Positivo)
          s_4 = p_tn_cn * math.log(p_tn_cn / (p_tn * p_cn))
          final_value = s_1 + s_2 + s_3 + s_4
        infogain_map_dict[label][feature] = final_value
  if False:
    for x in infogain_map_dict:
      current_dict = infogain_map_dict[x]
      sorted_x = sorted(current_dict.iteritems(), key=operator.itemgetter(1),
                        reverse=True)
      print "CLASS: %s" % x
      for item in sorted_x:
        print "%s, %s" % (item[0].encode('utf-8'), item[1])
  return features_set, infogain_map_dict

def infogain(labels, samples, percentage=100):
  logging.info("Pruning via infogain...")
  features_set, infogain_map_dict = infogain_priv(labels, samples)
  num_all_features = len(features_set)
  logging.info("Num features before infogain: %s" % num_all_features)
  feat_max = {}
  for feat in features_set:
    feat_max[feat] = sum([infogain_map_dict[x].get(feat, 0) for x in infogain_map_dict])
  sorted_x = sorted(feat_max.iteritems(), key=operator.itemgetter(1),
                    reverse=True)
  resulting_features = set([x[0] for x in sorted_x][0: int(percentage * (len(sorted_x)/float(100)))])
  logging.info("Num features after infogain: %s" % len(resulting_features))
  for idx, sample in enumerate(samples):
    samples[idx] = [feat for feat in sample if feat in resulting_features]
  return samples

def filter_dataset(labels, samples, minimum_frequency=2):
  label_set = set(labels)
  dict_ctr = defaultdict(lambda: init_dict(label_set))
  for label, sample in zip(labels, samples):
    for feat in sample:
      dict_ctr[feat][label] += 1
      dict_ctr[feat]['total'] += 1
  logging.info("Features before pruning: %s" % len(dict_ctr.keys()))
  new_samples = []
  for sample in samples:
    new_samples.append([feat for feat in sample
                        if dict_ctr[feat]['total'] >= minimum_frequency])

  final_dict = {}
  for sample in new_samples:
    for feat in sample:
      final_dict[feat] = 0
  logging.info("Features before pruning: %s" % len(final_dict))
  return (labels, new_samples)
