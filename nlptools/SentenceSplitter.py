#!/usr/bin/python
# -*- coding: utf-8 -*-
#$Id: SentenceSplitter.py,v 1.5 2012-02-03 10:52:46 simone Exp $

import sys
import getopt
import codecs
from felice_libsvm_2_91_svm import *
from felice_maxent import *
from Conf import Conf
from Utils import *
from pkg_resources import resource_filename


class SentenceSplitter:

    ver = "$RCSfile: SentenceSplitter.py,v $ $Revision: 1.5 $"
    def __init__(self):
        self.Lexicon = {}
        self.encode = 'utf-8'
        self.ritorniACapo = False
        self.debug = False
        self.training = False

    def setDebug(self,debug):
        self.debug = debug

    def setEncode(self,encode):
        self.encode = encode

    def setRitorniACapo(self, ritorniACapo):
        self.ritorniACapo = ritorniACapo

    def setTraining(self,training):
        self.training = training

    def setPickled(self, pickled):
        self.pickled = pickled

    def CaricaSentenceSplitter(self, confFileName, modelFileName):

        self.Lexicon={}
        self.conf = Conf()
        self.conf.CaricaFileConfigurazione(confFileName)

        if self.training:
            self.modello=modelFileName
        else:
            if (self.conf.ALG=='SVM'):
                self.modello=Fsvm_CaricaModelloSVM(modelFile)  #in felice_svm
                self.modelloIndici=Fsvm_CaricaIndici(modelFile)  #in felice_svm
            elif(self.conf.ALG=='ME'):
                self.modello=MaxentModel()
                self.modello.load(modelFileName+'.me')

    #Estraggo le features:
    def estraiFeatures(self, id, frase):

        lenFrase=len(frase)
        ListaFeatures=[]
        #WORD:
        if (self.conf.featValue["WORD"]):
            for feat in self.conf.featValue["WORD"]:
                    if (((id+feat) >= 0) and ((id+feat) < lenFrase)):
                        feature='W%d_' % feat + '%s' % (frase[id+feat]).lower()
                        #print feature #err
                        ListaFeatures.append(feature)
        #WORD_LENGTH:
        if (self.conf.featValue["WORD_LENGTH"]):
            for feat in self.conf.featValue["WORD_LENGTH"]:
                if (((id+feat) >= 0) and ((id+feat) < lenFrase)):
                    feature='L%d_' % feat + '%d' % len(frase[id+feat])
                    ListaFeatures.append(feature)
        #WORD_SUFFIX:
        if (self.conf.featValue["WORD_SUFFIX"]):
            for feat in self.conf.featValue["WORD_SUFFIX"]:
                if (((id+feat) >= 0) and ((id+feat) < lenFrase)):
                    i=1
                    while ((i<len(frase[id+feat])) and (i<(self.conf.maxSuffix)+1)):
                        feature='s%d_' % feat + '%s' % ((frase[id+feat])[(len(frase[id+feat])-i):]).lower()
                        ListaFeatures.append(feature)
                        i+=1
        #WORD_PREFIX:
        if (self.conf.featValue["WORD_PREFIX"]):
            for feat in self.conf.featValue["WORD_PREFIX"]:
                if (((id+feat) >= 0) and ((id+feat) < lenFrase)):
                    i=1
                    while ((i<len(frase[id+feat])) and (i<(self.conf.maxPrefix)+1)):
                        feature='p%d_' % feat + '%s' % ((frase[id+feat])[:-(len(frase[id+feat])-i)]).lower()
                        ListaFeatures.append(feature)
                        i+=1
        #PUNCT_DISTANCE:
        if (self.conf.featValue["PUNCT_DISTANCE"]):
            for feat in self.conf.featValue["PUNCT_DISTANCE"]:
                if (((id+feat) >= 0) and ((id+feat) < lenFrase)):
                    feature='D%d_' % feat + '%d' % (id+feat)
                    ListaFeatures.append(feature)
        #WORD_FORMAT:
        if (self.conf.featValue["WORD_FORMAT"]):

           for feat in self.conf.featValue["WORD_FORMAT"]:
                if (((id+feat) >= 0) and ((id+feat) < lenFrase)):
                    format=0
                    if((frase[id+feat]).islower()):
                        format=1
                    elif((frase[id+feat]).isupper()):
                        format=3
                    elif((len(frase[id+feat])>1) and ((frase[id+feat])[0].isupper()) and ((frase[id+feat])[1:].islower())):
                        format=2
                    feature='F%d_' % feat + '%d' % format
                    ListaFeatures.append(feature)
        #WORD_SHAPE:
        if (self.conf.featValue["WORD_SHAPE"]):
                for feat in self.conf.featValue["WORD_SHAPE"]:
                    if (((id+feat) >= 0) and ((id+feat) < lenFrase)):
                        shape=''
                        i=0
                        while (i<len(frase[id+feat])):
                            if (frase[id+feat][i].islower()):
                                shape+='x'
                            elif (frase[id+feat][i].isupper()):
                                shape+='X'
                            else:
                                shape+=frase[id+feat][i]
                            i+=1
                        feature='S%d_' % feat + '%s' % shape
                        ListaFeatures.append(feature)
        #Special Feature:
        if (self.conf.featValue["SUPER_FEAT"]):
            for feat in self.conf.featValue["SUPER_FEAT"]:
                feat_S=feat.split("_")
                completo=1
                feature='SB'
                for f in feat_S:
                    feat_id=int(f[1:])
                    Type=f[0]
                    if (((id+feat_id) >= 0) and ((id+feat_id) < lenFrase)):
                        if(Type=='W'):
                            feature=feature + '_%s_' %f + '%s' % frase[id+feat_id]
                        if(Type=='L'):
                            feature=feature + '_%s_' %f + '%d' % len(frase[id+feat_id])
                        if(Type=='D'):
                            feature=feature + '_%s_' %f + '%d' % id
                    else:
                        completo=0
                        break
                if (completo):
                    ListaFeatures.append(feature)
        return ListaFeatures

#######DA SISTEMARE. Ma per ora non serve...
    def Training(self, inputFileName):
        #ListaFeatures=[]
        l1='#'
        l2='#'
        l3='#'
        contatore=0.0
        labels=[]
        samples=[]
        #self.openIOFiles(inputFileName)
        self.fileInput=codecs.open(inputFileName, encoding=self.encode, mode='r')
        while l1=='\n' or l1[0]=='#':
            l1=self.fileInput.readline()
            #l1=sys.stdin.readline()
        while l2=='\n' or l2[0]=='#':
            l2=self.fileInput.readline()
            #l2=sys.stdin.readline()
#        l2=l2.decode('utf-8')  #Enc
        while not(l1==''):
            #print l1 #errrr
            lungL1=len(l1.split())
            while (not(len(l3)==0) and (l3=='\n' or l3[0]=='#')):
                l3=self.fileInput.readline()
                #l3=sys.stdin.readline()
#            l3=l3.decode('utf-8')  #Enc
            l1s=l1.strip()
            l2s=l2.strip()
            l3s=l3.strip()
            if l2s=='':
                l=l1s
            elif(l3s==''):
                l=l1s+' '+l2s
            else:
                l=l1s+' '+l2s+' '+l3s
            #print l #errrr
            lS=l.split()
            #print lS #errrr
            for i in range(lungL1):
                if len(lS[i])>1:
                    if lS[i][-1]=='.':
                        #Estraggo le labels:
                        if i==lungL1-1:
                            labels.append('S')
                        else:
                            labels.append('C')
                        #Estraggo le features:
                        ListaFeatures=self.estraiFeatures(i, lS)
                        samples.append(ListaFeatures)
            ListaFeatures=[]
            contatore+=1.0
            if not(contatore%1000):
                print >> sys.stderr,'+',
            elif not(contatore%100):
                print >> sys.stderr,'.',
            l1=l2
            l2=l3
            l3='#'
        LabelsSamples=(labels, samples)
        if (self.conf.ALG=='SVM'):
            modello=Fsvm_CreaModelloSVM(LabelsSamples, self.conf.parametriSVM, self.modello)  #in felice_svm
        elif (self.conf.ALG=='ME'):
            modello=Fme_CreaModelloME(LabelsSamples, self.conf.parametriME, self.modello)  #in felice_maxent
        else:
            print >> sys.stderr, 'ERR: Algoritmo di apprendimento sconosciuto:',self.conf.ALG
            exit(-1)
        #return (labels, samples)

    def ControllaFullStop(self, i, lS):
        if (lS[i][-1] in self.conf.featValue["CARATTERI_FULLSTOP_NON_AMBIGUI"] and (i == len(lS)-1 or not(lS[i+1] in self.conf.featValue["CARATTERI_BLOCCA_FULLSTOP"]))):
            return True
        else:
            return False

    def Stampa(self, parola, Risultato):
        if self.ritorniACapo and parola[-1]=='\n':
            parola=parola.strip()
        if Risultato=='S':
            print parola.encode('utf-8')
        else:
            print parola.encode('utf-8'),

    def StampaSuFile(self, parola, Risultato, fileOutput):
        if self.ritorniACapo and parola[-1]=='\n':
            parola=parola.strip()
        if Risultato=='S':
#            print >> fileOutput, parola.encode('utf-8')
            print >> fileOutput, parola
        else:
#            print >> fileOutput, parola.encode('utf-8'),
            print >> fileOutput, parola,


    def Parsing(self, inputFileName, destDir=None):

        self.inputFileName = inputFileName
        (self.fileInput, self.fileOutput, self.outputFileName) = openIOFiles(inputFileName, destDir, ".split.tmp")

        ListaFeatures=[]
        l1='\n'
        l2='\n'
        l3='\n'
        label=''
        while l1=='\n':
            l1=self.fileInput.readline()
            #l1=sys.stdin.readline()
#        l1=l1.decode('utf-8')  #Enc
        #print l1
        while l2=='\n':
            l2=self.fileInput.readline()
            #l2=sys.stdin.readline()
#        l2=l2.decode('utf-8')  #Enc
        while not(l1==''):
            while (l3=='\n'):
                l3=self.fileInput.readline()
                #l3=sys.stdin.readline()
#            l3=l3.decode('utf-8')  #Enc
            if not(self.ritorniACapo):
                l1s=l1.strip()
                l2s=l2.strip()
                l3s=l3.strip()
            else:
                l1s=l1
                l2s=l2
                l3s=l3
            #Mi serve per gestire bene i tag doc quando nn e' attivo il ritorno a capo:
            #gestendo separatamente apertura e chiusura impongo a chi crea il file di
            #  inserire SIA i tag di chiusura che quelli di apertura:
            if not(self.ritorniACapo) and l1s.startswith('</doc'):
                print
                print l1s.encode('utf-8')
                l1=l2
                l2=l3
                l3='\n'
                continue
            if not(self.ritorniACapo) and l1s.startswith('<doc'):
                print l1s.encode('utf-8')
                l1=l2
                l2=l3
                l3='\n'
                continue
            if l2s=='':
                l=l1s
            elif(l3s==''):
                l=l1s+' '+l2s
            else:
                l=l1s+' '+l2s+' '+l3s
            lS=l.split()
            lungL1=len(l1.split())
            for i in range(lungL1):
                if not(i==len(lS)):
                #if len(lS[i])>1 or lS[i]=='\n':
                    if self.ritorniACapo and  i==lungL1-1: #lS[i][-1]=='\n':
                        #self.Stampa(lS[i], 'S')
                        self.StampaSuFile(lS[i], 'S', self.fileOutput)
                        continue
                    if lS[i][-1]=='.':
                        if self.ritorniACapo and  i>0 and  i==lungL1-1: #lS[i-1][-1]=='\n':
                            lS[i-1]=lS[i-1].strip()
                        #Estraggo le features:
                        ListaFeatures=self.estraiFeatures(i, lS)
                        #errr:
                        #for feat in ListaFeatures:
                            #print feat.encode('utf-8')
                        if (self.conf.ALG=='SVM'):
                            ProbabilityPrediction=Fsvm_ApplicaSVM(ListaFeatures, self.modello, self.modelloIndici)
                            #print 'ProbabilityPrediction:', ProbabilityPrediction #errr
                            Risultato=ProbabilityPrediction[0][0]
                            #self.Stampa(lS[i], Risultato)
                            self.StampaSuFile(lS[i], Risultato, self.fileOutput)
                        elif (self.conf.ALG=='ME'):
                            ProbabilityPrediction=Fme_ApplicaME(ListaFeatures, self.modello, self.conf.MostraProbabilita)
                            if (self.conf.MostraProbabilita):
                                #print ProbabilityPrediction
                                Risultato=ProbabilityPrediction[0][0]
                            else:
                                Risultato=ProbabilityPrediction
                            self.StampaSuFile(lS[i], Risultato, self.fileOutput)
                            #self.Stampa(lS[i], Risultato)
                        else:
                            print >> sys.stderr, 'Algoritmo di apprendimento sconosciuto'
                        ListaFeatures=[]
                    else:
                        if self.ControllaFullStop(i, lS):
                            Risultato='S'
                        else:
                            Risultato='C'
                        self.StampaSuFile(lS[i], Risultato, self.fileOutput)
                        #self.Stampa(lS[i], Risultato)
                elif(lS[i]==''):
                    continue
                else:
                    if self.ControllaFullStop(i, lS) or lS[i]=='.':
                        Risultato='S'
                    else:
                        Risultato='C'
                        self.StampaSuFile(lS[i], Risultato, self.fileOutput)
                        #self.Stampa(lS[i], Risultato)
            l1=l2
            l2=l3
            l3='\n'

        self.fileInput.close()
        self.fileOutput.close()

#sposto il tmp nel fileOutput
        #print "SS 1 self.outputFileName:",self.outputFileName
        self.outputFileName = renameTmpFileName(self.outputFileName)
        #print "SS 2 self.outputFileName:",self.outputFileName
        return self.outputFileName
#

#
    def SentenceSplitting(self, inputFileName, destDir=None):
        if self.debug:
            print >> sys.stderr, "SS: inputFileName %s, destDir %s" %(inputFileName,destDir)
        return self.Parsing(inputFileName, destDir)



def usage():
    print """SentenceSplitter:
    Il file di training deve essere un file testo con una frase (con punto in fondo) per ogni riga
    Il file di test deve essere un file di testo in utf-8

    Usage:
    SentenceSplitter.py [options] file

    Options:
    -t                    training mode
    -i inputFile          input file
    -m file.model         modelFile
    -c confile.conf       set configuration file
    -r                    separa sistematicamente le frasi sul ritorno a capo (anche senza la presenza di punteggiatura)
    --help                display this help and exit
    --usage               display script usage

    """




def main():
    MODULE_NAME = "t2k.catenapy.SentenceSplitter"
    inputFile = ""
    outputFile = ""
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:e:m:rdtc:", ["debug", "input=", "help", "output=", "ritorni-a-capo", "training", "model"])
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    #valori di default
    confSentenceSplitterFile = resource_filename(MODULE_NAME, "Conf/it/sentenceSplitter.conf")
    modelSentenceSplitterFile = resource_filename(MODULE_NAME, "Modelli/it/SentenceSplitter/ME-IT-TrainingCorpus")
    output = None
    encode = 'utf-8'
    training = False
    debug = False
    outputFileName = None
    inputFileName = ""
    ritorniACapo = False
    #parsing degli argomenti
    for o, a in opts:
        if o in ("-h", "--help", "-u", "--usage"):
            usage()
            sys.exit()
        elif o in ("-o", "--output"):
            outputFileName = a
        elif o in ("-i", "--input"):
            inputFileName = a
        elif o in ("-c", "--conf"):
            confSentenceSplitterFile = a
        elif o in ("-m", "--model"):
            modelSentenceSplitterFile = a
        elif o in ("-e", "--encoding"):
            encode = a
        elif o in ("-r", "--ritorni-a-capo"):
            ritorniACapo = True
        elif o in ("-t", "--training"):
            training = True
        elif o in ("-d", "--debug"):
            debug = True
        else:
            assert False, "unhandled option"


    sentenceSplitter = SentenceSplitter()
    sentenceSplitter.setDebug(debug)
    sentenceSplitter.setEncode('utf-8')
    sentenceSplitter.setTraining(training)
    sentenceSplitter.setRitorniACapo(ritorniACapo)

    sentenceSplitter.CaricaSentenceSplitter(confSentenceSplitterFile, modelSentenceSplitterFile)
    if training:
        sentenceSplitter.Training(inputFileName)
    else:
        sentenceSplitter.SentenceSplitting(inputFileName)

if __name__ == "__main__":
    main()

