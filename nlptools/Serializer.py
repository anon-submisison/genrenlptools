#$Id: Serializer.py,v 1.2 2011-10-07 08:31:57 simone Exp $
import sys
import os.path
import getopt
import codecs
import re
from Utils import *
import cPickle
import time
import math

class Serializer:

    debug = False

    def __init__(self):
        self.rules = {}
        
    def setDebug(self, debug):
        self.debug = debug

    def setEncode(self, encode):
        self.encode = encode

    def readPickled(self, filename):
        with open(filename, 'rb') as f:
            entry = cPickle.load(f)     
        return entry

    def writePickled(self, filename, obj):
        with open(filename, 'wb') as f: 
            cPickle.dump(obj, f,2)            


def loadFromTextFile ():

    file = openFile ("Lessici/it/PosTagger/fullexMorph.tanl")

    HashMapLessico = {}
    for l in file:
        tokens = l.strip().split('\t')
        form  = tokens[0].lower()
        pos = tokens[1::2]
        lemma = tokens[2::2]
        try:
#            print form, ",",pos,",",lemma,len(pos)
            for index in range(len(pos)):
                HashMapLessico['%s:%s' % (form, pos[index])] = lemma[index].split('|')[0]
        except TypeError, e:
            print e, "(",index,")"
            exit(-1)
    return HashMapLessico


def main():

    s = Serializer()

    HashMapLessico = {}

    t0 = time.time()
    HashMapLessico = loadFromTextFile()
    t1 = time.time()
#    printHash(HashMapLessico,20)

    print "%f12" % (t1 - t0), "sec. - Hash created"  

    s.writePickled ("formario.pickled", HashMapLessico)
    t2 = time.time()
    print "%f" % (t2 - t1), "sec. - formario.pickled written"  

    HashMapLessico = s.readPickled("formario.pickled")

    t3 = time.time()
    print "%f" % (t3 - t2), "sec. - formario.pickled read"  
#    print "(",s.readPickled ("test.pickled"),")"
    #printHash(HashMapLessico)
if __name__ == "__main__":
    main()

