#$Id: Token.py,v 1.2 2013-01-14 14:26:55 felice Exp $
import os
import sys

# represent a single token in a sentence.
class Token:

	def __init__(self, items, id = '-1', pos = None, lemma=None):
		self.id = int(id)
		self.pos = pos
		self.lemma = lemma
		self.word = "_"
		#self.lemma = None
		self.cpos = "_"
		#self.pos = None
		self.mfeats = "_"
		self.head = "_"
		self.dep = "_"
		self.term = "_"
		self.ner = "_"
		self.coll = "_"
		self.children = "_"
		
		if isinstance(items, basestring):
			self.word = items

		elif isinstance(items, list):
			if (len(items) > 10 ):
				self.id = int(items[0])
				self.word = items[1]
				self.lemma = items[2]
				self.cpos = items[3]
				self.pos = items[4]
				self.mfeats = items[5]
				if not(self.head=="_"):
					self.head = int(items[6])
				else:
					self.head =items[6]
				self.dep = items[7]
				self.children = []
				self.term = items[8]
				self.ner = items[9]
				self.coll = items[10]
			if (len(items) > 9 ):
				self.id = int(items[0])
				self.word = items[1]
				self.lemma = items[2]
				self.cpos = items[3]
				self.pos = items[4]
				self.mfeats = items[5]
				if not(self.head=="_"):
					self.head = int(items[6])
				else:
					self.head =items[6]
				self.dep = items[7]
				self.children = []
				self.term = items[8]
				self.ner = items[9]
			if (len(items) > 8 ):
				self.id = int(items[0])
				self.word = items[1]
				self.lemma = items[2]
				self.cpos = items[3]
				self.pos = items[4]
				self.mfeats = items[5]
				if not(self.head=="_"):
					self.head = int(items[6])
				else:
					self.head =items[6]
				self.dep = items[7]
				self.children = []
				self.term = items[8]
			if (len(items) > 6 ):
				self.id = int(items[0])
				self.word = items[1]
				self.lemma = items[2]
				self.cpos = items[3]
				self.pos = items[4]
				self.mfeats = items[5]
				if not(self.head=="_"):
					self.head = int(items[6])
				else:
					self.head =items[6]
				self.dep = items[7]
				self.children = []
			if (len(items) > 5 ):
				self.id = int(items[0])
				self.word = items[1]
				self.lemma = items[2]
				self.cpos = items[3]
				self.pos = items[4]
				self.mfeats = items[5]
			else:
				self.word = items[0]
				if (len(items)>1):
					self.pos = items[1]
				else:
					self.pos = "_"
		else:
			print >> sys.stderr,'Token: Error type', type(items)


## 	def __init__(self, id, word, lemma, cpos, pos, mfeats, head, dep, tag9 = None, tag10 = None):
## 		self.id = int(id)
## 		self.word = word
## 		self.lemma = lemma
## 		self.cpos = cpos
## 		self.pos = pos
## 		self.mfeats = mfeats
## 		self.head = int(head)
## 		self.dep = dep
##                 self.children = []

	def __repr__(self):
		'\t'.join(filter(None, (str(self.id), self.word, self.lemma, self.cpos, self.pos, self.mfeats, str(self.head), self.dep, self.term, self.ner)))

	def setPos(self, pos):
		self.pos = pos
	
	def setWord(self,word):
		self.word = word
		
	def setId(self,id):
		self.id = id

	def printToken(self, outFile = sys.stderr):
		print >> outFile, self.word
