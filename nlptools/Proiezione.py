#!/usr/bin/python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------
#----------------------------------------------------------------------

# Proietta alcune caratteristiche linguistiche sul testo.

import sys
import unicodedata
import os.path
import getopt
import codecs
from Conf import Conf
from Token import Token
from Sentence import Sentence
from Utils import *


def visitaCatenePrep(inizio, tok, tokens, altezzaSUB, TOT_Catene_Parziali, lunghezzaCatena, Visitato, ALTEZZASub, IDinizio):
	    Visitato.append(tok.id)
	    Catena=False
	    fid=0
	    for figlio in tok.children:
		if figlio.dep=="prep":
			altezzaSUB+=1
			fid =  figlio.id
	    if fid!=0:
		    for figlio in tokens[fid-1].children:
			    if (figlio.dep in ('comp','comp_temp','comp_loc')):
				    inizio, altezzaSUB, TOT_Catene_Parziali, lunghezzaCatena, ALTEZZASub, Visitato, IDinizio = visitaCatenePrep(inizio, figlio, tokens, altezzaSUB, TOT_Catene_Parziali, lunghezzaCatena, Visitato, ALTEZZASub, IDinizio)
				    Catena=True
	    if not(Catena):
		if altezzaSUB:
	            #print 'altezzaSUB:', altezzaSUB #errr
		    TOT_Catene_Parziali+=1.0
		    if altezzaSUB not in ALTEZZASub:
			    IDinizio[altezzaSUB]=""
			    ALTEZZASub[altezzaSUB]=0.0
		    IDinizio[altezzaSUB]+=str(inizio)+" "
		    ALTEZZASub[altezzaSUB]+=1.0
		    lunghezzaCatena+=altezzaSUB
	    altezzaSUB=altezzaSUB-1
	    #print "PREP:", altezzaSUB
	    #print "fine_prep_ALTEZZASub:", ALTEZZASub
	    return inizio, altezzaSUB, TOT_Catene_Parziali, lunghezzaCatena, ALTEZZASub, Visitato, IDinizio

class TokenProiezione:
	def __init__(self, tok):
		self.id = tok.id
		self.word = tok.word
		self.complexWord = "_"
		self.feat = "_"
		
class Proiezione():
	
	def __init__(self,  encode='utf-8'):
		self.Dizionario = {}
		self.encode = encode
                self.espressioniGenericamenteSubordinanti={}
                self.espressioniGenericamenteSubordinanti[1]=["tramite"]
                self.espressioniGenericamenteSubordinanti[2]=["contestualmente a","come da"]
                self.espressioniGenericamenteSubordinanti[3]=["nel caso di","al costo di","una volta che"]
                
	def setLessico(self,lessico):
		self.lessico = lessico
	
	def CaricaDizionario(self):
		try:
			lessicoFile = codecs.open(self.lessico, encoding=self.encode, mode='r')
		except IOError:
			print >> sys.stderr,'Il file', self.lessico, 'non  esiste!'
			exit(-1)
		for l in lessicoFile:
			t = l.strip().split('\t')
			self.Dizionario[t[0]]=(t[1].split())[0]
        
	def StampaOutSoloLessico(self, tok, result, fileOutput=sys.stdout):
		if result=="Facile":
			print >> fileOutput, '\t'.join([str(tok.id) , tok.word, "_"])
			#print >> fileOutput, '\t'.join([str(tok.id) , tok.word])
		else:
			print >> fileOutput, '\t'.join([str(tok.id) , tok.word, "C"]) #Enc

	def StampaOut(self, tokensProiez, gruppoCopulaInizioFrase, IDinizio, CumuliAGG, CumuliSOST, espressioniGenericamenteSubord, VerbiSupport, fileOutput=sys.stdout):
		for tokProiez in tokensProiez:
			print >> fileOutput, '\t'.join([str(tokProiez.id) , tokProiez.word, tokProiez.complexWord, tokProiez.feat])
		print >> fileOutput, '\t'.join(["feat" , "Lunghezza_Frase", str(len(tokensProiez))])
		print >> fileOutput, '\t'.join(["feat" , "GruppoCopula+AggettivoInizioFrase", str(gruppoCopulaInizioFrase)])
                #Giustapposizioni Nominali:
                PresenzaCumuloNom=False
		stringaOutCumuloNom=""
                for lenGiustNominali in CumuliSOST:
                    if lenGiustNominali>2:
                        PresenzaCumuloNom=True
                        stringaOutCumuloNom+="\t"+"L:"+str(lenGiustNominali)+" ID:"+str((CumuliSOST[lenGiustNominali]).strip())+""
                stringa="feat"+"\t"+"GiustapposizioniNominali"
                if PresenzaCumuloNom:
                    stringa+=stringaOutCumuloNom
                else:
                    stringa+="\t"+str(PresenzaCumuloNom)
                print >> fileOutput, stringa
                #VerbiSupport
                stringa="feat"+"\t"+"VerbiSupporto"
                if VerbiSupport:
                    for elem in VerbiSupport:
                        stringa+="\t"+"ID:"+str(elem)
                else:
                    stringa+="\t"+"False"
                print >> fileOutput, stringa
                
                #espressioniGenericamenteSubord
                stringa="feat"+"\t"+"EspressioniGenericamenteSubord"
                if espressioniGenericamenteSubord:
                    for elem in espressioniGenericamenteSubord:
                        stringa+="\t"+"ID:"+str(elem)+" VAL:"+espressioniGenericamenteSubord[elem]
                else:
                    stringa+="\t"+"False"
                print >> fileOutput, stringa
                #Cumuli AGG
                PresenzaCumuloAGG=False
		stringaOutCumuloAGG=""
                for lenGiustAGG in CumuliAGG:
                    if lenGiustAGG>2:
                        PresenzaCumuloAGG=True
                        stringaOutCumuloAGG+="\t"+"L:"+str(lenGiustAGG)+" ID:"+str((CumuliAGG[lenGiustAGG]).strip())+""
                stringa="feat"+"\t"+"CumuliAGGETTIVALI"
                if PresenzaCumuloAGG:
                    stringa+=stringaOutCumuloAGG
                else:
                    stringa+="\t"+str(PresenzaCumuloAGG)
                print >> fileOutput, stringa
		#CatenePrep
		PresenzaCumuloPrep=False
		stringaOutCumuloPrep=""
                listaID=[]
                for lenCatena in IDinizio:
                    if lenCatena>3:
                        PresenzaCumuloPrep=True
                        listaS=(IDinizio[lenCatena]).strip().split(' ')
                        for idP in listaS:
                            if not(int(idP) in listaID):
                                listaID.append(int(idP))
                listaID.sort()
                for idP in listaID:
                    stringaOutCumuloPrep+=str(idP)+" "
##OLD:
## 		for lenCatena in IDinizio:
## 			if lenCatena>1:
## 				PresenzaCumuloPrep=True
## 				#stringaOutCumuloPrep+="\t"+"L:"+str(lenCatena)+" ID:"+str((IDinizio[lenCatena]).strip())+""
		stringa="feat"+"\t"+"CumuliPREPOSIZIONALI"
		if PresenzaCumuloPrep:
			stringa+="\t"+stringaOutCumuloPrep.strip()
		else:
			stringa+="\t"+str(PresenzaCumuloPrep)
		print >> fileOutput, stringa
		print >> fileOutput	

	def FeatLessicali(self, tok, tokProiez):
		result="Facile"
		if not(tok.pos in ["SP", "SA"]) and  not(tok.pos[0] in ["X", "F", "E", "N", "R"]):
			if not(tok.lemma in self.Dizionario):
				tokProiez.complexWord="C"
				#result="Difficile"
		#self.StampaOutSoloLessico(tok, result, self.fileOutput)
		return tokProiez

	def zione(self, tok, tokProiez):
		if tok.pos in ["S"]:
			if tok.word[-5:]=="zione" and len(tok.word)>7:
				#N:Probabile Nominalizzazione
				tokProiez.feat="N"
		return tokProiez

	def gerundio(self, tok, tokProiez):
		if tok.id==1 and tok.pos in ["V"] and tok.mfeats=="mod=g":
			tokProiez.feat="G"
		return tokProiez

	def gruppoCopula(self, tok, tokProiez, tokens):
		gruppoCopulaInizioFrase=False
		if tok.pos[0]=="A":
			if tok.dep=="pred" and tokens[tok.head-1].lemma=="essere" and tokens[tok.head-1].pos=="V":
				gruppoCopulaInizioFrase=True
		return gruppoCopulaInizioFrase

	# 
	def prolettica(self, tok, tokProiez, tokens):
		trovato1=False
		trovato2=False
		if tok.pos=="V" and tok.lemma=="essere":
			for figlio in tok.children:
				if figlio.dep=="subj" and figlio.id < tok.id:
					trovato1=True
				if figlio.dep=="arg" and figlio.id > tok.id and figlio.lemma=="che":
					trovato2=True
			if trovato1 and trovato2:
				tokProiez.feat="PR"
		return tokProiez

        def cumuliAggettivi(self, tok, tokens, CumuliAGG, VisitatoA):
            if tok.pos=="A" and not (tok.id in  VisitatoA):
                VisitatoA.append(tok.id)
                lenCumulo=1
                for i in range(tok.id, len(tokens)):
                    if tokens[i].pos=="A":
                        VisitatoA.append(i+1)
                        lenCumulo+=1
                    else:
                        break
                if not(lenCumulo in CumuliAGG):
                    CumuliAGG[lenCumulo]=""
                CumuliAGG[lenCumulo]+=str(tok.id)+" "
                
            return CumuliAGG, VisitatoA

        def cumuliSostantivi(self, tok, tokens, CumuliSOST, VisitatoS):
            if tok.pos=="S" and not (tok.id in  VisitatoS):
                VisitatoS.append(tok.id)
                lenCumulo=1
                for i in range(tok.id, len(tokens)):
                    if tokens[i].pos=="S":
                        VisitatoS.append(i+1)
                        lenCumulo+=1
                    else:
                        break
                if not(lenCumulo in CumuliSOST):
                    CumuliSOST[lenCumulo]=""
                CumuliSOST[lenCumulo]+=str(tok.id)+" "
            return CumuliSOST, VisitatoS

 	def ppchain(self, tok, tokens, altezzaSUB, TOT_Catene, lunghezzaCatena, Visitato, CATENE_PREP, IDinizio):
            if not(tok.id in Visitato):
                if (tok.dep in ('comp','comp_temp','comp_loc')):
                    TOT_Catene_Parziali=0
                    inizio, altezzaSUB, TOT_Catene_Parziali, lunghezzaCatena, CATENE_PREP, Visitato, IDinizio=visitaCatenePrep(tok.id, tok, tokens, 0, 0, lunghezzaCatena, Visitato, CATENE_PREP, IDinizio)
                    TOT_Catene+=TOT_Catene_Parziali
            return altezzaSUB, TOT_Catene, lunghezzaCatena, CATENE_PREP, Visitato, IDinizio

        def cercaEspressioniGenericamenteSubordinanti(self, tok, tokens, espressioniGenericamenteSubord):
            for j in [1,2,3]:
                forma=""
                for idF in range(j):
                    if len(tokens)>tok.id-1+idF:
                        forma+=tokens[tok.id-1+idF].word+" "
                forma=forma.strip()
                for espr in self.espressioniGenericamenteSubordinanti[j]:
                    if forma==espr:
                        espressioniGenericamenteSubord[tok.id]=espr
                        #print "ECCO:", tok.id, tok.word, 
                        break
            return espressioniGenericamenteSubord
        
        def verbiImpersonali(self, tok, tokProiez, tokens):
            if tok.pos=="V" and (("num=s|per=3" in tok.mfeats) or ("num=s|mod=p" in tok.mfeats)):
                trovato1=False
                for figlio in tok.children:
                    if figlio.lemma=="si" and  figlio.pos=="PC" and figlio.dep=="clit":
                        trovato1=True
                    if figlio.dep=="subj":
                        trovato1=False
                        break
                if trovato1:
                    tokProiez.feat="I"
            return tokProiez

        def verbiSupporto(self, tok, tokens, VerbiSupport):
            if tok.pos=="V" and tok.lemma in ["essere","avere"]:
                for figlio in tok.children:
                    if figlio.pos=="S" and (figlio.dep=="obj" or figlio.dep=="pred"):
                        VerbiSupport[tok.id]=1
            return VerbiSupport

        def verbiPassivi(self, tok, tokProiez, tokens):
            verboPassivo=False
            if tok.pos=="V" and "mod=p" in tok.mfeats:
                for figlio in tok.children:
                    if figlio.cpos=="V" and  figlio.dep=="aux":
                        for figlio1 in figlio.children:
                            if figlio1.cpos=="V" and  figlio1.dep=="aux":
                                verboPassivo=True
                        break
                if verboPassivo:
                    tokProiez.feat="PS"
            return tokProiez

        def gerundioInciso(self, tok, tokProiez, tokens, probInciso):
            if probInciso:
                suc=False
                if tok.pos=="V" and tok.mfeats=="mod=g":
                    for i in range(tok.id, len(tokens)-1):
                        if tokens[i].word==",":
                            suc=True
                            break
##                     print "1:", tok.word
##                     for figlio in tok.children:
##                         if figlio.cpos=="F":
##                             if figlio.id < tok.id:
##                                 pass
##                                 #print "2:", figlio.word
##                                 #prec=True
##                             else:
##                                 if figlio.id!=len(tokens):
##                                     print "3:", figlio.word
##                                     suc=True
##                     #if prec and suc:
                    if suc:
                        tokProiez.feat="GI"
            return tokProiez

        def analyze(self, tokens):
		tokensProiez  = []
		VisitatoP=[]
                VisitatoA=[]
                VisitatoS=[]
                probInciso=False
		TOT_Catene=0
		IDinizio={}
		CATENE_PREP={}
		lunghezzaCatena=0
                CumuliAGG={}
                CumuliSOST={}
                VerbiSupport={}
                espressioniGenericamenteSubord={}
		gruppoCopulaInizioFrase=False
		Sentence(tokens)
		for tok in tokens:
                    if tok.lemma==",":
                        probInciso=True
                    tokProiez = TokenProiezione(tok)
                    tokProiez=self.FeatLessicali(tok, tokProiez)
                    tokProiez=self.zione(tok, tokProiez)
                    tokProiez=self.gerundio(tok, tokProiez)
                    tokProiez=self.prolettica(tok, tokProiez, tokens)
                    tokProiez=self.verbiImpersonali(tok, tokProiez, tokens)
                    tokProiez=self.verbiPassivi(tok, tokProiez, tokens)
                    tokProiez=self.gerundioInciso(tok, tokProiez, tokens, probInciso)
                    if tok.head==1 and not(gruppoCopulaInizioFrase):
                            gruppoCopulaInizioFrase=self.gruppoCopula(tok, tokProiez, tokens)
                    espressioniGenericamenteSubord=self.cercaEspressioniGenericamenteSubordinanti(tok, tokens, espressioniGenericamenteSubord)
                    CumuliAGG, VisitatoA=self.cumuliAggettivi(tok, tokens, CumuliAGG, VisitatoA)
                    CumuliSOST, VisitatoS=self.cumuliSostantivi(tok, tokens, CumuliSOST, VisitatoS)
                    altezzaSUB, TOT_Catene, lunghezzaCatena, CATENE_PREP, VisitatoP, IDinizio=self.ppchain(tok, tokens, 0, 0, lunghezzaCatena, VisitatoP, CATENE_PREP, IDinizio)
                    VerbiSupport=self.verbiSupporto(tok, tokens, VerbiSupport)
                    tokensProiez.append(tokProiez)
		self.StampaOut(tokensProiez, gruppoCopulaInizioFrase, IDinizio, CumuliAGG, CumuliSOST, espressioniGenericamenteSubord, VerbiSupport, self.fileOutput)

	def Parsing(self, inputFileName, destDir = None):
		(self.fileInput, self.fileOutput, self.outputFileName) = openIOFiles(inputFileName, destDir, ".prz.tmp")
		tokens=[]
		tokensProiez=[]
		while True:
			line = self.fileInput.readline()
			if ('<doc ' in line) or ("VALORE" in line) or ('</doc>' in line):
				continue
			if line == '\n':
				if len(tokens)>0:
					self.analyze(tokens)
				tokens = []
				tokensProiez = []
				continue
			if line == '':
				break
			tok = Token(line.split())
			tokens.append(tok)
		self.outputFileName = renameTmpFileName(self.outputFileName)

	def CaricaProiezione(self):
		self.Dizionario={}
		self.CaricaDizionario()
					
	def Proiezione(self, inputFileName, destDir=None):
		self.Parsing(inputFileName, destDir)

def usage():
	print """Proiezione.py:
	Input file must be in CoNLL format.

	Usage:
	  Proiezione.py [options] [file]
	
	Options:
	-d                        set FileDizionario
	-i    nomefile            nome del file da monitorare documento per documento
  	"""

def main():
	try:
		long_opts = ['os', 'help', 'usage']
		opts, args = getopt.gnu_getopt(sys.argv[1:], 'h d: i:',long_opts)
	except getopt.GetoptError:
		usage()
		sys.exit()
	#Valori di default:
	confronto=False
	scriptpath = os.path.abspath(os.path.dirname(sys.argv[0]))
	fileLessico=scriptpath+"/"+'Lessici/it/Readability/DizionarioFondamentaleVodafone'
	inputFileName=""
	for opt, arg in opts:
	    if opt in ('-h', '--help'):
		usage()
		sys.exit()
	    elif opt == '--usage':
		usage()
		sys.exit()   
	    elif opt == '-d':
		fileLessico=arg
	    elif opt == '-i':
		inputFileName=arg
	    else:
		    assert False, "unhandled option"

	p = Proiezione()
	#p.setEncode(encode)
	p.setLessico(fileLessico)
	p.CaricaProiezione()
	p.Proiezione(inputFileName)
	
if __name__ == "__main__":
    main()
