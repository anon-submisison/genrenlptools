#/bin/env python

import sys
import math
import os.path
import getopt
import codecs
from Conf import Conf
from Token import Token
from Sentence import Sentence
from Utils import *
import cPickle as pickle
from collections import Counter, defaultdict

# Dato un Dizionario restituisce una lista di coppie (Chiave, Valore) ordinate per valore
def Ordina(dict):
	return sorted(dict.items(), key=lambda x: x[1], reverse=True)

class Readability ():

	debug=False
	ver = "$RCSfile: Readability.py,v $ $Revision: 1.1.1.1 $"

	def __init__(self,  encode='utf-8'):
	    self.Lexicon = {}
	    self.valutazione = True #mettere a True per valutare
	    self.encode = encode
	    self.stylo_feats = None
	    self.outcomes = []
	    self.labels = []
	    self.samples = []
	    self.ListaFeatures = [[], []]
	    self.min_samples = None
	    self.max_samples = None
	    self.doc_names = []
	    self.forme_doc_counter_new = defaultdict(lambda : defaultdict(Counter))
	    self.forme_global_counter_new = defaultdict(Counter)

	    self.labels_value = []
	    #self.printTextAnalisi = False

	def setEncode(self, encode):
		self.encode = encode

	def setLessico(self,lessico):
		self.lessico = lessico

	def setTraining(self,training):
		self.training = training

	def setPrintText(self, printText):
		self.printText = printText

	#def setPrintTextAnalisi(self, printTextAnalisi):
		#self.printTextAnalisi = printTextAnalisi

	def setDump(self, dump):
		self.dump = dump

	def setDebug(self, debug):
		self.debug = debug

	def setInfLength(self, infLength):
		self.infLength = infLength

	def setInfSenteces(self, infSenteces):
		self.infSenteces = infSenteces

	#Carica il Dizionario Fondamentale di DeMauro:
	def CaricaDizionarioFondamentale(self):
		try:
			lessicoFile = codecs.open(self.lessico, encoding=self.encode, mode='r')
		except IOError:
			print >> sys.stderr,'Il file', self.lessico, 'non  esiste!'
			exit(-1)
		for l in lessicoFile:
			t = l.strip().split('\t')
			self.Lexicon[t[0]]=(t[1].split())[0]

	def visita(self, tok, tokens):
	    if not(tok.children):
		altezza=0
	    else:
		altezzaMax=0
		for tokC in tok.children:
		    tmp=self.visita(tokC, tokens)
		    if tmp>altezzaMax:
			altezzaMax=tmp
		    altezza=altezzaMax+1
	    return altezza

	def visitaSubordinate(self, tok, tokens, altezzaSUB, TOT_Subordinate, altezzaSubordinantiTOT, ALTEZZASub):
	    trovato=False
	    #print altezzaSUB
	    for figlio in tok.children:
		if (figlio.cpos=='V' and (figlio.dep=='mod' or figlio.dep=='arg')):
		    trovato=True
		    break
		if ((figlio.pos=='CS' or figlio.pos=='B' or figlio.cpos=='E') and (figlio.dep=='arg' or figlio.dep=='mod' or figlio.dep=='comp')):
		    trovato=True
		    break
	    if not(trovato):
		if altezzaSUB:
		    TOT_Subordinate+=1.0
		    #print "TOT1__:", TOT_Subordinate #oggi
		    #print #err
		    #print 'altezzaSUB:', altezzaSUB  #oggi#err
		    #print "pre1_ALTEZZASub:", ALTEZZASub #oggi
		    if not(altezzaSUB in ALTEZZASub):
			ALTEZZASub[altezzaSUB]=0.0
		    #else:
		    ALTEZZASub[altezzaSUB]+=1.0
		    #print "1_ALTEZZASub:", ALTEZZASub #oggi
		    altezzaSubordinantiTOT+=altezzaSUB
		    #print 'TOT_Subordinate:', TOT_Subordinate
		    #print 'altezzaTOT:', altezzaSubordinantiTOT
		    #altezzaSUB=0 #qui
	    else:
	        beccato=False  #n
		for figlio in tok.children:
		    if (figlio.cpos=='V' and (figlio.dep=='mod' or figlio.dep=='arg')):
		        #albero+=" "+figlio.word #n
			#print "ECCOMI!!", figlio ####errrrr
			altezzaSUB+=1
			TOT_Subordinate, altezzaSubordinantiTOT, ALTEZZASub, altezzaSUB=self.visitaSubordinate(figlio, tokens, altezzaSUB, TOT_Subordinate, altezzaSubordinantiTOT, ALTEZZASub)	 #n
			#print "Ric:", ALTEZZASub #oggi
			beccato=True
		    #for figlio in tok.children: #vecchio
		    if ((figlio.pos=='CS' or figlio.pos=='B' or figlio.cpos=='E') and (figlio.dep=='arg' or figlio.dep=='mod' or figlio.dep=='comp')):
			#print "ECCOMI!!", figlio ####errrrr
			#print 'ecco2'
			for figlio2 in figlio.children:
                             #print figlio2
			     if figlio2.dep=='sub' or (figlio2.dep=='prep' and figlio2.cpos=='V'):
				#print 'ECCOMI!!!!!!!!!2'
				altezzaSUB+=1
				#albero+=" "+figlio2.word #n
				TOT_Subordinate, altezzaSubordinantiTOT, ALTEZZASub, altezzaSUB=self.visitaSubordinate(figlio2, tokens, altezzaSUB,  TOT_Subordinate, altezzaSubordinantiTOT, ALTEZZASub)
				#print "Ric:", ALTEZZASub #oggi
				beccato=True
		if not(beccato) and altezzaSUB:
		    TOT_Subordinate+=1.0
		    #print "TOT2__:", TOT_Subordinate  #oggi
		    #print #err
		    #print 'altezzaSUB:', altezzaSUB #err #oggi
		    #print "pre_ALTEZZASub:", ALTEZZASub #oggi
		    if not(altezzaSUB in ALTEZZASub):
			    ALTEZZASub[altezzaSUB]=0.0
		    #else:
		    ALTEZZASub[altezzaSUB]+=1.0
		    altezzaSubordinantiTOT+=altezzaSUB
		    #print "ALTEZZASub:", ALTEZZASub #oggi
                    #print 'TOT_Subordinate:', TOT_Subordinate
		    #print 'altezzaTOT:', altezzaSubordinantiTOT
		    #altezzaSUB=0 #qui
		    #print "AlberoRicorsivo:", albero #errrrr
		    #albero=""  #errrrr
	    altezzaSUB=altezzaSUB-1
	    #print "fine_ALTEZZASub:", ALTEZZASub #oggi
	    #print altezzaSUB #errrr
	    return TOT_Subordinate, altezzaSubordinantiTOT, ALTEZZASub, altezzaSUB


        def visitaCatenePrep(self, tok, tokens, altezzaSUB, TOT_Catene_Parziali, lunghezzaCatena, Visitato, ALTEZZASub):
	    Visitato.append(tok.id)
	    Catena=False
	    fid=0
	    for figlio in tok.children:
		if figlio.dep=="prep":
			altezzaSUB+=1
			fid =  figlio.id
	    if fid!=0:
		    for figlio in tokens[fid-1].children:
			    if (figlio.dep in ('comp','comp_temp','comp_loc')):
				    altezzaSUB, TOT_Catene_Parziali, lunghezzaCatena, ALTEZZASub, Visitato = self.visitaCatenePrep(figlio, tokens, altezzaSUB, TOT_Catene_Parziali, lunghezzaCatena, Visitato, ALTEZZASub)
				    Catena=True
	    if not(Catena):
		if altezzaSUB:
	            #print 'altezzaSUB:', altezzaSUB #errr
		    TOT_Catene_Parziali+=1.0
		    if altezzaSUB not in ALTEZZASub:
			    ALTEZZASub[altezzaSUB]=0.0
		    ALTEZZASub[altezzaSUB]+=1.0
		    lunghezzaCatena+=altezzaSUB
	    altezzaSUB=altezzaSUB-1
	    #print "PREP:", altezzaSUB
	    #print "fine_prep_ALTEZZASub:", ALTEZZASub
	    return altezzaSUB, TOT_Catene_Parziali, lunghezzaCatena, ALTEZZASub, Visitato


	#Legenda:(dal piu semplice al piu complesso)
	#1)FO: Fondamentale;
	#2)AU: Alto Uso
	#3)AD: Alta Disponibilta
	#DizFondamentale ={"AD":0.0, "AU":0.0, "FO": 0.0}

	def FeatWordCount(self, DOC, cur_doc_name):
	    Features={}
	    if not self.conf.top_n_words:
		    return Features
	    for num_gram, num_words, in self.conf.top_n_words:
		    for tokens in DOC:
			for idx, tok in enumerate(tokens):
			    if not(tok.cpos=='F'):
				#Rapporto Types/Token: (sul Lemma e sulla Forma)
				#Rapporto Types/Token: (sul Lemma e sulla Forma)
				    if idx+num_gram > len(tokens):
					    print "SKIIIIIP"
					    continue
                                    # Normalize with lower
				    str_value = "_".join([x.lemma for x in tokens[idx:idx+num_gram]]).lower()
				    self.forme_doc_counter_new[num_gram][cur_doc_name][str_value]+=1
				    self.forme_global_counter_new[num_gram][str_value]+=1
                    # Frequenza relativa
		    for ngram in self.forme_doc_counter_new[num_gram][cur_doc_name]:
			    self.forme_doc_counter_new[num_gram][cur_doc_name][ngram] = \
				self.forme_doc_counter_new[num_gram][cur_doc_name][ngram] / float(len(tokens))


	    return Features

	def FeatLessicali(self, DOC, cur_doc_name):
	    Forme={}
	    Lemmi={}
	    LunghezzaParole=0.0
	    Features={}
	    conta=0.0
	    if self.conf.dizionario:
		Features['CARD_LEMMI']=0.0
		Features['@DIZ-TYPE']={}
		Features['Diz_AD']=0.0
		Features['Diz_AU']=0.0
		Features['Diz_FO']=0.0
		Features['Diz_TrovateInDizionario']=0.0
	    TypeToken=0.0
	    cercatiInDizionario=0.0
	    for tokens in DOC:
		for tok in tokens:
		    if not(tok.cpos=='F'):
			#Rapporto Types/Token: (sul Lemma e sulla Forma)
			if conta == self.conf.rangeTypeToken and self.conf.typeToken:
			    TypeTokenF=len(Forme)
			    TypeTokenL=len(Lemmi)
			conta+=1
			#Rapporto Types/Token: (sul Lemma e sulla Forma)
			if tok.word in Forme:
			    Forme[tok.word]+=1.0
			  #  self.forme_doc_counter[cur_doc_name][tok.word]+=1
			  #  self.forme_global_counter[tok.word]+=1
			else:
			    Forme[tok.word]=1.0
			#    self.forme_doc_counter[cur_doc_name][tok.word]+=1
			#    self.forme_global_counter[tok.word]+=1
			if tok.lemma in Lemmi:
			    Lemmi[tok.lemma]+=1.0
			else:
			    Lemmi[tok.lemma]=1.0
			if (self.conf.mediaLunghezzaParole):
			    LunghezzaParole+=float(len(tok.word))
			#print "ECCO:", tok.id
			#print "ECCO:", tok.pos
			#print "ECCO:", tok.cpos
			#print "ECCO:", tok.word.encode("utf-8")
			#print "ECCO:", tok.lemma.encode("utf-8")
			if (self.conf.dizionario) and not(("c:"+tok.cpos in self.conf.featValue["POS_ESCLUSE_DA_DIZIONARIO"]) or ("p:"+tok.pos in self.conf.featValue["POS_ESCLUSE_DA_DIZIONARIO"])):# and ((tok.word in self.Lexicon) or (tok.lemma in self.Lexicon) or ((tok.lemma).lower() in self.Lexicon) or ((tok.word).lower() in self.Lexicon)):
			    #Features['Diz_TrovateInDizionario']+=1.0
			    cercatiInDizionario+=1.0

			    if (not(tok.lemma in Features['@DIZ-TYPE'])):
				Features['@DIZ-TYPE'][tok.lemma]=1.0
				if tok.lemma in self.Lexicon:
				    Features['Diz_'+self.Lexicon[tok.lemma]]+=1.0
				elif((tok.lemma).lower() in self.Lexicon):
				    Features['Diz_'+self.Lexicon[(tok.lemma).lower()]]+=1.0
				elif((tok.word).lower() in self.Lexicon):
				    Features['Diz_'+self.Lexicon[(tok.word).lower()]]+=1.0
				elif (tok.word in self.Lexicon):
				    Features['Diz_'+self.Lexicon[tok.word]]+=1.0
	    if self.conf.dizionario:
		if cercatiInDizionario==0:
		    Features['CARD_LEMMI']=1.0 #per evitare divisioni per 0
		else:
		    Features['CARD_LEMMI']=cercatiInDizionario      #len(Lemmi)*1.0 <-- vecchio

	    #la variabile "TypeTokenF" non esiste in certi casi, controllare la variabile conta ...
	    if conta<=self.conf.rangeTypeToken and self.conf.typeToken:
		TypeTokenF=len(Forme)
		TypeTokenL=len(Lemmi)
	    if self.conf.typeToken:
		Features['TypeTokenF']=TypeTokenF
		Features['TypeTokenL']=TypeTokenL
	    if self.conf.mediaLunghezzaParole:
		Features['MediaLunghezzaParole']=LunghezzaParole
	    return Features

	def FeatPos(self, DOC, Features):
	    if self.conf.densitaLessicale:
		Features['DensitaLessicale']=0.0
	    if self.conf.cpos:
		Features['A']=0.0
		Features['B']=0.0
		Features['C']=0.0
		Features['D']=0.0
		Features['E']=0.0
		Features['F']=0.0
		Features['I']=0.0
		Features['N']=0.0
		Features['P']=0.0
		Features['R']=0.0
		Features['S']=0.0
		Features['T']=0.0
		Features['V']=0.0
		Features['X']=0.0
	    if self.conf.verboModo:
		Features['V+p']=0.0
		Features['V+i']=0.0
		Features['V+g']=0.0
		Features['V+m']=0.0
		Features['V+c']=0.0
		Features['V+d']=0.0
		Features['V+f']=0.0
		Features['VA+c']=0.0
		Features['VA+m']=0.0
		Features['VA+f']=0.0
		Features['VA+g']=0.0
		Features['VA+p']=0.0
		Features['VA+d']=0.0
		Features['VA+i']=0.0
		Features['VM+m']=0.0
		Features['VM+p']=0.0
		Features['VM+d']=0.0
		Features['VM+c']=0.0
		Features['VM+f']=0.0
		Features['VM+i']=0.0
		Features['VM+g']=0.0
	    for tokens in DOC:
		for tok in tokens:
		    if self.conf.densitaLessicale:
			if (tok.cpos in ['S', 'A', 'B'] or (tok.cpos=='V' and not(tok.pos=='VA'))):
			    #print 'piene:', tok.pos
			    Features['DensitaLessicale']+=1.0
		    if self.conf.cpos:
			if tok.cpos in Features:
			    Features[tok.cpos]+=1.0
			if self.conf.verboModo and tok.cpos=='V':
			    posizione=(tok.mfeats).find("mod=")
			    if not(posizione==-1):
				posMfeat=tok.pos+'+'+(tok.mfeats)[posizione+4]
				if posMfeat in Features:
				    Features[posMfeat]+=1
		#print >> sys.stderr, '.',
	    return Features

	def FeatSintattiche(self, DOC, Features):
	    lunghezzaLinkmax=0.0
	    lunghezzalink=0.0
	    TOT_Catene=1.0 #per evitare divisioni per 0
	    ContaLinkEntranti=0.0 #per evitare divisioni per 0
	    lunghezzaCatena=0.0
	    NumeroLinkEntrantiAttuale=0
	    TotaleValidiPerLunghezzaLink=1.0 #per evitare divisioni per 0
	    lunghezzaLink=0.0
	    lunghezzaLinkmax=0.0
	    lunghezzaLinkMax=0.0
	    DirezioneDipendenzeSO={'subj_pre':0.0, 'subj_post':0.0, 'obj_pre':0.0, 'obj_post':0.0}
	    DirezioneDipendenze={'subord_pre':0.0, 'subord_post':0.0}
	    FrasiSubordinate=0.0
	    SubordinateInfinitive=0.0
	    SubordinateCompletive=0.0
	    TOT_Subordinate=1.0 #per evitare divisioni per 0
	    altezzaSubordinantiTOT=0.0
	    ALTEZZASubP={}
	    ALTEZZASub={}
	    altezzaSUB=0
	    ARG_Special={}
	    MOD_Special={}
	    COMP_Special={}
	    #altezza
	    nROOT=0.0
	    altezza=0.0
	    altezzaMax=0.0
	    altezzaTOT=0.0
	    altezzaMAXTOT=0.0
	    ##
	    rootVerbali=0.0
	    LINK_ENTRANTI={}
	    CATENE_PREP={}
	    if self.conf.catenaPrep:
		    Visitato=[]

	    if self.conf.dependencyT:
		Features['arg']=0.0
		Features['aux']=0.0
		Features['clit']=0.0
		Features['comp']=0.0
		Features['comp_ind']=0.0
		Features['comp_loc']=0.0
		Features['comp_temp']=0.0
		Features['con']=0.0
		Features['concat']=0.0
		Features['conj']=0.0
		Features['det']=0.0
		Features['dis']=0.0
		Features['disj']=0.0
		Features['mod']=0.0
		Features['mod_rel']=0.0
		Features['mod_loc']=0.0
		Features['mod_temp']=0.0
		Features['modal']=0.0
		Features['neg']=0.0
		Features['obj']=0.0
		Features['pred']=0.0
		Features['pred_loc']=0.0
		Features['pred_temp']=0.0
		Features['prep']=0.0
		Features['punc']=0.0
		Features['ROOT']=0.0
		Features['sub']=0.0
		Features['subj']=0.0
		Features['subj_pass']=0.0
	    for tokens in DOC:
		#print "ECCO:", tokens #err
		#for tok in tokens:  #err
			#print tok.word.encode("utf-8") #err
		Sentence(tokens)

		lunghezzaLinkmax=0.0
		altezzaTOT=0.0
		for i in range(len(tokens)):
		    tok=tokens[i]
		    #scorro la frase:
		    if self.conf.archiEntranti:
			if tokens[i].pos=='V':
			    NumeroLinkEntrantiAttuale=0.0
			    for tokC in tokens[i].children:
				if tokC.dep not in ('con','conj','dis','disj', 'sub','punc', 'prep', 'mod'):
				    ContaLinkEntranti+=1
				    NumeroLinkEntrantiAttuale+=1

			    if NumeroLinkEntrantiAttuale in LINK_ENTRANTI:
				LINK_ENTRANTI[NumeroLinkEntrantiAttuale]+=1
			    else:
				LINK_ENTRANTI[NumeroLinkEntrantiAttuale]=1
		    if self.conf.catenaPrep and not(tokens[i].id in Visitato):
			if (tokens[i].dep in ('comp','comp_temp','comp_loc')):
				TOT_Catene_Parziali=0
				altezzaSUB, TOT_Catene_Parziali, lunghezzaCatena, CATENE_PREP, Visitato=self.visitaCatenePrep(tokens[i], tokens, 0, 0, lunghezzaCatena, Visitato, ALTEZZASubP)
				TOT_Catene+=TOT_Catene_Parziali
		    #durante l'analisi della frase:
		    if self.conf.altezzaAlbero:
			if (tok.dep=='ROOT') and (tok.head==0):
			    altezza= self.visita(tok, tokens)
			    nROOT+=1.0
			    altezzaTOT+=altezza
		    if self.conf.rootVerbali:
			if (tok.dep=='ROOT') and (tok.head==0):
			    if tok.cpos=='V':
				rootVerbali+=1.0
		    if self.conf.dependencyT:
			if tok.dep in Features:
			    Features[tok.dep]+=1.0
		    if self.conf.lunghezzaLink:
			if tok.head!=0 and tok.cpos[0]!='F':
			    TotaleValidiPerLunghezzaLink+=1
			    lunghezzalink=math.fabs(tok.id-tok.head)
			    lunghezzaLink+=lunghezzalink
			    if lunghezzaLinkmax<lunghezzalink:
				lunghezzaLinkmax=lunghezzalink
		    #QUIIII nuoveFeat:
		    ####Subordinate Completive/Infinitive
		    if self.conf.subCompletiveInfinitive or self.conf.posizioneSubord or self.conf.SubordPrincipali:
			if (tok.dep == 'arg'):
			    if (((tok.pos=='CS') or (tok.cpos=='E')) and (tokens[tok.head-1].pos=='V')):
				for figlio in tok.children:
				    if (figlio.pos=='V' and (figlio.dep=='prep' or figlio.dep=='sub')):
					if tok.pos=='EA':
					    link=figlio.pos+'-->'+'E'+'-->'+tokens[tok.head-1].pos
					else:
					    link=figlio.pos+'-->'+tok.pos+'-->'+tokens[tok.head-1].pos
					##Calcolo se la subordinata e' a destra o a sinistra rispetto al verbo principale:
					if tok.head > tok.id:
					    dipDir='subord_pre'
					else:
					    dipDir='subord_post'
					if dipDir in DirezioneDipendenze:
					    DirezioneDipendenze[dipDir]+=1.0
					else:
					    DirezioneDipendenze[dipDir]=1.0
					######
					if link in ARG_Special:
					    ARG_Special[link]+=1.0
					else:
					    ARG_Special[link]=1.0
			    if (tok.pos=='V' and tokens[tok.head-1].pos=='V'):
				link=tok.pos+'-->'+tokens[tok.head-1].pos
				##Calcolo se la subordinata e' a destra o a sinistra rispetto al verbo principale:
				if tok.head > tok.id:
				    dipDir='subord_pre'
				else:
				    dipDir='subord_post'
				if dipDir in DirezioneDipendenze:
				    DirezioneDipendenze[dipDir]+=1.0
				else:
				    DirezioneDipendenze[dipDir]=1.0
				######
				if link in ARG_Special:
				    ARG_Special[link]+=1.0
				else:
				    ARG_Special[link]=1.0
			if (tok.dep == 'mod'):
			    #TOT_Mod+=1
			    if (((tok.pos=='CS') or (tok.cpos=='E') or (tok.cpos=='B')) and (tokens[tok.head-1].pos=='V')):
				for figlio in tok.children:
				    if (figlio.pos=='V' and (figlio.dep=='prep' or figlio.dep=='sub')):
					link=figlio.pos+'-->'+tok.pos+'-->'+tokens[tok.head-1].pos
					##Calcolo se la subordinata e' a destra o a sinistra rispetto al verbo principale:
					if tok.head > tok.id:
					    dipDir='subord_pre'
					else:
					    dipDir='subord_post'
					if dipDir in DirezioneDipendenze:
					    DirezioneDipendenze[dipDir]+=1.0
					else:
					    DirezioneDipendenze[dipDir]=1.0
					######
					if link in MOD_Special:
					    MOD_Special[link]+=1.0
					else:
					    MOD_Special[link]=1.0
			if (tok.dep == 'comp'):
			    #TOT_Comp+=1
			    if ((tok.cpos=='E') and (tokens[tok.head-1].pos=='V')):
				for figlio in tok.children:
				    if (figlio.pos=='V' and figlio.dep=='prep'):
					if tok.pos=='EA':
					    link=figlio.pos+'-->'+'E'+'-->'+tokens[tok.head-1].pos
					else:
					    link=figlio.pos+'-->'+tok.pos+'-->'+tokens[tok.head-1].pos
					    #link=figlio.pos+'-->'+tok.pos+'-->'+tokens[tok.head-1].pos
					##Calcolo se la subordinata e' a destra o a sinistra rispetto al verbo principale:
					if tok.head > tok.id:
					    dipDir='subord_pre'
					else:
					    dipDir='subord_post'
					if dipDir in DirezioneDipendenze:
					    DirezioneDipendenze[dipDir]+=1.0
					else:
					    DirezioneDipendenze[dipDir]=1.0
					######
					if link in COMP_Special:
					    COMP_Special[link]+=1.0
					else:
					    COMP_Special[link]=1.0

		    ###############Calcolo Subordinate e Principali:
		    if self.conf.cateneSubordinate:
			#albero="" #n
			if (tok.dep=='ROOT') and (tok.head==0) and tok.cpos=='V':
				altezzaSUB=0
				TOT_Subordinate, altezzaSubordinantiTOT, ALTEZZASub, altezzaSUB=self.visitaSubordinate(tok, tokens, 0, TOT_Subordinate, altezzaSubordinantiTOT, ALTEZZASub)
		    if self.conf.posizioneSogOgg:
			# direzione del link Soggetto/Oggetto rispetto al verbo:
			if tok.dep in ['subj', 'subj_pass']:
			    if tok.head > tok.id:
				dipDir='subj_pre'
			    else:
				dipDir='subj_post'
			    if dipDir in DirezioneDipendenzeSO:
				DirezioneDipendenzeSO[dipDir]+=1.0
			    else:
				DirezioneDipendenzeSO[dipDir]=1.0
			if tok.dep == 'obj':
			    if tok.head > tok.id:
				dipDir='obj_pre'
			    else:
				dipDir='obj_post'
			    if dipDir in DirezioneDipendenzeSO:
				DirezioneDipendenzeSO[dipDir]+=1.0
			    else:
				DirezioneDipendenzeSO[dipDir]=1.0

		##alla fine di una frase:
		if self.conf.altezzaAlbero:
		    if nROOT>0:
			altezzaMAXTOT+=(altezzaTOT/nROOT)
		    else:
			altezzaMAXTOT+=altezzaTOT
		    nROOT=0.0
		if self.conf.lunghezzaLink:
		    lunghezzaLinkMax+=lunghezzaLinkmax

	    ###alla fine del documento:

	    if self.conf.lunghezzaLink:
		Features['lunghezzaLinkMax']=lunghezzaLinkMax
		if TotaleValidiPerLunghezzaLink>1:
		    TotaleValidiPerLunghezzaLink=TotaleValidiPerLunghezzaLink-1
		Features['lunghezzaLink']=(lunghezzaLink/TotaleValidiPerLunghezzaLink)*1.0

	    if self.conf.posizioneSubord:
		for sp in DirezioneDipendenze:
		    if DirezioneDipendenze['subord_pre']+DirezioneDipendenze['subord_post']==0:
			denominatore=1.0
		    else:
			denominatore=DirezioneDipendenze['subord_pre']+DirezioneDipendenze['subord_post']
		    Features['posSubord'+'-'+sp]=(DirezioneDipendenze[sp]/denominatore)*1.0

	    if self.conf.SubordPrincipali or self.conf.subCompletiveInfinitive:
		for arg in ARG_Special:
		    FrasiSubordinate+=ARG_Special[arg]
		    if arg in ["V-->V", "V-->E-->V", "V-->EA-->V"]:
			SubordinateInfinitive+=ARG_Special[arg]
		    elif arg=="V-->CS-->V":
			SubordinateCompletive+=ARG_Special[arg]
		for mod in MOD_Special:
		    FrasiSubordinate+=MOD_Special[mod]
		    if mod in ["V-->E-->V", "V-->EA-->V"]:
			SubordinateInfinitive+=MOD_Special[mod]
		    elif mod in ["V-->CS-->V", "V-->B-->V"]:
			SubordinateCompletive+=MOD_Special[mod]
		for comp in COMP_Special:
		    if comp in ["V-->E-->V", "V-->EA-->V"]:
			SubordinateInfinitive+=COMP_Special[comp]
		    FrasiSubordinate+=COMP_Special[comp]
		#Per evitare la divisione per 0
		if FrasiSubordinate==0:
		    FrasiSubordinateT=1
		else:
		    FrasiSubordinateT=FrasiSubordinate
		#evito la divisione per 0
		if rootVerbali==0.0:
		    rootVerbaliT=1
		else:
		    rootVerbaliT=rootVerbali
		if self.conf.SubordPrincipali:
		    Features['frasiPrincipali']=(rootVerbali/(FrasiSubordinate+rootVerbaliT))*1.0
		    Features['frasiSubordinate']=(FrasiSubordinate/(FrasiSubordinate+rootVerbaliT))*1.0
		if self.conf.subCompletiveInfinitive:
		    Features['frasiSubordinateCompletive']=(SubordinateCompletive/FrasiSubordinateT)*1.0
		    Features['frasiSubordinateInfinitive']=(SubordinateCompletive/FrasiSubordinateT)*1.0

	    if self.conf.cateneSubordinate:
		if TOT_Subordinate>1:
		    TOT_Subordinate=TOT_Subordinate-1
		Features['MediaCateneSub']=(altezzaSubordinantiTOT/TOT_Subordinate)*1.0
		#print altezzaSubordinantiTOT #errrrrr
		#print TOT_Subordinate  #errrrrr
	    if self.conf.distribuzioneCateneSubordinate:
		for  i in range(9):
		    if i+1 in ALTEZZASub:
			Features['CatenaS_'+str(i+1)]=(ALTEZZASub[i+1]/TOT_Subordinate)*1.0
		    else:
			Features['CatenaS_'+str(i+1)]=0.0
	    ###############
	    if self.conf.posizioneSogOgg:
		for sOg in DirezioneDipendenzeSO:
		    if sOg[0]=="s":
			if (DirezioneDipendenzeSO['subj_pre']+DirezioneDipendenzeSO['subj_post'])==0:
			    Features[sOg]=0.0
			else:
			    Features[sOg]=(DirezioneDipendenzeSO[sOg]/(DirezioneDipendenzeSO['subj_pre']+DirezioneDipendenzeSO['subj_post']))*1.0
		    else:
			if (DirezioneDipendenzeSO['obj_pre']+DirezioneDipendenzeSO['obj_post'])==0:
			    Features[sOg]=0.0
			else:
			    Features[sOg]=(DirezioneDipendenzeSO[sOg]/(DirezioneDipendenzeSO['obj_pre']+DirezioneDipendenzeSO['obj_post']))*1.0
	    if self.conf.altezzaAlbero:
		Features['altezzaAlberi']=altezzaMAXTOT
	    if self.conf.archiEntranti:
		# ogni elemento va diviso per il numero totale di verbi
		Features['MediaArchiEntranti']=ContaLinkEntranti
	    if self.conf.distribuzioneArchiEntranti:
		for  zz in range(11):
		    if zz in LINK_ENTRANTI:
			Features['archiE_'+str(zz)]=LINK_ENTRANTI[zz]
		    else:
			Features['archiE_'+str(zz)]=0.0
	    if self.conf.catenaPrep:
		if TOT_Catene>1:
		    TOT_Catene=TOT_Catene-1
		Features['MediaCatenePrep']=(lunghezzaCatena/TOT_Catene)*1.0
		#CatenePrep
	    if self.conf.distribuzioneCatenaPrep:
		for  i in range(9):
		    if i+1 in CATENE_PREP:
			Features['CatenaP_'+str(i+1)]=(CATENE_PREP[i+1]/TOT_Catene)*1.0
		    else:
			Features['CatenaP_'+str(i+1)]=0.0

	    if self.conf.rootVerbali:
		Features['MediaRootVerbali']=rootVerbali
	    return Features

	def extract_stylo_feat(self):
		c = codecs.open("word-normalized-frequencies.txt", 'r', 'utf-8')
		docs = {}
		docs_cnt = 0
		indexes = {}
		feats = {}
		for x in c:
			docs_cnt+=1
			a = x.split("\t")
			if docs_cnt == 1:
				for idx, item in enumerate(a):
					item = item.strip()
					indexes[idx] = item
			else:
				cur_word = None
				for idx, item in enumerate(a):
					item = item.strip()
					if idx == 0:
						cur_word = item
						try:
							pass
						#print cur_word
						except:
							print "ERR"
							print cur_word
							assert False
						feats[item] = {}
					else:
						feats[cur_word][indexes[idx]] = item
		print len(feats)
		return feats


	#Estraggo le features:
	def estraiFeatures(self, DOC, cur_doc_name):
	    Features={}
	    DizFondamentale_paroleTrovate=({"AD":0.0, "AU":0.0, "FO": 0.0}, 0.0)
	    indiceFeat=0
	    Features= self.FeatWordCount(DOC, cur_doc_name)
	    if self.conf.Flessico:
		Features= self.FeatLessicali(DOC, cur_doc_name)
	    if self.conf.Fpos:
		Features = self.FeatPos(DOC, Features)
	    if self.conf.Fdependency:
		Features = self.FeatSintattiche(DOC, Features)
# 	    if self.conf.word_feats or True:
# 		    if False:
# 			    if not self.stylo_feats:
# 				    self.stylo_feats = self.extract_stylo_feat()
# 			    sorted_keys =  sorted(self.stylo_feats.keys())
# 			    for k in sorted_keys:
# 				    Features["WORD_"+k] = float(self.stylo_feats[k][cur_doc_name])
# 		    else:
# 			    pass

# 	#	    print Features
	    return Features, DizFondamentale_paroleTrovate

	def trasformaNormalizzaFormatta(self, Pre_Features, numFrasi, numToken, numTokenP, numVerbi, nROOT):
	    ListaFeatures=[]
	    ListaIndici=[]
	    if numVerbi==0:
		numVerbi=1.0
	    if numToken==0:
		numToken=1.0
	    if numTokenP==0:
		numTokenP=1.0
	    if self.conf.lunghezza_doc:
		    Pre_Features['lunghezzaDOC']=numFrasi*1.0
	    if self.conf.Flessico:
		if self.conf.dizionario:
		    lunghezza=len(Pre_Features['@DIZ-TYPE'])*1.0
		    if lunghezza==0.0:
			lunghezza=1.0
		    Pre_Features['Diz_AU']=(Pre_Features['Diz_AU']/lunghezza)*1.0
		    Pre_Features['Diz_AD']=(Pre_Features['Diz_AD']/lunghezza)*1.0
		    Pre_Features['Diz_FO']=(Pre_Features['Diz_FO']/lunghezza)*1.0
		    #Pre_Features['Diz_TrovateInDizionario']=(Pre_Features['Diz_TrovateInDizionario']/numToken)*1.0
		    Pre_Features['Diz_TrovateInDizionario']=(len(Pre_Features['@DIZ-TYPE'])*1.0/Pre_Features['CARD_LEMMI'])*1.0
		    del Pre_Features['@DIZ-TYPE']
		    del Pre_Features['CARD_LEMMI']
		if self.conf.typeToken:
		    if numToken < self.conf.rangeTypeToken:
			if numToken==0:
			    numToken=1.0
			Pre_Features['TypeTokenF']=(Pre_Features['TypeTokenF']/numToken)*1.0
			Pre_Features['TypeTokenL']=(Pre_Features['TypeTokenL']/numToken)*1.0
		    else:
			Pre_Features['TypeTokenF']=(Pre_Features['TypeTokenF']/self.conf.rangeTypeToken)*1.0
			Pre_Features['TypeTokenL']=(Pre_Features['TypeTokenL']/self.conf.rangeTypeToken)*1.0
		if self.conf.mediaLunghezzaParole:
		    Pre_Features['MediaLunghezzaParole']=(Pre_Features['MediaLunghezzaParole']/numToken)*1.0
		if self.conf.mediaLunghezzaFrasi:
		    #QUI!!!
		    Pre_Features['MediaLunghezzaFrasi']=(numToken/numFrasi)*1.0
	    if self.conf.Fpos:
		if self.conf.densitaLessicale:
		    Pre_Features['DensitaLessicale']=(Pre_Features['DensitaLessicale']/numToken)*1.0
		if self.conf.cpos:
		    Pre_Features['A']=(Pre_Features['A']/numTokenP)*1.0
		    Pre_Features['B']=(Pre_Features['B']/numTokenP)*1.0
		    Pre_Features['C']=(Pre_Features['C']/numTokenP)*1.0
		    Pre_Features['D']=(Pre_Features['D']/numTokenP)*1.0
		    Pre_Features['E']=(Pre_Features['E']/numTokenP)*1.0
		    Pre_Features['F']=(Pre_Features['F']/numTokenP)*1.0
		    Pre_Features['I']=(Pre_Features['I']/numTokenP)*1.0
		    Pre_Features['N']=(Pre_Features['N']/numTokenP)*1.0
		    Pre_Features['P']=(Pre_Features['P']/numTokenP)*1.0
		    Pre_Features['R']=(Pre_Features['R']/numTokenP)*1.0
		    Pre_Features['S']=(Pre_Features['S']/numTokenP)*1.0
		    Pre_Features['T']=(Pre_Features['T']/numTokenP)*1.0
		    Pre_Features['V']=(Pre_Features['V']/numTokenP)*1.0
		    Pre_Features['X']=(Pre_Features['X']/numTokenP)*1.0
		if self.conf.verboModo:
		    Pre_Features['V+p']=(Pre_Features['V+p']/numVerbi)*1.0
		    Pre_Features['V+i']=(Pre_Features['V+i']/numVerbi)*1.0
		    Pre_Features['V+g']=(Pre_Features['V+g']/numVerbi)*1.0
		    Pre_Features['V+m']=(Pre_Features['V+m']/numVerbi)*1.0
		    Pre_Features['V+c']=(Pre_Features['V+c']/numVerbi)*1.0
		    Pre_Features['V+d']=(Pre_Features['V+d']/numVerbi)*1.0
		    Pre_Features['V+f']=(Pre_Features['V+f']/numVerbi)*1.0
		    Pre_Features['VA+c']=(Pre_Features['VA+c']/numVerbi)*1.0
		    Pre_Features['VA+m']=(Pre_Features['VA+m']/numVerbi)*1.0
		    Pre_Features['VA+f']=(Pre_Features['VA+f']/numVerbi)*1.0
		    Pre_Features['VA+g']=(Pre_Features['VA+g']/numVerbi)*1.0
		    Pre_Features['VA+p']=(Pre_Features['VA+p']/numVerbi)*1.0
		    Pre_Features['VA+d']=(Pre_Features['VA+d']/numVerbi)*1.0
		    Pre_Features['VA+i']=(Pre_Features['VA+i']/numVerbi)*1.0
		    Pre_Features['VM+m']=(Pre_Features['VM+m']/numVerbi)*1.0
		    Pre_Features['VM+p']=(Pre_Features['VM+p']/numVerbi)*1.0
		    Pre_Features['VM+d']=(Pre_Features['VM+d']/numVerbi)*1.0
		    Pre_Features['VM+c']=(Pre_Features['VM+c']/numVerbi)*1.0
		    Pre_Features['VM+f']=(Pre_Features['VM+f']/numVerbi)*1.0
		    Pre_Features['VM+i']=(Pre_Features['VM+i']/numVerbi)*1.0
		    Pre_Features['VM+g']=(Pre_Features['VM+g']/numVerbi)*1.0
	    if self.conf.Fdependency:
		if self.conf.altezzaAlbero:
		    Pre_Features['altezzaAlberi']=(Pre_Features['altezzaAlberi']/numFrasi)*1.0
		if self.conf.archiEntranti:
		    Pre_Features['MediaArchiEntranti']=(Pre_Features['MediaArchiEntranti']/numVerbi)*1.0
		if self.conf.distribuzioneArchiEntranti:
		    for  i in range(11):
			Pre_Features['archiE_'+str(i)]=(Pre_Features['archiE_'+str(i)]/numVerbi)*1.0
		    #Pre_Features['archiE_'+str(i)]=(Pre_Features['archiE_'+str(i)]/numVerbi)*1.0
		if self.conf.rootVerbali:
		    if nROOT==0:
			nROOT=1.0
		    Pre_Features['MediaRootVerbali']=(Pre_Features['MediaRootVerbali']/nROOT)*1.0
		if self.conf.dependencyT:
		    Pre_Features['arg']=(Pre_Features['arg']/numTokenP)*1.0
		    Pre_Features['aux']=(Pre_Features['aux']/numTokenP)*1.0
		    Pre_Features['clit']=(Pre_Features['clit']/numTokenP)*1.0
		    Pre_Features['comp']=(Pre_Features['comp']/numTokenP)*1.0
		    Pre_Features['comp_ind']=(Pre_Features['comp_ind']/numTokenP)*1.0
		    Pre_Features['comp_loc']=(Pre_Features['comp_loc']/numTokenP)*1.0
		    Pre_Features['comp_temp']=(Pre_Features['comp_temp']/numTokenP)*1.0
		    Pre_Features['con']=(Pre_Features['con']/numTokenP)*1.0
		    Pre_Features['concat']=(Pre_Features['concat']/numTokenP)*1.0
		    Pre_Features['conj']=(Pre_Features['conj']/numTokenP)*1.0
		    Pre_Features['det']=(Pre_Features['det']/numTokenP)*1.0
		    Pre_Features['dis']=(Pre_Features['dis']/numTokenP)*1.0
		    Pre_Features['disj']=(Pre_Features['disj']/numTokenP)*1.0
		    Pre_Features['mod']=(Pre_Features['mod']/numTokenP)*1.0
		    Pre_Features['mod_rel']=(Pre_Features['mod_rel']/numTokenP)*1.0
		    Pre_Features['mod_loc']=(Pre_Features['mod_loc']/numTokenP)*1.0
		    Pre_Features['mod_temp']=(Pre_Features['mod_temp']/numTokenP)*1.0
		    Pre_Features['modal']=(Pre_Features['modal']/numTokenP)*1.0
		    Pre_Features['neg']=(Pre_Features['neg']/numTokenP)*1.0
		    Pre_Features['obj']=(Pre_Features['obj']/numTokenP)*1.0
		    Pre_Features['pred']=(Pre_Features['pred']/numTokenP)*1.0
		    Pre_Features['pred_loc']=(Pre_Features['pred_loc']/numTokenP)*1.0
		    Pre_Features['pred_temp']=(Pre_Features['pred_temp']/numTokenP)*1.0
		    Pre_Features['prep']=(Pre_Features['prep']/numTokenP)*1.0
		    Pre_Features['punc']=(Pre_Features['punc']/numTokenP)*1.0
		    Pre_Features['ROOT']=(Pre_Features['ROOT']/numTokenP)*1.0
		    Pre_Features['sub']=(Pre_Features['sub']/numTokenP)*1.0
		    Pre_Features['subj']=(Pre_Features['subj']/numTokenP)*1.0
		    Pre_Features['subj_pass']=(Pre_Features['subj_pass']/numTokenP)*1.0
		if self.conf.lunghezzaLink:
		    Pre_Features['lunghezzaLinkMax']=(Pre_Features['lunghezzaLinkMax']/numFrasi)*1.0

	    # Stampa i valori delle features:
	    if self.dump:
	        for label in Pre_Features:
			print '\t'.join([label, str(Pre_Features[label])])
		print
	    for feat in sorted(Pre_Features.iterkeys()):
	        ListaFeatures.append(Pre_Features[feat])
		ListaIndici.append(feat)
	    return ListaIndici, ListaFeatures

	def extract_global_feats(self):
		if not self.conf.top_n_words:
			return
		most_common = []
		if not self.for_testing:
			for num_gram, num_words, in self.conf.top_n_words:
				most_common.append((num_gram,
						   self.forme_global_counter_new[num_gram].most_common(num_words)))

			pickle.dump(most_common, open("most_common_words.pickle", "w"))
		else:
			most_common = pickle.load(open("most_common_words.pickle", "r"))
		for num_grams, most_common_grams in most_common:
			assert len(self.doc_names) == len(self.samples)
			for idx, doc_name in enumerate(self.doc_names):
				self.samples[idx] += [self.forme_doc_counter_new[num_grams][doc_name][x[0]]
						      for x in most_common_grams]
			for x in [x[0] for x in most_common_grams]:
				feat_val = "WORD_%s_%s" % (num_grams, x)
				self.ListaFeatures.append(feat_val)


	def analyze(self, DOC, cur_doc_name):
	    ListaFeatures=[]
	    numVerbi=0.0
	    numFrasi=0.0
	    numToken=0.0
	    numTokenP=0.0
	    nROOT=0.0
	    for tokens in DOC:
		numFrasi+=1.0
		#servono per il calcolo delle features globali del documento
		for tok in tokens:
		    numTokenP+=1.0
		    if tok.cpos=='V':
			numVerbi+=1.0
		    if self.conf.Fdependency:
			if tok.dep == 'ROOT':
			    nROOT+=1.0
		    if not(tok.cpos=='F'):
			numToken+=1.0
			############
	    Pre_Features, DizFondamentale_paroleTrovate=self.estraiFeatures(DOC, cur_doc_name)
	    print >> sys.stderr, '.',
	    #for tokens in DOC: #errr
		    #for tok in tokens:  #errr
			    #print tok.word.encode("utf-8")  #errr
	    ListaFeatures_Indice=self.trasformaNormalizzaFormatta(Pre_Features, numFrasi, numToken, numTokenP, numVerbi, nROOT)
	    return ListaFeatures_Indice

	def dump_features(self, fileOUT):
		self.samples = self.normalize_samples(self.samples)
		Versione1=0
		Versione2=1
		Versione3=0
		#COMMENTATO MA OK:
		#for sample in samples:
			#for feat in sample:
			        #if not feat in FeatName:
					#FeatName[feat]=0.0
				#FeatName[feat]+=1.0
		#FeatNameList_temp=FeatName.keys()
		#FeatNameList_temp.sort()
		#print FeatNameList_temp
		#FeatNameList=[]
		#for feat in FeatNameList_temp:
			#if FeatName[feat]>2:
				#FeatNameList.append(feat)
		#MARTIN:
		if Versione1:
			fileLabels = codecs.open("Labels.txt", "w", "utf-8")
			fileFeatureNames = codecs.open("FeatNames.txt", "w", "utf-8")
			fileMatrix = codecs.open("Matrix.txt", "w", "utf-8")
			for l in labels:
			    print >> fileLabels, l
			for f in self.ListaFeatures[0]:
				print >> fileFeatureNames, f.encode('utf-8')
			id=0
			for sample in samples:
				stringa=""
				for feat in sample:
					stringa+=str(feat)+"\t"
			## stringa=""
## 			for featOrd in FeatNameList:
## 				if featOrd in sample:
## 					stringa+="1\t"
## 				else:
## 					stringa+="0\t"
				stringa.strip()
				print >>  fileMatrix, stringa
		if Versione2:
			assert len(self.labels_value) == len(self.samples)
			id=0
			#fileOUT = codecs.open("Graph.txt", "w", "utf-8")
			fileFeatureNames = codecs.open(self.output_dir + "/" + "FeatNames.txt", "w", "utf-8")
			for f in self.ListaFeatures:
				print >> fileFeatureNames, f
			for idx, sample in enumerate(self.samples):
				if self.for_testing:
					stringa= self.labels_value[idx] + " "+str(len(sample))
					idS=0
					for feat in sample:
						idS+=1
						stringa+=" "+str(idS)+" "+str(feat)
					print >>  fileOUT, stringa
				elif self.outcomes[idx] == True:
					print >>  fileOUT, 2
					stringa= "1 "+str(len(sample))
					idS=0
					for feat in sample:
						idS+=1
						stringa+=" "+str(idS)+" "+str(feat)
					print >>  fileOUT, stringa
					print >>  fileOUT, "0 0"
				else:
					print >>  fileOUT, 2
					stringa = "0 "+str(len(sample))
					idS=0
					for feat in sample:
						idS+=1
						stringa+=" "+str(idS)+" "+str(feat)
					print >>  fileOUT, stringa
					print >>  fileOUT, "1 0"

				id+=1

		if Versione3:
			id=0
			fileOUT = codecs.open("test_Graph.txt", "w", "utf-8")
			for sample in samples:
				stringa=labels[id]+" "+str(len(sample))
				idS=0
				for feat in sample:
					idS+=1
					stringa+=" "+str(idS)+" "+str(feat)
				print >>  fileOUT, stringa
				id+=1
		##########
		#if (self.conf.ALG=='SVM'):
			#print >> sys.stderr, self.conf.ALG
			#modello=Fsvm_Denso_CreaModelloSVM(LabelsSamples, self.conf.parametriSVM, self.modello)  #in felice_svm
		#else:
			#print >> sys.stderr, 'ERR: Algoritmo di apprendimento sconosciuto:',self.conf.ALG

		pass

	def normalize_samples(self, samples):
		must_dump = True
		if self.min_samples:
			must_dump = False
			min_samples = self.min_samples
			max_samples = self.max_samples
		else:
			min_samples = [None] * len(samples[0])
			max_samples = [None] * len(samples[0])
			for sample in samples:
				for idx, val in enumerate(sample):
					if min_samples[idx] is None:
						min_samples[idx] = val
						max_samples[idx] = val

					else:
						min_samples[idx] = min(min_samples[idx], val)
						max_samples[idx] = max(max_samples[idx], val)
			pickle.dump((min_samples, max_samples), open(self.output_dir + "/norm.pickle", "w"))
		for sample in samples:
			for idx, val in enumerate(sample):
				if max_samples[idx] == min_samples[idx]:
					continue
				sample[idx] = (sample[idx] - min_samples[idx]) /  float((max_samples[idx] - min_samples[idx]))
		return samples



	def parseFile(self, inputFileName, fileOUT, is_positive, for_testing, clazz, translation_file):
		tokens=[]
		gold=""
		id=0
		labels=[]
		samples=[]
		DOC=[]
		Versione1=0
		Versione2=1
		Versione3=0
		counter = 0
		cur_doc_name = ""
		skipped = open(self.output_dir + '/skipped.txt', 'a')
		import re
		regex = re.compile('"(.*)\.txt"')
		self.fileInput=codecs.open(inputFileName, encoding=self.encode, mode='r')
		for l in self.fileInput:
			#fine file
			#print "ECCOLO:", l #errr
			if l == '':
				break

			#if l[:5] == "<doc ":
				#tokens=[]
				#DOC=[]
				#continue
				#print

                        #fine frase
			if "<doc" in l:
				cur_doc_name = l
				#print l
				cur_doc_name = regex.findall(l)[0]
				#print cur_doc_name
			if l == '\n':
				#Se la lunghezza della frase e' sotto un certo limite scarta la frase
				if len(tokens)<self.infLength:
					tokens=[]
					continue
				DOC.append(tokens)
				if self.conf.unitaAnalisi=="frase":
					id+=1
					Martin=0
					if Versione1:
						labels.append(gold+"_"+str(id))
					else:
						labels.append(gold)

					ListaFeatures=seSlf.analyze(DOC)
					#print "--->", ListaFeatures[0] #err1107
					if Martin:
						count=0
						ListaFeatMartin=[]
						for elem in ListaFeatures[0]:
							val="%.5f" % ListaFeatures[1][count]
							feat=elem+"_"+val  #str(ListaFeatures[1][count])
							count+=1
							ListaFeatMartin.append(feat)
						samples.append(ListaFeatMartin)
					else:
						samples.append(ListaFeatures[1])
					DOC=[]
				tokens = []
				continue

			#fine documento
			if l == "</doc>\n":
				counter +=1
			#	print counter
				if self.conf.unitaAnalisi=="documento":
					Martin=0
					#print self.infSenteces
					#Se la lunghezza del DOC e' minore di "self.infSenteces" frasi allora salta l'analisi ##conf
					if len(DOC)<=self.infSenteces:
						#print cur_doc_name
						skipped.write(cur_doc_name.strip() + "\n")
						DOC=[]
						#print "ECCOMI!!!"
						continue
					id+=1
					translation_file.write(cur_doc_name + "\n")
					if Versione1:
						labels.append(gold+"_"+str(id))
					else:
						labels.append(gold)
					ListaFeatures=self.analyze(DOC, cur_doc_name)
					self.doc_names.append(cur_doc_name)
					if Martin:
						count=0
						ListaFeatMartin=[]
						for elem in ListaFeatures[0]:
							val="%.5f" % ListaFeatures[1][count]
							feat=elem+"_"+val  #str(ListaFeatures[1][count])
							count+=1
							ListaFeatMartin.append(feat)
						samples.append(ListaFeatMartin)
					else:
						samples.append(ListaFeatures[1])
					DOC=[]
					tokens=[]
				#	if counter != len(samples):
				#		import pdb; pdb.set_trace()

					continue
				else:
					tokens=[]
					#if counter != len(samples):
				#		import pdb; pdb.set_trace()
					continue
				if counter != len(samples):
					import pdb; pdb.set_trace()
			# stiamo leggendo un tag doc di apertura:
			#    cosi' viene cancallato (altrimenti gestire in altro modo)
			if l[0] == "<":
				tokens = []
				continue
			if l[:6]=="VALORE":
				print >> sys.stderr
				print >> sys.stderr, l
				l=(l.strip()).split('\t')
				gold=l[1]
				tokens = []
				continue
			l=(l.split('\t'))
			tok = Token(l)
			tokens.append(tok)
		#samples = self.normalize_samples(samples)
		LabelsSamples=(labels, samples)
		FeatName={}
		self.ListaFeatures = ListaFeatures[0]
		self.samples += samples
		self.outcomes += [is_positive] * len(samples)
		self.labels_value += [clazz] * len(samples)

	#	print samples

		if True:
			return
		#COMMENTATO MA OK:
		#for sample in samples:
			#for feat in sample:
			        #if not feat in FeatName:
					#FeatName[feat]=0.0
				#FeatName[feat]+=1.0
		#FeatNameList_temp=FeatName.keys()
		#FeatNameList_temp.sort()
		#print FeatNameList_temp
		#FeatNameList=[]
		#for feat in FeatNameList_temp:
			#if FeatName[feat]>2:
				#FeatNameList.append(feat)
		#MARTIN:
		if Versione1:
			fileLabels = codecs.open("Labels.txt", "w", "utf-8")
			fileFeatureNames = codecs.open("FeatNames.txt", "w", "utf-8")
			fileMatrix = codecs.open("Matrix.txt", "w", "utf-8")
			for l in labels:
			    print >> fileLabels, l
			for f in ListaFeatures[0]:
				print >> fileFeatureNames, f.encode('utf-8')
			id=0
			for sample in samples:
				stringa=""
				for feat in sample:
					stringa+=str(feat)+"\t"
			## stringa=""
## 			for featOrd in FeatNameList:
## 				if featOrd in sample:
## 					stringa+="1\t"
## 				else:
## 					stringa+="0\t"
				stringa.strip()
				print >>  fileMatrix, stringa
		if Versione2:
			id=0
			#fileOUT = codecs.open("Graph.txt", "w", "utf-8")
			fileFeatureNames = codecs.open(self.output_dir + "/FeatNames.txt", "w", "utf-8")
			for f in ListaFeatures[0]:
				print >> fileFeatureNames, f
			for idx, sample in samples:
				if  for_testing:
					stringa= clazz + " "+str(len(sample))
					idS=0
					for feat in sample:
						idS+=1
						stringa+=" "+str(idS)+" "+str(feat)
					print >>  fileOUT, stringa
				elif is_positive:
					print >>  fileOUT, 2
					stringa= "1 "+str(len(sample))
					idS=0
					for feat in sample:
						idS+=1
						stringa+=" "+str(idS)+" "+str(feat)
					print >>  fileOUT, stringa
					print >>  fileOUT, "0 0"
				else:
					print >>  fileOUT, 2
					stringa = "0 "+str(len(sample))
					idS=0
					for feat in sample:
						idS+=1
						stringa+=" "+str(idS)+" "+str(feat)
					print >>  fileOUT, stringa
					print >>  fileOUT, "1 0"

				id+=1

		if Versione3:
			id=0
			fileOUT = codecs.open("test_Graph.txt", "w", "utf-8")
			for sample in samples:
				stringa=labels[id]+" "+str(len(sample))
				idS=0
				for feat in sample:
					idS+=1
					stringa+=" "+str(idS)+" "+str(feat)
				print >>  fileOUT, stringa
				id+=1
		##########
		#if (self.conf.ALG=='SVM'):
			#print >> sys.stderr, self.conf.ALG
			#modello=Fsvm_Denso_CreaModelloSVM(LabelsSamples, self.conf.parametriSVM, self.modello)  #in felice_svm
		#else:
			#print >> sys.stderr, 'ERR: Algoritmo di apprendimento sconosciuto:',self.conf.ALG

		pass


	def Training(self, inputFileName, allFiles, for_testing):
		self.for_testing = for_testing
		if for_testing:
			self.min_samples, self.max_samples = pickle.load(open(self.output_dir + "/norm.pickle", "r"))
			fileout_map = codecs.open(self.output_dir + "/testing.grafting.map", "w", "utf-8")

			fileOUT = codecs.open(self.output_dir + "/testing.grafting", "w", "utf-8")
		else:
			inputFileName = os.path.basename(inputFileName)
			fileout_map = codecs.open(self.output_dir + "/" + inputFileName +".grafting.map", "w", "utf-8")
			fileOUT = codecs.open(self.output_dir + "/" + inputFileName + ".grafting", "w", "utf-8")
		for x in allFiles:
			print >> sys.stderr, "\nprocessing %s " % x
			clazz = None
			if "Educ".lower() in x.lower():
				clazz = "EDU"
			if "Jour".lower() in x.lower():
				clazz = "JOU"
			if "Scie".lower() in x.lower():
				clazz = "SCI"
			if "LIT".lower() in x.lower():
				clazz = "LIT"
			assert clazz is not None
                        is_positive = os.path.basename(x) == inputFileName
			self.parseFile(x, fileOUT, is_positive, for_testing, clazz, fileout_map)
		self.extract_global_feats()
		self.dump_features(fileOUT)
			#exit(-1)

	def Parsing(self, inputFileName, destDir=None):
		if self.valutazione:
			GOLD={}
			Previsioni={}
			Corretti={}
			Sbagliati={}
			corretti=0.0
			sbagliati=0.0
		self.inputFileName = inputFileName
		(self.fileInput, self.fileOutput, self.outputFileName) = openIOFiles(inputFileName, destDir, ".read")
		tokens=[]
		ListaFeatures=[]
		DOC=[]
		#for l in self.fileInput:
		while True:
		    l=self.fileInput.readline()
		    if l == '':
			if len(DOC) >= self.infSenteces: #QUII
			    if self.conf.unitaAnalisi=="frase":
				if len(tokens)<self.infLength:
					ProbabilityPrediction="NAN"
					self.StampaOut(DOC, ProbabilityPrediction)
					if self.valutazione:
						GOLD, Previsioni, Corretti, Sbagliati, corretti, sbagliati=self.Valutazione(ProbabilityPrediction, gold, Previsioni, GOLD, Corretti, Sbagliati, corretti, sbagliati)
					continue
			    ListaFeatures=self.analyze(DOC)
			    if (self.conf.ALG=='SVM'):
				    ProbabilityPrediction=Fsvm_Denso_ApplicaSVM(ListaFeatures[1], self.modello)
				    self.StampaOut(DOC, ProbabilityPrediction)
				    if self.valutazione:
					    GOLD, Previsioni, Corretti, Sbagliati, corretti, sbagliati=self.Valutazione(ProbabilityPrediction, gold, Previsioni, GOLD, Corretti, Sbagliati, corretti, sbagliati)
			DOC=[]
			tokens=[]
			break
		    #fine frase
		    if not(self.valutazione):
			    if l[0] == "V":
				    continue
		    else:
			    if l[:6]=="VALORE":
				    l=(l.strip()).split('\t')
				    gold=float(l[1])
				    tokens = []
				    continue
		    if l == '\n':
			if tokens:
				if len(tokens)>=self.infLength:
					DOC.append(tokens)
				if self.conf.unitaAnalisi=="frase":
				    if len(tokens)<self.infLength:
					ProbabilityPrediction="NAN"   #  NAN sta per non calcolabile..(ad es: troppo corta la frase..)

					self.StampaOut(DOC, ProbabilityPrediction)
					if self.valutazione:
					    GOLD, Previsioni, Corretti, Sbagliati, corretti, sbagliati=self.Valutazione(ProbabilityPrediction, gold, Previsioni, GOLD, Corretti, Sbagliati, corretti, sbagliati)
					continue
				    ListaFeatures=self.analyze(DOC)
				    if (self.conf.ALG=='SVM'):
					ProbabilityPrediction=Fsvm_Denso_ApplicaSVM(ListaFeatures[1], self.modello)
					self.StampaOut(DOC, ProbabilityPrediction)
					if self.valutazione:
					    GOLD, Previsioni, Corretti, Sbagliati, corretti, sbagliati=self.Valutazione(ProbabilityPrediction, gold, Previsioni, GOLD, Corretti, Sbagliati, corretti, sbagliati)
				    DOC=[]   # in questo caso "conf.unitaAnalisi==frase" DOC deve contenere sempre solo una frase
				tokens = []
			continue
		    if l[:5] == "<doc ":
			if self.printText:
				lS=l.strip().split("\"")
				titolo=lS[1]
				print  >> self.fileOutput, ''.join([titolo, ":"])
				print
			continue
		    #fine documento
		    if l == "</doc>\n":
		        if len(DOC) >= self.infSenteces:
				if self.conf.unitaAnalisi=="documento":
				    ListaFeatures=self.analyze(DOC)
				    if (self.conf.ALG=='SVM'):
					ProbabilityPrediction=Fsvm_Denso_ApplicaSVM(ListaFeatures[1], self.modello)
					self.StampaOut(DOC, ProbabilityPrediction)
					if self.valutazione:
						GOLD, Previsioni, Corretti, Sbagliati, corretti, sbagliati=self.Valutazione(ProbabilityPrediction, gold, Previsioni, GOLD, Corretti, Sbagliati, corretti, sbagliati)
			DOC=[]
			tokens=[]
			continue
		    # stiamo leggendo un tag doc di apertura:
		    #    cosi' viene cancallato (altrimenti gestire in altro modo)
		    if l[0] == "<":
			tokens = []
			continue

		    l=(l.split('\t'))
		    tok = Token(l)
		    tokens.append(tok)
		#se l'ultimo elemento finisce senza il tag <\doc>
		if len(DOC) >= self.infSenteces:
		    ListaFeatures=self.analyze(DOC)
		    if (self.conf.ALG=='SVM'):
			ProbabilityPrediction=Fsvm_Denso_ApplicaSVM(ListaFeatures[1], self.modello)
			self.StampaOut(DOC, ProbabilityPrediction)
			if self.valutazione:
				GOLD, Previsioni, Corretti, Sbagliati, corretti, sbagliati=self.Valutazione(ProbabilityPrediction, gold, Previsioni, GOLD, Corretti, Sbagliati, corretti, sbagliati)
		if self.valutazione:
			self.StampaValutazioni(GOLD, Previsioni, Corretti, Sbagliati, corretti, sbagliati)

	def StampaValutazioni(self, GOLD, Previsioni, Corretti, Sbagliati, corretti, sbagliati):
		print >> self.fileOutput
		print >> self.fileOutput, 'VALUTAZIONE GENERALE:'
		print >> self.fileOutput, '\t'.join(['Numero di Documenti totale:', str(corretti+ sbagliati)])
		print >> self.fileOutput, 'distribuiti cosi\': '
		for gg in GOLD:
			print >> self.fileOutput,  '\t'.join([str(gg),str(GOLD[gg])])
		print >> self.fileOutput
		print >> self.fileOutput, 'Classificati cosi\':'
		for label in  Previsioni:
			print >> self.fileOutput, '\t'.join([str(label), str(Previsioni[label])])
		print >> self.fileOutput
		if Corretti or Sbagliati:
			print >> self.fileOutput, 'Classificati correttamente: ', corretti,', sbagliati: ',sbagliati
			print >> self.fileOutput, 'Valutazione:'
			print >> self.fileOutput, 'CORRETTI:'
			for label in  Corretti:
				print >> self.fileOutput, '\t'.join(['Gold:',str(label), 'Corretti:', str(Corretti[label])])
			print >> self.fileOutput
			print >> self.fileOutput, 'SBAGLIATI:'
			ListaOrdinataSbagliati=Ordina(Sbagliati)
			for label in  ListaOrdinataSbagliati:
				labelS=label[0].split('_')
				print >> self.fileOutput, '\t'.join(['Gold:',labelS[0], 'Sbagliati in:', labelS[1], 'freq:', str(label[1])]) #str(Sbagliati[label])])
		##Calcolo della Precision e Recall:
		print >> self.fileOutput
		print >> self.fileOutput
		print >> self.fileOutput, 'Misure di Valutazione:'
		print >> self.fileOutput, '\t'.join(['Accuracy:', str(corretti/(corretti+ sbagliati))])
		for gg in GOLD:
			if not gg in Corretti:
				Corretti[gg]=0.0
			Recall=(Corretti[gg])/GOLD[gg]
			if gg in Previsioni:
				Precision=(Corretti[gg])/Previsioni[gg]
			else:
				Precision=0.0
			if Precision+Recall:
				FMeasure=2*((Precision*Recall)/(Precision+Recall))
			else:
				FMeasure=0.0
			print >> self.fileOutput, '\t'.join([str(gg), 'Precision:', str(Precision), 'Recall:', str(Recall), 'F-measure:', str(FMeasure)])

	def StampaOut(self, DOC, ProbabilityPrediction):
		if self.printText:
			for tokens in DOC:
				for tok in tokens:
					print >> self.fileOutput, tok.word,
				#print
			print >> self.fileOutput
		if not(ProbabilityPrediction=="NAN"):
			ProbabilityPrediction=ProbabilityPrediction[2][0][1]
		if not(self.valutazione):
			print >> self.fileOutput, '\t'.join(["Indice_DYLAN:", str(ProbabilityPrediction)])
			print >> self.fileOutput


	def Valutazione(self, ProbabilityPrediction, gold, Previsioni, GOLD, Corretti, Sbagliati, corretti, sbagliati):
		if not(ProbabilityPrediction=="NAN"):
			ProbabilityPrediction=ProbabilityPrediction[0][0]
		if not(gold in GOLD):
			GOLD[gold]=0.0
		GOLD[gold]+=1.0
		if not (ProbabilityPrediction in Previsioni):
			Previsioni[ProbabilityPrediction]=0.0
		Previsioni[ProbabilityPrediction]+=1
		if gold==ProbabilityPrediction:
			corretti+=1.0
			if not (gold in Corretti):
				Corretti[gold]=0.0
			Corretti[gold]+=1.0
			print >> self.fileOutput, '\t'.join(["CORRETTO:", str(gold)])
		else:
			sbagliati+=1.0
			label="GOLD:"+str(gold)+'_'+"OUTPUT:"+str(ProbabilityPrediction)
			if not(label in Sbagliati):
				Sbagliati[label]=0.0
			Sbagliati[label]+=1.0
			print >> self.fileOutput, '\t'.join(["SBAGLIATO:", label])
		return  GOLD, Previsioni, Corretti, Sbagliati, corretti, sbagliati


	def CaricaReadability(self, confFileName, modelFileName):
		self.Lexicon={}
		self.conf = Conf()
		#import pdb; pdb.set_trace()
	#	import re
		regex = re.compile("/(.*?).conf")
		self.output_dir = "extracted_features/" + regex.findall(confFileName)[0]
		import os
		try:
			os.makedirs(self.output_dir)
		except:
			pass
#		import pdb; pdb.set_trace()
		self.conf.CaricaFileConfigurazione(confFileName)
		if self.conf.dizionario:
			self.CaricaDizionarioFondamentale()
		if self.training:
			self.modello=modelFileName
		else:
			if (self.conf.ALG=='SVM'):
				self.modello = Fsvm_Denso_CaricaModelloSVM(modelFileName)  #in felice_svm
			else:
				print >> sys.stderr, 'ERR: Algoritmo di apprendimento sconosciuto:',self.conf.ALG
				exit(-1)

	def Readability(self,inputFileName, destDir=None):
		self.Parsing(inputFileName, destDir)

def usage():
	print """Classificatore_Filtro.py:
	Input file must be in CoNLL format.

	Usage:
	  ML_Leggibilita.py [options]

	Options:
	  -t                        set training mode
	  -i inputFile              input file name
	  -m file.model             modelFile (default filtro.model)
	  -d                        print the value of the features
	  -x                        print the text
	  -c confile.conf           set configuration file (default filtro.conf)
	  -n arg                    select the minimum number of tokens for sentence (default 6)
	  -s arg                    select the minimum number of sentence for document (default 1)
	  --help                    display this help and exit
	  --usage                   display script usage

	"""

def main():
	try:
		long_opts = ['os', 'help', 'usage']
		opts, args = getopt.gnu_getopt(sys.argv[1:], 'h c: t x m: s: n: d o i: A a: z',long_opts)
	except getopt.GetoptError:
		usage()
		sys.exit()
	#Valori di default:
	dump=False
	scriptpath = os.path.abspath(os.path.dirname(sys.argv[0]))
	fileLessico=scriptpath+"/"+'Lessici/DizionarioFondamentale'
	fileConf='Conf/leggibilita.conf'
	training=False
	printText=False
	#printTextAnalisi=False
	modelFile='Modelli/leggibilita'
	infLength=2
	infSenteces=1
	inputFileName=""
	all_file_names=""
	for_testing = False
	for opt, arg in opts:
	    if opt in ('-h', '--help'):
		usage()
		sys.exit()
	    elif opt == '--usage':
		usage()
		sys.exit()
	    elif opt == '-t':
		training=True
	    elif opt == '-x':
		printText=True
	    #elif opt == '-A':
		#printTextAnalisi=True
	    elif opt == '-c':
		fileConf=arg
	    elif opt == '-m':
		modelFile=arg
	    elif opt == '-n':
		infLength=int(arg)
	    elif opt == '-s':
		infSenteces=int(arg)
	    #elif opt == '-l': Si usa sempre il lessico di demauro!
		#fileLessico=arg
	    elif opt == '-d':
		dump=True
	    elif opt == '-i':
		inputFileName=arg
	    elif opt == '-a':
		all_file_names=arg
	    elif opt == '-z':
		for_testing=True

	    else:
		    assert False, "unhandled option"
	p = Readability()
	#p.setEncode(encode)
	p.setLessico(fileLessico)
	p.setTraining(training)
	p.setPrintText(printText)
	p.setDump(dump)
	p.setInfLength(infLength)
	p.setInfSenteces(infSenteces)
	#p.setPrintTextAnalisi(printTextAnalisi)
	#p.CaricaReadability(fileConf,modelFile, fileLessico, training, printText, dump)
	p.CaricaReadability(fileConf,modelFile)
	if training:
		all_file_names = all_file_names.split()
		p.Training(inputFileName, all_file_names, for_testing)
	else:
		#p.CaricaReadability(scriptpath+"/"+fileConf,scriptpath+"/"+modelFile, scriptpath+"/"+fileLessico, printText, dump)
		#print >> sys.stderr, 'Caricate le strutture...'
		p.Readability(inputFileName)
		#print >> sys.stderr, 'Fine analisi...'

if __name__ == "__main__":
    main()

